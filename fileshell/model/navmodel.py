# Object to hold information about the current location
# that the viewport is showing, and/or any adjacency
# information that may be needed
#
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os

import PyQt4
from PyQt4 import QtCore as qcore

from fileshell.model import fsdb
from fileshell.model.fsutils import *
from fileshell.time.timeRange import *

###################################################
# Navigation Model

"""
	TODO:
		- path separators?
"""

class NavModel(qcore.QObject):
	# Signal for changed location
	locationChanged = qcore.pyqtSignal()
	
	# ctor
	# < fs_model: (FsModel) full file system model
	# < (start): (FolderData | str) path to initial starting folder, OR the folder itself
	# < (private): (bool) private navigation - no history is stored (for future use)
	def __init__(self, fs_model, start=None, private=False):
		qcore.QObject.__init__(self)
		
		# store file system
		self.fs_model = fs_model
		
		# init backend data
		self.location = None	# (FolderData) location
		self.root = None		# (FolderData) first location that we started from
		
		# init adjacency data
		self.depth = 0			# how deep (from a root) is item
		self.depth_target = 0	# target depth (i.e. the depth that our targets should exist at - usually same as depth, unless we don't have one there)
		
		# init history stack
		self.previous = []		# ([FolderData]) destinations visited before current
		self.later = []			# ([FolderData]) destinations visited after current - IN REVERSE ORDER (nearest last)
		
		self.private = private	# (bool) don't update access events list after visiting items if True
		
		# set current location
		self.setLocation(start)
		self.set_root()
	
	# Location API =================================
	
	# Set current location
	# < (loc): (FolderData | str) path to initial starting folder, OR the folder itself
	def setLocation(self, loc):
		# parse string to identify location?
		if type(loc) in (str, unicode):
			# parse from string
			_loc = loc
			loc = self.location_from_string(loc)
			if loc is None: print "ERROR: Couldn't find matching destination '%s'" % (_loc)
		elif type(loc) is qcore.QString:
			# parse from QString
			_loc = str(loc)
			loc = self.location_from_string(_loc)
			if loc is None: print "ERROR: Couldn't find matching destination q'%s'" % (_loc)
			
		if loc is None:
			# we're at root...
			loc = self.fs_model.tree[0]
			
		# start trying to change location if we're not there already
		if self.location != loc:
			# purge history
			if self.location is not None:
				self.push_history()
			
			# navigate to this item now
			self.go_to_location(loc)
	
	# Helper function to get location from a string
	# TODO: build in advanced query language here which can support rearranged paths!
	#		'?' = start of query string
	#		'|' = delimits one of tags that should be present
	#		'/?/' = any number of paths may occur between here
	def location_from_string(self, locstr):
		# helper to find folder with matching path
		def find_folder(folder, basestr, lstr, sep):
			# add folder's name to the base string
			if not basestr:
				# 2 versions - full path version (default) and a truncated version where folder is a root
				path = folder.base_path.replace('\\', sep).replace('/', sep)
				#print path, folder.base_path
				
				path2 = folder.name
			else:
				# no alternatives
				path = ''.join((basestr, sep, folder.name))
				path2 = path   # although less efficient, this will end up evaluating the same
			
			# check if we're on the right track (or if we've found our target)
			if (path == lstr) or (path2 == lstr):
				# return match
				return folder
			elif lstr.startswith(path) or lstr.startswith(path2):
				# determine which is the base path
				bpath = path if lstr.startswith(path) else path2
				
				# keep going
				for child in folder.subdirs:
					ret = find_folder(child, bpath, lstr, sep)
					if ret is not None:
						return ret
			else:
				# dead end
				return None
		
		# for now, just traverse entire tree until we find a matching string
		#if ('/' in locstr) or ('\\' in locstr):
		#	sep = '/' if locstr.startswith('/') else '\\'
		#else:
		#	sep = os.path.sep
		sep = os.path.sep
		
		for root in self.fs_model.tree:
			ret = find_folder(root, None, locstr, sep)
			if ret is not None:
				return ret
		else:
			return None
	
	# Navigate to a specified location
	def go_to_location(self, folder, action='a'):
		# update access events
		if not self.private:
			self.fs_model.accessItem(folder, action)
		
		# update depth
		self.depth = 0
		
		f = folder
		while (f is not None) and (f.parent and f.parent()):
			self.depth += 1
			f = f.parent()
		
		# set location
		self.location = folder
		
		# send out updates
		self.locationChanged.emit()
	
	# Set the current folder as the root that we reference off
	def set_root(self):
		self.root = self.location
	
	# Reset NavModel ===========================================
	
	# Reset navmodel to initial state
	# NOTE: This is mainly only useful when performing experiments
	def reset(self):
		# go back to root
		self.location = self.root
		self.depth = 0
		
		# clear all history
		self.previous = []
		self.later = []
		
		# flush
		self.locationChanged.emit()
		
	# History API ==============================================
	# TODO: a more sophisticated method should store additional information for faster restores
	#       i.e. using a helper state object which stores locations within folders too
	
	# Add current location to the history stack, and start new traveral direction
	def push_history(self):	
		# for now, just append the folder
		self.previous.append(self.location)
		
		# truncate the future history
		self.later = []
	
	
	# Check if we can go backwards
	# > returns: (bool) True if can go backwards
	def canGoBack(self):
		return len(self.previous) > 0
		
	# Check if we can go forwards
	# > returns: (bool) True if can go forwards
	def canGoForward(self):
		return len(self.later) > 0
		
	# Check if we can go up one level
	def canGoUp(self):
		return bool(self.location and 
					self.location.parent and 
					self.location.parent())
	
	
	# Go backwards in history
	# ! Assumes that canGoBack() as already been called
	# < (steps): (int) number of "go back" steps to take - is clamped to length of previous list
	def goBack(self, steps=1):
		# go back a number of times until we've gone back the required number of steps
		steps = min(len(self.previous), max(1, steps))
		
		loc = self.location
		for i in range(steps):
			# add current to future stack (LIFO)
			self.later.append(loc)
			
			# go to last item in history (and remove it from history)
			loc = self.previous.pop()
		
		# navigate to the desired position now
		self.go_to_location(loc, 'GoBack')
		
	# Go forwards in history
	# ! Assumes that canGoForward() as already been called
	# < (steps): (int) number of "go forward" steps to take - is clamped to length of forward list
	def goForward(self, steps=1):
		# go forward a number of times until we've gone forward the required number of steps
		steps = min(len(self.later), max(1, steps))
		
		loc = self.location
		for i in range(steps):
			# append current location to history again
			self.previous.append(loc)
			
			# take location from forward stack (LIFO)
			loc = self.later.pop()
		
		# navigate to the desired position now
		self.go_to_location(loc, 'GoForward')
		
	# Go up one level
	# ! Assumes that canGoUp() as already been called
	def goUp(self):
		# add current location to history stack
		self.push_history()
		
		# go to parent of folder
		self.go_to_location(self.location.parent(), 'GoUp')
		
		# updates
		self.locationChanged.emit()
		
	# Adjacency API =============================================
	
	# Navigate horizontally along levels
	# - Assume that only folders 2-levels deep are compatible
	
	# Recursively find candidate 'sibling' folder to navigate to
	# < src: (FolderData) current folder we're standing on...
	# < depth: (int) depth of item we came from
	def find_sibling(self, src, depth, dir):
		# get parent of folder we just came from
		if (src.parent is None) or (src.parent() is None):
			# root folder - try sibling roots
			srcIndex = self.fs_model.tree.index(src)
			
			if 0 <= (srcIndex + dir) < len(self.fs_model.tree):
				# another root folder
				return self.fs_model.tree[srcIndex + dir]
			else:
				# not a valid root folder...
				return None
		else:
			# grab parent
			parent = src.parent()
			
			# try to go to sibling of current item
			srcIndex = parent.subdirs.index(src)
			
			if 0 <= (srcIndex + dir) < len(parent.subdirs):
				# sibling of current item is possible candidate
				return parent.subdirs[srcIndex + dir]
			else:
				# no other siblings to src... go to parent of parent to find it's siblings (i.e. uncles)
				uncle = self.find_sibling(parent, depth-1, dir)
				
				# candidate should now be first/last (depending on direction) child of uncle...
				if uncle and uncle.subdirs:
					if dir < 0:
						return uncle.subdirs[-1] 	# last child - i.e. head to left = last
					else:
						return uncle.subdirs[0]		# first child - i.e. head to right = first
				else:
					# uncle is dead end: settling here is bad idea and is confusing
					return None
			
	
	# Go to "previous" item  
	def goPrev(self):
		# recursively traverse tree upwards+across until we get somewhere we want
		sibling = self.find_sibling(self.location, self.depth, -1)
		
		# this should now be our new destination
		if sibling:
			# add current location to history stack
			self.push_history()
			
			# go to sibling
			self.go_to_location(sibling, 'GoPrev')
			
			# updates
			self.locationChanged.emit()
		else:
			print "Previous sibling not found"
		
	# Go to "next" item
	def goNext(self):
		# recursively traverse tree upwards+across until we get somewhere we want
		sibling = self.find_sibling(self.location, self.depth, 1)
		
		# this should now be our new destination
		if sibling:
			# add current location to history stack
			self.push_history()
			
			# go to sibling
			self.go_to_location(sibling, 'GoNext')
			
			# updates
			self.locationChanged.emit()
		else:
			print "Next sibling not found"

###################################################
