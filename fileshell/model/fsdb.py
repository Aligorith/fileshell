# Utilities for interacting loading file system information stored
# in sqlite database files, and stores this info in memory
#
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os
import sqlite3 as sql
import time
import weakref

from fileshell.time.timeRange import dtFromTimestamp, timestampFromDt

from alphanum import alphanum_sort
from fsutils import *

##############################################
# Database Schema - Used to create tables

fsdbSchema = """\
	/* A file system item */
	create table Item 
	(
		id integer primary key autoincrement,
		
		name,		/* (str) short name to display */
		type,		/* ('file', 'folder') */
		
		ctime,		/* (float) OS-dependent - creation/metadata-edit time in secs */
		mtime,		/* (float) modification time secs */
		atime,		/* (float) access time in secs */
		size		/* (int) size in bytes */
	);
	
	/* Root folders */
	create table RootFolder
	(
		itemId integer primary key,
		path,
		
		FOREIGN KEY(itemID) REFERENCES Item(id)
	);
	
	/* Folder <-> Subfolders */
	create table ChildFolder
	(
		itemId integer primary key,
		parent integer,
		depth integer,
		
		FOREIGN KEY(itemID) REFERENCES Item(id),
		FOREIGN KEY(parent) REFERENCES Item(id)
	);
	
	/* Folder <-> Files */
	create table ChildFile
	(
		itemId integer primary key,
		folder integer,
		
		FOREIGN KEY(itemID) REFERENCES Item(id),
		FOREIGN KEY(folder) REFERENCES Item(id)
	);
	
	/* Folders (and their subtrees) that should not be shown */
	create table HiddenFolder
	(
		folder integer,
		
		FOREIGN KEY(folder) references Item(id)
	);
	
	/* Items which jump to other items */
	create table JumpList
	(
		fromId integer,		/* item to start from */
		toId integer,		/* item to jump to */
		
		FOREIGN KEY(fromId) REFERENCES Item(id),
		FOREIGN KEY(toId) REFERENCES Item(id)
	);
	
	/* Access Time events */
	create table AccessEvent
	(
		id integer primary key autoincrement,
		
		item integer,	/* (ref) item that was accessed */
		time,			/* (float) time of access in secs */
		action,			/* ('c', 'm', 'a') type of access action */
		
		hour,			/* (int [0, 23]) hour of day */
		dayOfWeek,		/* (int [1, 7]) weekday index */
		
		FOREIGN KEY(item) REFERENCES ITEM(id)
	);
"""

##############################################
# File System Datatypes

# A File System Item
class FsItem:
	# ctor
	def __init__(self, id, name):
		self.id = id					# identifier for item (in database)
		self.name = name		 		# name of item
		
		self.fs_model = None			# (weakref<FsModel>) the File System model item is attached to
		self.parent = None				# (weakref<FolderData>) parent item
		
		self.ignore = False				# should item be ignored in display?
		self.bookmarked = False			# user-specified "importance" tag
		
		self.attributes = {} 			# metadata/path data
	
	# is item a symlink or shortcut
	def isLink(self):
		# XXX: name ending check fails for non-files...
		return ( ('symlink' in self.attributes) or
			     (self.name.endswith('.lnk')) )
	
	# get the path of the item
	def path(self, fullPath=True, asStr=False):
		# base of path + self
		if hasattr(self, 'base_path') and fullPath:
			# with a base path, assume that we are root
			# - our name is already included in the path
			path_elems = [self.base_path]
		elif self.parent is None:
			# no parent (base case) - just own name
			path_elems = [self.name]
		else:
			# build from parents
			# - don't build as string (it's only done at toplevel)
			#   to avoid accumulation problems
			path_elems = self.parent().path(fullPath, False)
			path_elems.append(self.name)
		
		# return whatever is relevant
		# TODO: cache the results for speed
		if asStr:
			# use whatever separator is natural for this platform
			sep = os.path.sep
			return sep.join(path_elems)
		else:
			# just return the composite list
			return path_elems
	
	# get modification time of item
	# TODO: how to handle symlinks...
	def mtime(self, force=False):
		if 'm' in self.attributes:
			return self.attributes['m']
		else:
			return None
	
	# item modified
	# < ts: (float) time stamp when item was modified
	def itemModified(self, ts):
		# NOTE: To be implemented by subclasses!
		# 1) modify timestamp stored in memory...
		# 2) update database or add to set of modified items
		pass
		
	# mark item as being important
	# < (apply): (bool) if True, add new bookmark to item
	def itemBookmark(self, apply=True):
		if apply:
			# set local flag
			self.bookmarked = True
			
			# update database
			if not self.fs_model().foreign:
				# XXX: add to special "bookmarks" table
				pass
		else:
			# set local flag
			self.bookmarked = False
			
			# update database
			if not self.fs_model().foreign:
				# XXX: remove from "bookmarks" table
				pass

# A Folder
class FolderData(FsItem):
	# ctor
	def __init__(self, id, name):
		# super-ctor
		FsItem.__init__(self, id, name)
		
		# children
		self.subdirs = []
		self.files = []
	
	def __repr__(self):
		# two versions here:
		# 1) first version is for debug
		# 2) second version is for sorting
		
		#return "FolderData(%s, %s, %s, %s)" % (self.id, self.name, self.subdirs, self.files)
		return "Dir(%s)" % (self.name)
	
	# Metadata/Stats Operations =====================================
	
	# override of standard method - take the most recent time between self and contained files and use that...
	# < (mtime): (fn(...)) aggregation function for determining what to retrieve
	# < (skip_children): (bool) skip adjusting time using children times
	# < (force): (bool) ignore cached values
	def mtime(self, fn=max, skip_children=False, force=False):
		# for efficiency, we cache the aggregate values...
		# NOTE: it is the responsibility of children (when modified) to unset this cache so that it gets recalculated
		if ('M' in self.attributes) and (not force):
			# grab from cache
			t = self.attributes['M']
		else:
			# use standard method as baseline (for self)
			t = FsItem.mtime(self)
			
			if t is None:
				if len(self.files):
					t = 0.0
				else:
					return None
			
			# check child files
			if len(self.files) and (skip_children is False):
				ct = fn(f.mtime() for f in self.files)
				t  = fn(t, ct)
			
			# save off value for later
			self.attributes['M'] = t
		
		# return time
		return t
		
	# item modified
	# < ts: (float) time stamp when item was modified
	def itemModified(self, ts):
		# folder itself
		# NOTE: assume that modifications can only ever get tagged to occur later than stated...
		self.attributes['m'] = ts
		self.attributes['M'] = max(self.attributes['M'], ts)
		
		# update time-range and/or database
		fs_model = self.fs_model()
		
		tmodel = fs_model.time_model
		tmodel.update_from_item(ts)
		
		if fs_model.foreign:
			fs_model.modified_items.add(self)
		else:
			# TODO: update database...
			pass
	
	# Ignore folder and its children
	def ignoreTree(self):
		# mark folder and its children as being ignored
		def folder_ignore_cb(f, par, depth, data):
			# apply ignore tag
			f.ignore = True
			
			# TODO: update database...
		
		# recursively apply, starting from self
		self.traverseFolders(folder_ignore_cb)
	
	# -------------------------------------------------------------------
	
	# return the number of descendents that folder has
	def numDescendents(self):
		# +1 is in to include the subdir itself
		return sum([f.numDescendents() + 1 for f in self.subdirs])
		
	# returns whether the folder can be considered a link in a chain (zero-order tree)
	# XXX: maybe we need minimum thresholds instead?
	def isChainLink(self):
		# - it should only have one child folder
		# - it must also have very few files
		#   XXX: allow for a single file too (for a readme)  --- (REVIEW THIS!)
		return (len(self.subdirs) == 1) and (len(self.files) < 2)
	
	# Tree Building Operations =======================================
		
	# add subdirectory
	def addSubdir(self, subdir):
		if type(subdir) is str:
			self.subdirNames.append(subdir)
		else:
			self.subdirs.append(subdir)
			
	# add file
	# < child: (FileData)
	def addFile(self, child):
		self.fileNames.append(child.name)
		self.files.append(child)
	
	# Generic Tree API ================================================
		
	# traverse folder hierarchy (DFS order), running the given callback
	# < callback: (fn(folder:FolderData, parent:FolderData, depth:int, data=None)) 
	#			operation to perform on each folder in the hierarchy
	# < (data): where callback can use as context and/or to dump stuff it finds
	#		   - use where the callback doesn't already have closure info to use
	# < (parent): (FolderData) parent folder of the current folder
	# < (depth): (int) number of levels from the root
	def traverseFolders(self, callback, data=None, parent=None, depth=0):
		# operate on folder itself
		callback(self, parent, depth, data)
		
		# visit subfolders
		for subdir in self.subdirs:
			subdir.traverseFolders(callback, data, self, depth+1)
	
	# sort the folders and files
	# XXX: this only sorts using natural sorting order for filenames now...
	def sort(self):
		# callback for performing sorting
		def sort_cb(folder, parent, depth, data):
			alphanum_sort(folder.subdirs, True)
			alphanum_sort(folder.files, True)
		
		# recursively sort, starting from self
		self.traverseFolders(sort_cb)


# A File
class FileData(FsItem):
	# ctor
	# < id: (int) Identifier of item
	# < name: (str) Name of file only (no path)
	def __init__(self, id, name):
		# super-ctor 
		FsItem.__init__(self, id, name)
		
	def __repr__(self):
		# two versions here:
		# 1) first version is for debug
		# 2) second version is for sorting
		
		#return "FileData(%s, %s)" % (self.id, self.name)
		return "File(%s)" % (self.name)
		
	# item modified
	# < ts: (float) time stamp when item was modified
	def itemModified(self, ts):
		# file itself
		self.attributes['m'] = ts
		
		# update time-range and/or database
		fs_model = self.fs_model()
		
		tmodel = fs_model.time_model
		tmodel.update_from_item(ts)
		
		if fs_model.foreign:
			fs_model.modified_items.add(self)
		else:
			# TODO: update database...
			pass
		
		# parent...
		if self.parent():
			# flush modified time up to parent folder
			folder = self.parent()
			
			#folder.mtime(force=True)
			folder.attributes['M'] = max(folder.attributes['M'], ts)
			
			if fs_model.foreign:
				fs_model.modified_items.add(folder)
			else:
				# TODO: update database...
				pass

##############################################
# Time Model

# TODO: this needs a signal that can be used when the range changes

class TimeModel:
	# reference model
	def __init__(self, fsmodel):
		# database model used
		self.fsmodel = fsmodel
		
		# value cache - only fetches once per second
		# to minimise database thrashing
		self.last_polled = None
		
		self._min = 0
		self._max = 0
	
	
	# perform fetch from db?
	def update(self):
		# check if there was already an update recently
		# XXX: for now, the regular polling is disabled, since the database doesn't get updated...
		ts = time.time()
		if (self.last_polled is None): # or (ts - self.last_polled > 1):
			self.update_from_db()
			
			# update last polled time
			self.last_polled = ts
			
	# Fetch time from DB - Use for resetting time range 
	# (when doing purely in-memory time updates)
	def update_from_db(self):
		# fetch new values
		# XXX: mtime only?
		with self.fsmodel.dbConnection() as C:
			low, high = C.execute("select min(mtime), max(mtime) from Item").fetchone()
			if (low is not None) and (high is not None):
				self._min = low
				self._max = high
	
	# Update time extents given the timestamp provided
	# (as a result of some item being modified)
	def update_from_item(self, ts):
		# extent range (locally only)
		self._min = min(self._min, ts)
		self._max = max(self._max, ts)
	
	
	# properties - min/max
	def tmin():
		def fget(self):
			self.update()
			return self._min
		return locals()
	tmin = property(**tmin())
	
	def tmax():
		def fget(self):
			self.update()
			return self._max
		return locals()
	tmax = property(**tmax())

##############################################
# Access Event Logger

# Basic logger used
class AccessLogger:
	# ctor
	def __init__(self, fs_model):
		self.fs_model = fs_model
		self.is_bound = False
		
		
	# Bind logger to model
	def bind(self):
		# assume that if bound, cannot be bound again
		if self.is_bound:
			assert(self in self.fs_model.access_loggers)
			return
			
		# bind to model
		self.fs_model.access_loggers.append(self)
		self.is_bound = True
		
	# Unbind logger from model
	def unbind(self):
		# assume that if not bound, it isn't already bound
		if not self.is_bound:
			assert(self not in self.fs_model.access_loggers)
			return
			
		# unbind
		self.fs_model.access_loggers.remove(self)
		self.is_bound = False
		
		
	# Log an access event
	# ! To be implemented by subclasses
	def accessEvent(self, item, action, ts):
		pass
		
# Default logger used by FsModel
class DefaultAccessLogger(AccessLogger):
	# Log an access event
	def accessEvent(self, item, action, ts):
		# write this in the FsModel's database
		with self.fs_model.dbConnection() as C:
			# extract data needed for logging access event
			hour = ts.hour
			day = ts.isoweekday()
			
			tval = timestampFromDt(ts)
			
			# construct and add access event
			access = (item.id, tval, action, hour, day)
			C.execute("insert into AccessEvent(item, time, action, hour, dayOfWeek) values (?, ?, ?, ?, ?)", access)

##############################################
# File System data model

class FsModel:
	# ctor
	def __init__(self, fileN, is_snapshot=False):
		# filename of backing database
		self.fileN = fileN
		
		# is backing database just a snapshot of a foreign database?
		self.foreign = is_snapshot
		
		# cached references to items
		# NOTE: this is initially a dict, which becomes weakref dict when items have been stored later
		#       otherwise garbage collection gobbles the data before we've had a chance to use it
		self.folders = {} 
		self.files = weakref.WeakValueDictionary()
		
		# forest of folder hierarchies
		self.tree = []
		
		# time model
		self.time_model = None
		
		# TODO: file system info - path separator, drives, bookmarks...
		
		# access event loggers
		self.access_loggers = []
		
		if not is_snapshot:
			self.default_logger = DefaultAccessLogger(self)
			self.default_logger.bind()
		else:
			# set of items which have been modified
			self.modified_items = set()
		
	# Low-Level Database Wrangling ==================================
		
	# get database connection
	# ! This is not guaranteed to return the same instance everytime
	def dbConnection(self):
		# get connection to database
		# - fetch items as namedtuple types (using builtin Row object)
		C = sql.connect(self.fileN)
		C.row_factory = sql.Row
		
		return C
		
	# Data Loading ==================================================
		
	# load model data and prepare for use
	def load(self, include_files=True, sort_data=False):
		# init data if non-existent
		with self.dbConnection() as C:
			if not C.execute("select count(*) from sqlite_master where name='Item' and type='table'").fetchone()[0]:
				print "Initialising database..."
				C.executescript(fsdbSchema)
		
		# load data
		self.load_folders()
		
		if include_files:
			self.load_files()
		
		# init time model
		self.time_model = TimeModel(self)
		
		# sort
		if sort_data:
			self.sort_tree()
		
	# utility to set item attributes from a database row
	def item_attrs_from_dbrow(self, row, item):
		for attr,key in {'ctime':'c', 'mtime':'m', 'atime':'a', 'size':'s'}.items():
			if row[attr] is not None:
				item.attributes[key] = row[attr]
				
		# also, add "original mtime" for caching old mtime value...
		if 'm' in item.attributes:
			item.attributes['original_mtime'] = item.attributes['m']
		
	# load all folders
	def load_folders(self):
		# open database to load data
		with self.dbConnection() as C:
			# 1) create folder objects in memory
			with TimedOperation("Loading Folders from db..."):
				query = "select id, name, ctime, mtime, atime, size from Item where type='folder'"
				for f_row in C.execute(query).fetchall():
					# create folder
					folder = FolderData(f_row['id'], f_row['name'])
					self.item_attrs_from_dbrow(f_row, folder)
					
					# store
					self.folders[f_row['id']] = folder
					
					# attach to model
					folder.fs_model = weakref.ref(self)
			
			# 2) link up folder objects
			with TimedOperation("Build Folder Tree..."):
				# get roots, and add as roots in the tree...
				root_ids = C.execute("select * from RootFolder").fetchall()
				
				for root in root_ids:
					folder = self.folders[root['itemId']]
					folder.base_path = root['path']
					
					self.tree.append(folder)
				
				# get depth extents
				minDepth, maxDepth = C.execute("select min(depth), max(depth) from ChildFolder").fetchone()
				
				if not minDepth: minDepth = 0
				if not maxDepth: maxDepth = 0
				
				for depth in range(minDepth, maxDepth+1):
					# get items at this depth
					query = "select itemId, parent from ChildFolder where depth=?"
					layer_ids = C.execute(query, (depth,)).fetchall()
					
					for (id, parId) in layer_ids:
						# get folder items
						folder = self.folders[id]
						parent = self.folders[parId]
						
						# link them up
						parent.subdirs.append(folder)
						folder.parent = weakref.ref(parent)
						
			# 3) now that the refs are safely loaded, convert the dict to a weakrefs object
			self.folders = weakref.WeakValueDictionary(self.folders)
			
	# load all files
	def load_files(self):
		# open database to load data
		with self.dbConnection() as C:
			# fetch items as namedtuple types (using builtin Row object)
			C.row_factory = sql.Row
			
			# 1) fetch files and attach to folders
			# TODO: in future, consider options for loading files on demand...
			# NOTE: proportionally, there are more of these, so we should really prioritise how these get loaded
			with TimedOperation("Fetching all files..."):
				query = """ SELECT id, name, folder, ctime, mtime, atime, size \
							FROM   Item, ChildFile \
							WHERE  itemId=id """
				
				for f_row in C.execute(query).fetchall():
					# create file
					file = FileData(f_row['id'], f_row['name'])
					self.item_attrs_from_dbrow(f_row, file)
					
					# attach to folder
					# TODO: the list that these get stored in could be extended to automatically chuck these out after 
					# a period of inactivity...
					folder = self.folders[f_row['folder']]
					folder.files.append(file)
					file.parent = weakref.ref(folder)
					
					# store locally too...
					self.files[file.id] = file
					
					# attach to model
					file.fs_model = weakref.ref(self)
			
	# sort the filenames in the tree to ensure sane order
	def sort_tree(self):
		with TimedOperation("Sorting tree..."):
			# recursively sort
			for root in self.tree:
				root.sort()
				
	# Data Access ==================================================
	
	# Log an access to an item
	# < item: (FsItem)
	# < action: ('c', 'm', 'a')
	def accessItem(self, item, action):
		# use 'now' as timestamp
		ts = dtFromTimestamp(time.time())
		
		# log event in each attached logger
		for logger in self.access_loggers:
			logger.accessEvent(item, action, ts)
			
	# Status ========================================================
	
	# Reset status
	def reset(self):
		# reset time-model from DB
		self.time_model.update_from_db() # XXX: needs an alias
		
		# reset times for all "modified" items
		if self.foreign:
			self.reset_modified_files()
			
	# Reset all files which have been modified
	def reset_modified_files(self):
		# force recalc of the items themselves
		for item in self.modified_items:
			# restore from cached value
			if 'original_mtime' in item.attributes:
				item.attributes['m'] = item.attributes['original_mtime']
			else:
				print "Item %s has no original mtime value" % (item.id)
			
			# clear cache
			if 'M' in item.attributes:
				del item.attributes['M']
		
		# clear modified items - none are modified now
		self.modified_items.clear()
		
	# Statistics Wrangling ==========================================
	
	# Get maximum depth of tree (without traversing tree to find out)
	def getMaxDepth(self):
		# use DB to perform this query in background
		with self.dbConnection() as C:
			depth = C.execute("select max(depth) from ChildFolder").fetchone()[0]
			return int(depth)
	
	# Get list of the most recent
	# < (as_folders): (bool) return results in terms of the folders containing the items selected?
	# < (one_per_folder): (bool) limit the number of items per folder to only one 
	#                            This allows us to query a range of folders which
	#                            have targets which were accessed recently
	# < (limit): (int) maximum number of most recent items to return
	#
	# > returns: ([FolderData] or [FileData]) list of matching items, sorted descending order 
	#            (most recent first) by modification time
	def findRecentItems(self, as_folders=False, one_per_folder=True, limit=20):
		# open database to load data
		with self.dbConnection() as C:
			# construct query
			query = """SELECT id, folder \
			           FROM Item, ChildFile \
			           WHERE itemId = Item.id """
			
			if one_per_folder:
				query += " GROUP BY ChildFile.folder "
			
			query += " ORDER BY Item.mtime desc "
			
			if limit:
				query += " LIMIT %d" % (int(limit))
				
			# perform query
			if as_folders:
				recent_items = [self.folders[folderId]   for itemId, folderId in C.execute(query).fetchall()]
			else:
				recent_items = [self.files[itemId]       for itemId, folderId in C.execute(query).fetchall()]
				
			# return matching items
			return recent_items
				
		
########################################

if __name__ == '__main__':
	# test callback to dump list of things
	def print_dump(folder, parent, depth, data):
		print '%s|%s' % (' '*depth, folder.name)
		for file in folder.files:
			print '%s%s' % (' '*depth, file.name)
	
	# create model
	model = FsModel(sys.argv[1])
	model.load(include_files=True, sort_data=True)
	
	# dump
	#print "Dumping Tree..."
	#for root in model.tree:
	#	root.traverseFolders(print_dump)
	
	print "Time extents: %f %f" % (model.time_model.tmin, model.time_model.tmax)
	
	
