# Search box widget
# Author: Joshua Leung

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

# TODO: 
# 	- users need to be able to connect to this live and respond accordingly
#		-> textChanged(str) signal...

##########################################

# Live-search textbox
class SearchBox(qgui.QLineEdit):
	def __init__(self):
		qgui.QLineEdit.__init__(self)
		
		self.setup_ui()
		self.bind_events()
		
	# customise appearance
	def setup_ui(self):
		# widget settings ------------------------------------------------
		# textbox settings
		self.setStyleSheet("QLineEdit { padding: 2px; height: 18px; }")
		self.setToolTip("Text to filter files and folders by")
		self.setPlaceholderText("Search...")
		
		# search control
		self.searchbut = qgui.QToolButton()
		self.searchbut.setIcon(qgui.QIcon(":/images/search.png"))
		self.searchbut.setStyleSheet("QToolButton { padding: 2px; height: 16px; }")
		self.searchbut.setToolTip("Filter files and folders displayed")
		self.searchbut.setCursor(qcore.Qt.ArrowCursor)
		
		# layout management -----------------------------------------------
		# hbox - used to position search control inside widget
		hbox = qgui.QHBoxLayout()
		hbox.setMargin(0)
		hbox.setContentsMargins(0,0,0,0)
		
		hbox.addStretch(1.0)
		hbox.addWidget(self.searchbut)
		
		self.setLayout(hbox)
		
	# event handling
	def bind_events(self):
		# ctrl-k to focus
		aboxShortcut = qgui.QShortcut(qgui.QKeySequence.fromString("Ctrl+K"), self)
		aboxShortcut.activated.connect(self.setFocus)
		
		# search = enterkey
		self.searchbut.clicked.connect(self.returnPressed)

##########################################

if __name__ == '__main__':
	app = qgui.QApplication(sys.argv)
	
	win = qgui.QMainWindow()
	win.setWindowTitle("Search Box")
	
	win.setCentralWidget(SearchBox())
	
	win.show()
	app.exec_()
