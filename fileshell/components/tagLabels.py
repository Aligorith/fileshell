# A widget for representing floating speech-bubble/tag
# widgets beside alongside another widget
#
# Author: Joshua Leung
# Date: 18-19 August 2013

import os
import textwrap

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

######################################################
# Tag Constants

# Tag "callout triangle" Size
TAG_SIZE_STD = 12          # <-- ok for big multi-lined tags

TAG_SIZE_SMALL = 18        # <-- ok for single-line, but quite cramped
TAG_SIZE_MEDIUM = 24       # <-- ideal for single-line labels

# ----------------------------------------------------

# Constants for tag label drawing 
PADDING = 3  # px surrounding each side of the widget
XOFS    = 2  # extra horizontal offset at start of label

######################################################
# Tag Labels

# Base tag label type
# ! DO NOT USE DIRECTLY
class TagLabel(qgui.QWidget):
	# User clicked on tag
	clicked = qcore.pyqtSignal()
	
	# -----------------------------
	
	# ctor
	# < host: (QWidget) widget that tag is being used to indicate features of
	# < spawn_co: (int) x/y-offset from origin of host for where tag is to appear on side of host
	#                   For HORIZONTAL tags, this is y
	#                   For VERTICAL tags, this is x
	# < content: (QWidget) widget which will be displayed inside tag
	#
	# < (fill): (QBrush) background colour/gradient used inside the tag (and behind the content)
	#                    If None, a light-coloured base will be used
	# < (border): (QPen) colour used for border around edge of tag
	#
	# < (fading_effects): (bool) perform animated fade in/out
	# < (tsize): (TAG_SIZE_SMALL | TAG_SIZE_MEDIUM) size of triangle indicator
	def __init__(self, host, spawn_co, content, fill=None, border=None, tsize=TAG_SIZE_MEDIUM, fading_effects=False):
		# parent is either host or screen - screen for Windows, otherwise window gets raised...
		if os.name == 'nt':
			parent = qgui.QApplication.desktop().screen(self.getTipScreen(host, spawn_co))
		else:
			parent = host
		
		# initialise base widget with this info
		qgui.QWidget.__init__(self, parent, qcore.Qt.ToolTip)
		
		# store host-reference data
		self.host = host
		self.spawn_co = spawn_co
		
		# store color info (or init to defaults if not set)
		cs = qgui.QPalette()
		
		if fill is None:
			fill = qgui.QBrush(cs.color(qgui.QPalette.Base), qcore.Qt.SolidPattern)
		if border is None:
			border = qgui.QPen(cs.color(qgui.QPalette.Dark), 1)
		
		self.fill = fill
		self.border = border
		
		# settings for drawing/effects
		self.fading_effects = fading_effects
		self._opacity = 1.0
		
		self.tsize = tsize
		
		# attach content we're hosting to tag body
		self.content = content
		self.content.setParent(self)
		
		# compute various size/shape parameters
		self.compute_tag()        # <--- from subclasses
		self.compute_render_regions()
		self.updatePosition()     # <--- from subclasses
		
	# Tag Compute ===================================
	
	# Return which screen (on multi-screen setups) the tooltip appears on
	# < pos: (QPoint) point on screen where tooltip/widget appears
	# < widget: (QWidget) widget that tooltip appears for
	# > returns: (int) screen number/index
	#          >> -1 = if widget does not appear
	def getTipScreen(self, widget, pos):
		if qgui.QApplication.desktop().isVirtualDesktop():
			pos = widget.mapToGlobal(widget.geometry().topLeft()) # XXX...
			return qgui.QApplication.desktop().screenNumber(pos)
		else:
			return qgui.QApplication.desktop().screenNumber(widget)
	
	
	# Compute dimensions of tag + lay out content + cache all this for later
	def compute_tag(self):
		# TO BE PROVIDED BY SUBCLASSES...
		pass
		
	# compute/set dimensions for rendering-related regions
	def compute_render_regions(self):
		h = self.height()
		
		# set extents for gradient (if used)
		if isinstance(self.fill, qgui.QLinearGradient):
			# top to bottom, but leave a gap around edges, so that real colors can sink in...
			GAP = 1
			
			self.fill.setStart(GAP,       GAP)
			self.fill.setFinalStop(GAP,   h - GAP)
		
		# set transparancy mask
		# XXX: shrink this back a bit to avoid jaggies...
		maskedRegion = qgui.QRegion(self.shape)
		self.setMask(maskedRegion)
	
	
	# Update position of tag relative to host
	def updatePosition(self):
		# TO BE PROVIDED BY SUBCLASSES...
		pass
		
	# Custom Properties =============================
	
	# Opacity property is used for the fading-in effects...
	@qcore.pyqtProperty(float)
	def opacity(self):
		return self._opacity
	
	@opacity.setter
	def opacity(self, val):
		self._opacity = val
		try:
			self.repaint()
		except:
			pass # XXX: we're repainting a dead widget
	
	# Tag Drawing ===================================
	
	# Custom drawing method
	def paintEvent(self, evt):
		# draw tag shape
		p = qgui.QPainter()
		p.begin(self)
		
		p.setBackgroundMode(qcore.Qt.TransparentMode)
		p.setRenderHint(qgui.QPainter.Antialiasing)
		
		if self.fading_effects:
			p.setOpacity(self._opacity) # for fade in/out
		
		#self.draw_drop_shadow(p)
		self.draw_tag(p)
		
		p.end()
	
	# Draw tag background
	def draw_tag(self, p):
		# set color and fill
		p.setBrush(self.fill)
		p.setPen(self.border)
		
		# draw polygon using these settings
		p.drawPolygon(self.shape)
	
	# Tag Methods ===================================
	
	# Show tag
	def showTag(self, immediately=False):
		# animate opacity - results in a flashing effect
		if self.fading_effects and not immediately:
			ani = qcore.QPropertyAnimation(self, "opacity")
			
			ani.setStartValue(0.0)  # off...
			ani.setEndValue(1.0)    # on...
			ani.setDuration(1000)   # fade in...
			
			def updateOpacity_FadeIn():
				val = ani.currentValue().toFloat()[0]
				self.opacity = val
			ani.valueChanged.connect(updateOpacity_FadeIn)
			
			# start animating
			ani.start()
			
		# as last step, ensure that opacity is ALWAYS set to full-strength (to avoid occasional glitches)
		self.opacity = 1.0
		
		# show widget...
		#print "showing tag - %d, %d" % (self.pos().x(), self.pos().y())
		self.show()
		self.content.show()
		
	# Hide tag
	def hideTag(self, immediately=False):
		if self.fading_effects and not immediately:
			# animate opacity - should fade out, but this doesn't work that great...
			# NOTE: this doesn't work so well...
			ani = qcore.QPropertyAnimation(self, "opacity")
			
			ani.setStartValue(1.0)  # on...
			ani.setEndValue(0.05)    # off...
			ani.setDuration(1000)   # fade out...
			
			def updateOpacity_FadeOut():
				# animate fading...
				val = ani.currentValue().toFloat()[0]
				self.opacity = val
			
			ani.valueChanged.connect(updateOpacity_FadeOut)
			ani.start()
			
			# properly hide at very end
			self.hide()
		else:
			# just hide immediately
			self.hide()
		
	# Event Handling ================================
	
	# "Click" event
	def mouseReleaseEvent(self, mevt):
		# assume for now that clicks only end when mouse-up
		# TODO: distinguish between click and drag?
		print "Tag clicked"
		self.clicked.emit()

# ====================================================

# TagLabel base-type for those where arrow is horizontal
class HorizontalBaseTag(TagLabel):
	# Compute base dimensions
	# < content_xofs_start: (Num) amount of extra padding at start of x-axis
	# > returns: {various} Check return at end for list of relevant values...
	def compute_dimensions(self, content_xofs_start):
		# get size of contents to show
		csize = self.content.sizeHint()
		
		cw = csize.width()  + PADDING * 2 + XOFS
		ch = csize.height() + PADDING * 2
		
		# compute real total dimensions of widget
		# - In addition to the wrapper content, we also have the triangular
		#   area pointing to the yco that everything is spawned from
		tw = self.tsize
		
		w = cw + tw
		h = max(ch, tw)
		
		self.bounds = qcore.QSize(w, h)
		self.resize(self.bounds)
		
		
		# move content-widget into place
		c_xofs = PADDING + content_xofs_start
		c_yofs = (h / 2.0) - (ch / 2.0) + PADDING # ensure that content is centered
		
		self.content.setGeometry(qcore.QRect(qcore.QPoint(c_xofs, c_yofs), csize))
		
		
		# return these dimensions that are needed later...
		return cw, ch, tw, w, h
		
	# Update position of tag relative to host
	# < xofs: (int) offset on x-axis
	def update_position(self, xofs):
		# use host's top-left corner (i.e. it's position) in global coordinates as our anchor point
		host = self.host
		p = host.mapToGlobal(qcore.QPoint(0,0))
		
		# obtain our dimensions
		w  = self.bounds.width()
		h  = self.bounds.height()
		
		hh = h / 2.0     # half-height is what we must line-up against yco
		
		# compute vertical offset:
		# - This is done by lining up our midpoint with yoffset from top of host.
		#   That is, 1) move widget down to sit on yoffset, then 
		#            2) correct back for interleaving distance
		yofs = self.spawn_co - hh
		
		# apply offset and new position
		delta = qcore.QPoint(xofs, yofs)
		p += delta
		
		self.move(p)



# ---------------------------------------------------------



# TagLabel base-type for those where arrow is vertical
class VerticalBaseTag(TagLabel):
	# Compute base dimensions
	# < content_yofs_start: (Num) amount of extra vertical padding from top of tag widget (for content)
	# > returns: {various} Check return at end for list of relevant values...
	def compute_dimensions(self, content_yofs_start):
		# get size of contents to show
		csize = self.content.sizeHint()
		
		cw = csize.width()  + PADDING * 2 + XOFS
		ch = csize.height() + PADDING * 2 + XOFS
		
		# compute real total dimensions of widget
		# - In addition to the wrapper content, we also have the triangular
		#   area pointing to the xco that everything is spawned from
		tw = self.tsize
		
		w = max(cw, tw)
		h = ch + tw
		
		self.bounds = qcore.QSize(w, h)
		self.resize(self.bounds)
		
		
		# move content-widget into place
		c_xofs = (w / 2.0) - (cw / 2.0) + PADDING # ensure that content is centered
		c_yofs = PADDING + content_yofs_start
		
		self.content.setGeometry(qcore.QRect(qcore.QPoint(c_xofs, c_yofs), csize))
		
		
		# return these dimensions that are needed later...
		return cw, ch, tw, w, h
		
	# Update position of tag relative to host
	# < yofs: (int) offset on y-axis
	def update_position(self, yofs):
		# use host's top-left corner (i.e. it's position) in global coordinates as our anchor point
		host = self.host
		p = host.mapToGlobal(qcore.QPoint(0,0))
		
		# obtain our dimensions
		w  = self.bounds.width()
		h  = self.bounds.height()
		
		hw = w / 2.0     # half-width is what we must line-up against xco
		
		# compute vertical offset:
		# - This is done by lining up our midpoint with yoffset from top of host.
		#   That is, 1) move widget down to sit on yoffset, then 
		#            2) correct back for interleaving distance
		xofs = self.spawn_co - hw
		
		# apply offset and new position
		delta = qcore.QPoint(xofs, yofs)
		p += delta
		
		self.move(p)


# ====================================================

# Left-Side Tag
class LeftSideTag(HorizontalBaseTag):
	# Compute size + dimensions of many aspects of this type of tag
	# ! Override(TagLabel)
	def compute_tag(self):
		# calculate and set basic dimension values
		cw, ch, tw, w, h = self.compute_dimensions(XOFS)
		
		# compute polygon for tag shape
		#  1 ----------------- 2
		#  |                   |
		#  |                   |
		#  |                   3
		#  |                    \
		#  |                      4
		#  |                    /
		#  |                   5
		#  |                   |
		#  |                   |
		#  7 ----------------- 6
		#
		# NOTE: all of these are in local coordinates (i.e. local to tag widget), since this is used at draw time
		# TODO: add corner rounding...
		hh = h / 2.0            # vertical midpoint along which the shape is mirrored 
		th = tw / 2.0           # half of triangle height - for coordinates of its points
		
		points = [
			qcore.QPoint(0,  0),     # 1) top left corner
			qcore.QPoint(cw, 0),     # 2) top right corner
			
			qcore.QPoint(cw, hh-th), # 3) top of triangle
			qcore.QPoint(w,  hh),    # 4) peak of triangle
			qcore.QPoint(cw, hh+th), # 5) bottom of triangle 
			
			qcore.QPoint(cw, h),     # 6) bottom right corner
			qcore.QPoint(0,  h)      # 7) bottom left corner
		]
		self.shape = qgui.QPolygon(points)
	
	
	# Update position of tag relative to host
	# ! Override(TagLabel)
	def updatePosition(self):
		# performed by base horizontal tag type...
		w = self.bounds.width()
		self.update_position(-w)


# Right-Side Tag
class RightSideTag(HorizontalBaseTag):
	# Compute size + dimensions of many aspects of this type of tag
	# ! Override(TagLabel)
	def compute_tag(self):
		# calculate and set basic dimension values
		cw, ch, tw, w, h = self.compute_dimensions(self.tsize + XOFS)
		
		# compute polygon for tag shape
		#     2 ----------------- 1
		#     |                   |
		#     |                   |
		#     3                   |
		#    /                    |
		#   4                     |
		#    \                    |
		#     5                   |
		#     |                   |
		#     |                   |
		#     6 ----------------- 7
		#
		# NOTE: all of these are in local coordinates (i.e. local to tag widget), since this is used at draw time
		# TODO: add corner rounding...
		hh = h / 2.0            # vertical midpoint along which the shape is mirrored 
		th = tw / 2.0           # half of triangle height - for coordinates of its points
		
		points = [
			qcore.QPoint(w,  0),     # 1) top right corner
			qcore.QPoint(tw, 0),     # 2) top left corner
			
			qcore.QPoint(tw, hh-th), # 3) top of triangle
			qcore.QPoint(0,  hh),    # 4) peak of triangle
			qcore.QPoint(tw, hh+th), # 5) bottom of triangle
			
			qcore.QPoint(tw, h),     # 6) bottom left corner
			qcore.QPoint(w,  h),     # 7) bottom right corner			
		]
		self.shape = qgui.QPolygon(points)
	
	
	# Update position of tag relative to host
	# ! Override(TagLabel)
	def updatePosition(self):
		# performed by base horizontal tag type...
		self.update_position(self.host.width())


# Top-Side Tag
class TopSideTag(VerticalBaseTag):
	# Compute size + dimensions of many aspects of this type of tag
	# ! Override(TagLabel)
	def compute_tag(self):
		# calculate and set basic dimension values
		# - above...
		cw, ch, tw, w, h = self.compute_dimensions(0)
		
		# compute polygon for tag shape
		# 
		#  1 ----------------- 2
		#  |                   |
		#  |                   |
		#  7 -----6     4----- 3
		#          \   /  
		#            5
		#
		# NOTE: all of these are in local coordinates (i.e. local to tag widget), since this is used at draw time
		# TODO: add corner rounding...
		hw = w / 2.0            # horizontal midpoint along which the shape is mirrored 
		th = tw / 2.0           # half of triangle height - for coordinates of its points
		
		points = [
			qcore.QPoint(0,  0),     # 1) top left corner
			qcore.QPoint(w,  0),     # 2) top right corner
			
			qcore.QPoint(w, ch),     # 3) bottom right corner
			
			qcore.QPoint(hw+th, ch), # 3) right of triangle
			qcore.QPoint(hw,    h),  # 4) peak of triangle
			qcore.QPoint(hw-th, ch), # 5) left of triangle 
			
			qcore.QPoint(0, ch)      # 7) bottom left corner
		]
		self.shape = qgui.QPolygon(points)
	
	
	# Update position of tag relative to host
	# ! Override(TagLabel)
	def updatePosition(self):
		# performed by base horizontal tag type...
		h = self.bounds.height()
		self.update_position(-h)


######################################################
# Tag Spawner

# Tag Priorities - A simple way of using one of several predefined styles
TAG_STANDARD = -1   # Tooltip default 

TAG_PLAIN  = 0      # Greyish theme...
TAG_INFO   = 1      # Help/Info text indicator

TAG_ERROR  = 2      # Error occurred here - Red Alert
TAG_WARN   = 3      # Warning - Yellow/Orange Alert
TAG_GOOD   = 4      # Everything is going well - Green Alert
TAG_PRIORTY_SPECIAL = 5      # Purple

# ====================================================

# Is responsible for showing a bunch of tags, and deciding when to hide them
# - It can control a bunch of tags which are shown beside more than one widget
class TagSpawner(qcore.QObject):
	# ctor
	# < host: ([QWidget]) list of host widgets to show tags beside
	#
	# < (fading_effects): (bool) animate opacity of tags to get them fading in/out
	# < (opacity): (float) 0 = invisible, 1 = full strength 
	# < (tsize): (TAG_SIZE_SMALL | TAG_SIZE_MEDIUM)
	def __init__(self, hosts, fading_effects=True, opacity=1.0, tsize = TAG_SIZE_MEDIUM):
		super(TagSpawner, self).__init__()
		
		# store list of host widgets
		# NOTE: assumes that each item is only in there once...
		assert(len(hosts) > 0)
		self.hosts = hosts
		
		self.bound_owners = set()         # set of "owner" widgets which have had events bound already...
		
		# init lists of tags
		self.tags = []                    # ([LeftSideTag | ...])
		
		# tag showing state
		self.showing_tags = False         # (bool) are tags currently showing
		# XXX: how do we attach different behaviours for dismissing tags?
		
		# tag options
		self.fading_effects = fading_effects
		self.tag_opacity = opacity
		
		# size of tag triangles
		self.tsize = tsize
		
		# factory method helpers
		self.wordwrapper = None           # (textwrap.TextWrapper)
		
	# Finalise
	# ! This must be called before showing any tags, once set of tags is ready to be shown
	def finalise(self):
		# just calls bind_events()
		self.bind_events()
	
	# Host Management ===============================
	
	# Add a new host
	def addHost(self, host):
		if host not in self.hosts:
			self.hosts.append(host)
	
	# Handle "host" argument to tag-adding functions
	# < arg: (QWidget | int) the host widget itself, or the index to it (default of 0 is convenience for single-host spawners)
	def verify_host(self, arg):
		# if an index, try to fetch it from the existing hosts registry
		if type(arg) is int:
			# index...
			try:
				return self.hosts[arg]
			except:
				raise IndexError, "Index (%d) doesn't point to a known host widget" % (arg)
		else:
			# widget...
			if arg in self.hosts:
				# already exists in registry
				return arg
			else:
				# add to registry...
				self.addHost(arg)
				return arg
	
	# Tag Adding ====================================
	
	# Add a given tag to spawner
	# < tag: (LeftSideTag | ...)
	def addTag(self, tag):
		assert(tag not in self.tags)
		self.tags.append(tag)
	
	
	
	# Add a new tag to left hand side of widget
	# < content: (QWidget) widget that tag holds/displays
	# < (yco): (Num | None) the y-offset from top of host widget, or None to have this computed automatically as midpoint of host
	# < (priority): (TAG_*) predefined theming options to apply
	# < (host): (QWidget | int) the host widget itself, or the index to it (default of 0 is convenience for single-host spawners)
	# < (tsize): (TAG_SIZE_* | None) when None, the default triangle size is used...
	# > returns: (LeftSideTag | ...) tag added
	def addTagToLeft(self, content, yco=None, priority=TAG_STANDARD, host=0, tsize=None):
		# get theming options for priority
		fill, border = self.getPriorityColors(priority)
		
		# deal with host-related issues
		host_w = self.verify_host(host)
		
		if yco is None:
			yco = host_w.height() / 2.0
			#print "override yco = %d %d" % (yco, host_w.height())
			
		# triangle size
		if tsize is None:
			tsize = self.tsize
		
		# create and add new tag
		tag = LeftSideTag(host_w, yco, content, fill, border, 
		                  tsize=tsize, 
		                  fading_effects=self.fading_effects)
		self.addTag(tag)
		
		return tag
		
	# Add a new tag to right hand side of widget
	# < content: (QWidget) widget that tag holds/displays
	# < (yco): (Num | None) the y-offset from top of host widget, or None to have this computed automatically as midpoint of host
	# < (priority): (TAG_*) predefined theming options to apply
	# < (host): (QWidget | int) the host widget itself, or the index to it (default of 0 is convenience for single-host spawners)
	# < (tsize): (TAG_SIZE_* | None) when None, the default triangle size is used...
	# > returns: (LeftSideTag | ...) tag added
	def addTagToRight(self, content, yco=None, priority=TAG_STANDARD, host=0, tsize=None):
		# get theming options for priority
		fill, border = self.getPriorityColors(priority)
		
		# deal with host-related issues
		host_w = self.verify_host(host)
		
		if yco is None:
			yco = host_w.height() / 2.0
			#print "override yco = %d %d" % (yco, host_w.height())
			
		# triangle size
		if tsize is None:
			tsize = self.tsize
		
		# create and add new tag
		tag = RightSideTag(host_w, yco, content, fill, border, 
		                   tsize=tsize, 
		                   fading_effects=self.fading_effects)
		self.addTag(tag)
		
		return tag
		
		
	# Add a new tag above widget
	# < content: (QWidget) widget that tag holds/displays
	# < (xco): (Num | None) the x-offset from top of host widget, or None to have this computed automatically as midpoint of host
	# < (priority): (TAG_*) predefined theming options to apply
	# < (host): (QWidget | int) the host widget itself, or the index to it (default of 0 is convenience for single-host spawners)
	# < (tsize): (TAG_SIZE_* | None) when None, the default triangle size is used...
	# > returns: (LeftSideTag | ...) tag added
	def addTagToTop(self, content, xco=None, priority=TAG_STANDARD, host=0, tsize=None):
		# get theming options for priority
		fill, border = self.getPriorityColors(priority)
		
		# deal with host-related issues
		host_w = self.verify_host(host)
		
		if xco is None:
			xco = host_w.width() / 2.0
			#print "override xco = %d %d" % (xco, host_w.width())
			
		# triangle size
		if tsize is None:
			tsize = self.tsize
		
		# create and add new tag
		tag = TopSideTag(host_w, xco, content, fill, border, 
		                  tsize=tsize, 
		                  fading_effects=self.fading_effects)
		self.addTag(tag)
		
		return tag
		
	
	# Prepare text label for display
	# This specifically performs word-wrapping on long text-strings
	#
	# < text: (str) the piece of text to sanitise
	# < sep: (str) Usually '<br />' or '\n' - the separator used for dividing up long string
	def prepareLongText(self, text, sep):
		# create new word-wrapper if none in use yet
		if self.wordwrapper is None:
			self.wordwrapper = textwrap.TextWrapper(width=55)
			
		# split up text into smaller chunks, then rejoin
		return sep.join(self.wordwrapper.wrap(text))
	
	# Build text label for textual label methods
	# < text: (str) Text to display at given y-offset
	# < (priority): (TAG_*) predefined theming options to apply
	# < (bold): (bool) render text in bold font
	def makeTextLabel(self, text, priority=TAG_STANDARD, bold=False):		
		# create a label
		lbl = qgui.QLabel(text)
		
		# make font bold?
		if bold:
			font = lbl.font()
			font.setBold(True)
			lbl.setFont(font)
		
		# adjust font color to be readable...
		if priority not in (TAG_STANDARD, TAG_PLAIN):
			lbl.setStyleSheet("QLabel { color: white; }")
		
		return lbl
	
	# Add a new textual label/tag to left hand side of widget
	# < text: (str) Text to display at given y-offset
	# < (yco): (Num | None) the y-offset from top of host widget, or None to have this computed automatically as midpoint of host
	# < (priority): (TAG_*) predefined theming options to apply
	# < (host): (QWidget | int) the host widget itself, or the index to it (default of 0 is convenience for single-host spawners)
	# < (bold): (bool) render text in bold font
	# < (tsize): (TAG_SIZE_* or None) when None, the default triangle size is used...
	# > returns: (LeftSideTag | ...) tag added
	def addTextToLeft(self, text, yco=None, priority=TAG_STANDARD, host=0, bold=False, tsize=None):
		lbl = self.makeTextLabel(text, priority, bold)
		tag = self.addTagToLeft(lbl, yco, priority, host, tsize)
		
		return tag
	
	# Add a new textual label/tag to right hand side of widget
	# < text: (str) Text to display at given y-offset
	# < (yco): (Num | None) the y-offset from top of host widget, or None to have this computed automatically as midpoint of host
	# < (priority): (TAG_*) predefined theming options to apply
	# < (host): (QWidget | int) the host widget itself, or the index to it (default of 0 is convenience for single-host spawners)
	# < (bold): (bool) render text in bold font
	# < (tsize): (TAG_SIZE_* or None) when None, the default triangle size is used...
	# > returns: (LeftSideTag | ...) tag added
	def addTextToRight(self, text, yco=None, priority=TAG_STANDARD, host=0, bold=False, tsize=None):
		lbl = self.makeTextLabel(text, priority, bold)
		tag = self.addTagToRight(lbl, yco, priority, host, tsize)
		
		return tag
		
	# Add a new textual label/tag above widget
	# < text: (str) Text to display at given y-offset
	# < (xco): (Num | None) the x-offset from left-side of host widget, or None to have this computed automatically as midpoint of host
	# < (priority): (TAG_*) predefined theming options to apply
	# < (host): (QWidget | int) the host widget itself, or the index to it (default of 0 is convenience for single-host spawners)
	# < (bold): (bool) render text in bold font
	# < (tsize): (TAG_SIZE_* or None) when None, the default triangle size is used...
	# > returns: (TopSideTag | ...) tag added
	def addTextToTop(self, text, yco=None, priority=TAG_STANDARD, host=0, bold=False, tsize=None):
		lbl = self.makeTextLabel(text, priority, bold)
		tag = self.addTagToTop(lbl, yco, priority, host, tsize)
		
		return tag
	
	# Tag Priorities ===============================
	
	# Get fill and border for tag priority
	# < priority: (TAG_*)
	# > returns: (QBrush, QPen)
	def getPriorityColors(self, priority):
		# "standard" - no gradients - use for long blurbs of text
		if priority == TAG_STANDARD:
			# XXX: this will cause the defaults in the tag to get triggered...
			fill = border = None
		else:
			# get base color
			if priority == TAG_ERROR:
				base_color = qgui.QColor(204, 0, 0)    # red
			elif priority == TAG_WARN:
				base_color = qgui.QColor(245, 121, 0)  # orange
			elif priority == TAG_GOOD:
				base_color = qgui.QColor(78, 154, 6)   # green
			elif priority == TAG_INFO:
				base_color = qgui.QColor(52, 101, 164) # dark blue
			elif priority == TAG_PRIORTY_SPECIAL:
				base_color = qgui.QColor(117, 80, 123) # purple
			elif priority == TAG_PLAIN:
				#base_color = qgui.QColor(138, 226, 52) # light green
				#base_color = qgui.QColor(114, 159, 207) # light blue
				base_color = qgui.QColor(220, 220, 245) # light blue-gray
				
			# apply opacity
			base_color.setAlpha(min(255, max(0, self.tag_opacity * 255)))
			
			# create gradient based on this base color
			# NOTE: the coordinates for this will be set when setting the tag extents
			fill = qgui.QLinearGradient()
			fill.setColorAt(0.0, base_color.lighter()) # TOP 
			fill.setColorAt(1.0, base_color.darker())  # BOTTOM
			
			# create a pen based on base color - is usually a bit darker
			bcol = base_color.darker().darker()
			if self.tag_opacity < 0.8: 
				bcol.setAlpha(245)   # should be quite solid, even if rest isn't
			
			border = qgui.QPen(bcol, 1)
		
		# return colors
		return fill, border
	
	# Tag Management ===============================
	
	# Return number of tags
	def __len__(self):
		return len(self.tags)
		
	# Check if there are any tags in spawner
	# > returns: (bool)
	def isEmpty(self):
		return len(self.tags) == 0
	
	
	# Hide and free all tags
	def clear(self):
		for tag in self.tags:
			tag.hideTag(immediately=True)
			tag.deleteLater()
		
		self.tags = []
		self.showing_tags = False
	
	# Hide all tags
	def hideAll(self, immediately=False):
		#print "Hiding tags..."
		for tag in self.tags:
			tag.hideTag(immediately)
			
		self.showing_tags = False
	
	
	# Show all tags in one go...
	def showAll(self, immediately=False):
		# always update tag locations, just in case they widget has moved...
		self.updateTagLocations()
		
		# now start showing tags...
		#print "Showing tags..."
		for tag in self.tags:
			tag.showTag(immediately)
			
		self.showing_tags = True
	
	
	# Show tags one-by-one with a time delay
	# < delay: (int - ms) time in milliseconds before next tag gets shown
	def showSequentially(self, delay=500):
		assert(delay >= 1)
		
		#print "Sequentially Showing Targets..."
		# get iterator over tags
		tagIter = iter(self.tags)
		
		# setup timer
		showTimer = qcore.QTimer(self)
		showTimer.setInterval(delay)
		
		# callback to fetch next tag and show it
		# NOTE: only update position of tags right before showing it, 
		# just in case user moves/resizes the window midway
		def showNextTag():
			try:
				tag = tagIter.next()
				tag.updatePosition()
				tag.showTag()
			except StopIteration:
				showTimer.stop()
				self.showing_tags = True
				
		# attach to timer and run...
		showTimer.timeout.connect(showNextTag)
		
		showNextTag()
		showTimer.start()
	
	
	# Update positions of all tags relative to widget (esp. after host widget size changed)
	def updateTagLocations(self):
		for tag in self.tags:
			tag.updatePosition()
		
	# Event Filters ================================
			
	# Register self as event handler (eventFilter() below gets called)
	# on hosts so that our tags can respond to changes accordingly
	def bind_events(self):
		# extract set of all hosts + parent-windows
		# NOTE: we extract a set here to avoid inefficiencies adding these multiple times
		owners = set()
		
		for host in self.hosts:
			# obviously on host, since when it moves/resizes we definitely want to know
			owners.add(host)
			
			# also bind host's window, since that can affect placement of host without it knowing
			owners.add(host.window())
			
		# install event filters
		# - but only if this owner hasn't already been registered...
		for owner in owners:
			if owner not in self.bound_owners:
				owner.installEventFilter(self)
		
		# update set of bound owners...
		self.bound_owners = owners
		
	# Unbind events
	def closeEvent(self, evt):
		#print "Unbinding events"
		for owner in self.bound_owners:
			try:
				owner.removeEventFilter(self)
			except:
				pass
		
		qcore.QObject.closeEvent(self, evt)
		
	
	# Filter host's events to grab hints that we're interested in
	# < obj: (QObject) object that event came from
	# < evt: (QEvent) type of event
	# > returns: (bool) True if we've swallowed an event; False otherwise
	def eventFilter(self, obj, evt):
		etype = evt.type()
		
		if etype in (qcore.QEvent.Resize, qcore.QEvent.Move):
			# update tag positions when host (or perhaps even its owner changes)
			# so that we're still pointing in the right places
			self.updateTagLocations()
		elif etype == qcore.QEvent.WindowDeactivate:
			# when window deactivates, we need to hide...
			self.hideAll(immediately=True)
		elif etype == qcore.QEvent.WindowActivate:
			# window came back - show again
			self.showAll(immediately=True)
		
		# never eat any events...
		return False

######################################################
 
