# Addressbar
# Author: Joshua Leung

"""
Addressbar

== The Vision ==
* Paths should be represented in the "breadcrumbs" style,
  which allows navigation to different subtrees by jumping
  in at any point and changing to a subdirectory there
  
* "Important" landmarks within the paths should be highlighted
  to draw attention to them, but also to make it clearer where
  you are at a given point in time. 
  
  * The potential exists for some flattening to take place here
    to elide parts of longer names and/or hide some of the gory 
    details of deep paths	
  
* It should be possible to manually edit the path still

* Wildcards should be able to be used in constructing the paths,
  facilitating navigation by wildcards only
"""

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

##################################################
# AddressBox

# for now, just use a humble line-edit will have to do...
# FIXME: this is just a placeholder... we need a proper breadcrumbs representation!
class AddressBox(qgui.QLineEdit):
	# custom signal emitted when the text changes
	goToLocation = qcore.pyqtSignal(str)
	
	# ctor
	def __init__(self):
		# default widget appearance
		qgui.QLineEdit.__init__(self)
		self.setPlaceholderText("Go to folder...")
		
		# set decoration icon
		# XXX: this is extremely hacky!
		ficon = qgui.QIcon.fromTheme("folder", qgui.QIcon(":/images/folder.png"))
		self.setStyleSheet("QLineEdit { padding: 2px; padding-left: 20px; height: 18px; }")
		
		self.iconLbl = qgui.QLabel(self)
		self.iconLbl.setCursor(qcore.Qt.ArrowCursor)
		self.iconLbl.setPixmap(ficon.pixmap(16, 16))
		self.iconLbl.move(3, 2)
		
		# attach signal to get fired when enter/editing finished
		self.editingFinished.connect(self.on_EditingFinished)
		
	# Helper to pipe editing finished to send a "goToLocation" custom signal
	def on_EditingFinished(self):
		self.goToLocation.emit(self.text())

##################################################
# AddressBar

# AddressBar + Refresh
class AddressBar(qgui.QWidget):
	# ctor
	def __init__(self):
		qgui.QWidget.__init__(self)
		
		# for later...
		self.navmodel = None
		
		# addressbox
		self.addressbox = AddressBox()
		
		aboxShortcut = qgui.QShortcut(qgui.QKeySequence.fromString("Ctrl+L"), self)
		aboxShortcut.activated.connect(self.addressbox.setFocus)
		
		# refresh button
		self.refreshBut = qgui.QToolButton()
		self.refreshBut.setIcon(qgui.QIcon(":/images/refresh.png"))
		self.refreshBut.setIconSize(qcore.QSize(15, 15)) # XXX: it should be the box that's larger...
		self.refreshBut.setToolTip("Refresh") # XXX: show the actual path we'd refresh...
		self.refreshBut.setStyleSheet("QToolButton { padding: 2px; }")
		self.refreshBut.setShortcut(qgui.QKeySequence(qgui.QKeySequence.Refresh))
		
		# layout manager
		hbox = qgui.QHBoxLayout()
		
		hbox.setContentsMargins(0,0,0,0)
		hbox.setSpacing(0)
		hbox.setMargin(0)
		
		hbox.addWidget(self.addressbox)
		hbox.addWidget(self.refreshBut)
		
		self.setLayout(hbox)
		
	# Navigation Model Support ====================================
		
	# Attach navigation model
	def attachNavigationModel(self, navmodel):
		# store reference
		self.navmodel = navmodel
		
		# TODO: on user input, display some options for possible targets
		
		# once user input ends, go to target
		self.addressbox.goToLocation.connect(self.navmodel.setLocation)
		
		# on location changed, update path
		self.navmodel.locationChanged.connect(self.on_location_changed)
		
	# helper callback for updating contents of addressbox in response to current location in navbar changing
	def on_location_changed(self):
		# update the path
		# XXX: AddressBox should probably handle this itself
		path = self.navmodel.location.path(fullPath=False, asStr=True)
		self.addressbox.setText(path)

##################################################
