# Interactive Full Tree Visualisation
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import bisect
from math import *

from fileshell.model import fsdb
from fileshell.model.fsutils import *
from fileshell.time.timeRange import *

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

#############################################
# Summary Widget

# item in the treelist
class FsTreeListItem(object):
	__slots__ = ('name', 'depth', 'data',
				 'length', 'color', 
				 'tMult', 'important', 'filterOk',
				 'parItem')
	
	# ctor
	def __init__(self, name, depth, data, parItem):
		# id info
		self.name = name
		self.depth = depth
		
		# data being represented
		self.data = data
		
		# "parent" list item
		self.parItem = parItem
		
		# visualisation data
		self.length = len(self.name) 	# length of line - starts from length of name
		self.color = [0, 0, 0] 		 	# all use black as default (colors are 0-255 range for PyQt)
		
		self.tMult = 1.0				# multiplying factor - used to emphasize important items (by time)
		self.important = False			# is item an important target (i.e. appears in consecutive bins)
		self.filterOk = False			# does item's contents match filter requirements (namely, search-text)

# ============================================

# Size of "Hit Buckets"
HITBUCKET_SIZE = 10		# number of pixels that hit bucket occupies
HITBUCKET_HIGH = 10		# number of pixels that target gets displayed as

# "Hit Bucket"
#
# Used to group a bunch of closely related items together so that they can be ranked
# and only the most salient one chosen
class HitBucket(object):
	__slots__ = (
		'target',	# (FsTreeListItem) most salient item in the bucket that should be chosen
		'items',	# ([FsTreeListItem]) list of items that fall in this region
		
		'filters',	# (FilterModel) filters that currently apply
	)
	
	# ctor
	# < filters: (FilterModel)
	def __init__(self, filters):
		# init local bucket info
		self.target = None
		self.items = []
		
		# store filters for later usage
		self.filters = filters
		
	# update the bucket's selected item
	def update_target(self):
		# find item with the newest timestamp that falls within filter range
		# XXX: if there are multiple with the same timestamp, then we should consider their depth - select parent if needed
		def key_fn(item):
			t = item.data.mtime() # XXX: should cache this in the item...
			
			# only consider if time is in range, unless nothing else works
			inRange = (self.filters.tmin <= t <= self.filters.tmax)
			
			# also, include factor for search-relevance (if applicable)
			if self.filters.search_text:
				# only if the item is already in range, should we check if the name works...
				if inRange:
					filter_txt = self.filters.search_text  # cache, for quicker access (if we later do some looping)
					
					# item must be in range AND match the text filter
					for nameItem in ([item] + item.data.files):
						# match found...
						# NOTE: this is just the "brute-force" exact match version - but it's slightly faster...
						if filter_txt in nameItem.name:
							item.filterOk = True
							break;
					else:
						# no matches found
						item.filterOk = False
				else:
					# not in range - it doesn't matter if it matches or not!
					item.filterOk = False
			else:
				# always works - it is irrelevant...
				item.filterOk = True
			
			
			# return a bunch of factors which help range the result - from most to least important...
			return (inRange, item.filterOk, item.tMult, t)
		
		# set the new target
		self.target = max(self.items, key=key_fn)

# ============================================

# Default tooltip for footprints
FootprintsHelpText = """\
<p style='white-space:pre'>
<b>SCOFT Bar:</b>
This view displays your entire file system, allowing you to quickly jump anywhere
on your folders hierarchy with very little effort.

To use this:
 - Click a tag to jump straight to the tagged folder
    You can use the <i>time slider</i> to change the range of tags displayed
 
 - Hold down the <i>Ctrl</i> key and slowly move your mouse to explore the
    folder structure. The folder and its children will be highlighted. <br />
    HINT: <i>Left/Right</i> movement controls the selection accuracy
</p>
"""

# SCOFT - Spatially COnsistent Folders Thumbnail
#
# Displays entire filesystem as a series of lines, showing the full 
# tree-like structure of the filesystem to allow rapid access to items
class FsOverviewBar(qgui.QWidget):
	# Tooltip to display when no targets are selected
	DEFAULT_TOOLTIP = FootprintsHelpText
	
	# Something was clicked on - use to update related view accordingly
	# < index: (int) index of item clicked on
	itemClicked = qcore.pyqtSignal(int)
	
	# Target entered/lost
	# < yco: (int) relative vertical coordinate of the item entered (use to get the range)
	targetEntered = qcore.pyqtSignal(int)
	targetLost    = qcore.pyqtSignal()
	
	# Mouse leaving widget - trigger for timing out the floaters
	mouseExit     = qcore.pyqtSignal()
	
	# ------------------------------------------
	
	# ctor
	# < fs_model: (FsModel) file system model
	# < filters: (FilterModel) filtering info mediator
	# < lens_spawner: (FsVizLensSpawner) floating lens
	# < (indent): (int) number of pixels to indent each level
	def __init__(self, fs_model, filters, lens_spawner, indent=3):
		qgui.QWidget.__init__(self)
		
		# widget configuration
		self.setMinimumSize(50, 1)
		self.setMouseTracking(True)         # for mousemove highlighting to work
		
		# own settings
		self.indent = indent		        # pixels each level is indented by
		
		self.highlight_index = -1	        # index (full-tree model) for row/subtree that is highlighted (under cursor)
		self.selected_index  = -1	        # index (full-tree model) for subtree that is selected
		
		self.flash_index     = -1           # index (full-tree model) for row/subtree that should be flashed (nothing shown means no flash)
		self.flash_alpha     = 0            # (float) opacity of flash to draw - 0 = invisible
		
		self.sticky_tars = False	        # do we make it easier for users to select targets?
		
		# floating lens vs tooltips
		# NOTE: having a lens and showing tooltips is mutually exclusive, 
		# since they fight for space, and end up overlapping
		self.show_tooltips = (not lens_spawner)       # are tooltips shown when mousing over?
		self.lens_spawner = lens_spawner              # reference to lens spawner (if provided)
		
		if lens_spawner:
			lens_spawner.attachToHost(self)
		
		# underlying data models
		self.fs_model = fs_model
		self.filters = filters
		
	# ------------------------------------------
		
	# build display model
	# XXX: we need to distinguish between full model and display model
	def build_display(self):
		# 1) flatten tree out to form a flat list of all the items
		# NOTE: we always clear out the display model when we do this
		print "Building display model..."
		self.clear_model()
		self.build_model()
		
		# 2) update coloring
		print "Binning display model..."
		self.build_bins()
		
		print "Timestamping display model..."
		self.update_filters()
	
	# ------------------------------------------
	
	# Clear internal model data - so that model building starts from a clean slate
	# NOTE: override this to knock out model rebuilding when dealing with proxies...
	def clear_model(self):
		self.model = []			# ([FsTreeListItem]) each item that can potentially show up in the tree
		self.bins = []			# ([HitBucket]) most promising FsTreeListItem per pixel row
		
		self.max_depth = 0		# max depth of recursion
		self.max_len = 0		# max length (including indents) of items
	
	# Build internal model (by traversing file system)
	def build_model(self):
		# flatten the trees rooted at the folders given
		for folder in self.fs_model.tree:
			folder.traverseFolders(self.flatten_folders, self.model)
	
	
	# callback to flatten the folder hierarchy to a list (in self.model)
	# TODO: add support for not including "hidden" items?
	def flatten_folders(self, folder, parent, depth, data):
		# find parent item in the list
		# NOTE: inefficient, but shouldn't be too bad in most cases (assuming we don't have a "big blob" problem)
		if parent:
			for parItem in reversed(data):
				if parItem.data == parent:
					break
			else:
				parItem = None
		else:
			parItem = None
		
		# store folder
		item = FsTreeListItem(folder.name, depth, folder, parItem)
		data.append(item)
		
		# adjust extents
		self.max_depth = max(self.max_depth, depth)
		self.max_len   = max(self.max_len,   depth*self.indent + item.length)			
	
	# ------------------------------------------
		
	# build bins for clustering most salient items at each position
	def build_bins(self):
		w, h, high = self.calc_item_dimensions()
		
		# Initial estimate: one bin per row, or if there are fewer items than rows, whatever that is
		# Second pass: based on this, determine an estimate for the size of buckets, and from this
		#              new figure, compute exactly how many buckets we'd make this way 
		#              (to prevent empty overflows)
		if h < len(self.model):
			numBins = int(h - 1)
		else:
			numBins = len(self.model)
		
		bucketSize = int(ceil(len(self.model) / float(numBins)))
		numBins = int(ceil(len(self.model) / bucketSize))
		
		# number of buckets above/below the current that include a given item
		hsize = HITBUCKET_SIZE / 2
		
		# create bins
		self.bins = [HitBucket(self.filters) for i in range(numBins)]
		
		# add each item to one or more bins
		bb = -1 # incremented on first run
		
		for i, item in enumerate(self.model):
			# base bucket - increment every "bucketsize" steps
			if (i % bucketSize) == 0: 
				bb += 1
			
			# add item to all the bins around here
			for bId in range(bb - hsize, bb + hsize + 1):
				if 0 <= bId < numBins:
					self.bins[bId].items.append(item)
		
	# ------------------------------------------
		
	# iterate over items, adjusting the color of items
	def update_filters(self):
		# update colors of items
		self.update_colors()
		
		# update selected item per bin
		self.update_bins()
		
		# trigger repaint
		self.repaint()
		
	# update color filters
	def update_colors(self):
		# size of time window
		tmodel = self.filters
		
		tmin = tmodel.tmin
		tmax = tmodel.tmax
		
		wsize = tmax - tmin
		
		# calculate gradient timestamps
		tranges = calcTimeGradientStops(tmin, tmax)
		tcolors = getTimeGradientStopPairs(tranges)
		
		# pre-compute colour for "irrelevant" items - not adaptive anymore, but just fixed
		# NOTE: on Linux, for some reason the blending goes wrong and makes things invisible, so we need darker shades...
		if os.name == 'posix':
			irrelevantCol = [120, 120, 120, 180]
		else:
			irrelevantCol = [180, 180, 180, 128]
		
		# iterate over items in list, adjusting their colors
		# NOTE: we could easily parallise this part...
		for item in self.model:
			folder = item.data
			
			# get timestamp
			t = folder.mtime()
			if t is None: t = 0.0
			
			# alpha - oldest should be faintest...
			# 	max(0, val) is to clamp against invalid times...
			val = ((t - tmin) / wsize)
			#val = sqrt(max(0, val))  # <-- ok, but not that clear on ends
			
			aval = 255 * val # time-faded alpha value
			
			# color - oldest should be faintest
			# 	max(0, val) is to clamp against invalid times...
			""" # NOTE: commented out for efficiency, as time-based fading isn't used anymore
			val = ((tmax - t) / wsize)
			val = sqrt(max(0, val))  # <-- ok, but not that clear on ends
			
			cval = 255 * val # time-faded color value
			"""
			
			# find relevant time range that we fall into
			# XXX: do we have the right side of the bin?
			i = bisect.bisect_right(tranges, t) 
			#print t, tmin, tmax, i
			
			# boost relevance factor (tMult) for most recent items
			# FIXME: hardcoded multipliers
			if (i <= 0) or (i > 8):
				item.tMult = 1.0 # no boost - out of range
			else:
				if i >= 7: # >= "3 hrs"
					item.tMult = 20.0
				elif i > 5: # >= "week"
					item.tMult = 15.0
				elif i > 3: # >= "month"
					item.tMult = 10.0
				else: # hardly any, but just enough to be ignored when mousing over
					item.tMult = 2.0
				
			# lookup relevant color to use
			if (t < tmin) or (i <= 0) or (i >= len(tranges)):
				# too old OR too new - just make them all greyish
				#item.color = [cval, cval, cval, val*50]
				item.color = irrelevantCol[:]
			else:
				# fetch the color, and add our own alpha value
				col = tcolors[i][1] # second item is color, first is timestamp
				item.color = [col[0], col[1], col[2], aval+128]
				
			# ensure valid colors - perform clamping
			item.color = [min(255, max(0, x)) for x in item.color]
			
			# clear importance tagging (will be added back later)
			item.important = False
		
	# update item per bin
	def update_bins(self):
		# update item per bin
		for bin in self.bins:
			bin.update_target()
			
		# second pass over the bins - for every continuous stretch of items
		# greater than a threshold, show a prominent target for these
		lastTarget = None
		for bin in self.bins:
			if (lastTarget == bin.target) and (bin.target.tMult > 1.0) and (bin.target.filterOk):
				# tag as important if we encounter more than once
				# AND it is non-trivial
				lastTarget.important = True
			else:
				# change target
				lastTarget = bin.target
		
	# ----------------------------------------
	
	# compute item dimensions
	# > returns: (float, float, float) canvas width, canvas height, item height
	def calc_item_dimensions(self):
		# widget area - excluding borders
		w = float(self.width() - 3)
		h = float(self.height() - 3)
		
		# dimension of items
		high = h / len(self.model)
		
		# return these three aspects
		return w, h, high
		
	# ----------------------------------------
	
	# compute density of items (i.e. items per pixel row)
	# ! Keep this in sync with calc_item_dimensions()
	# > returns: (float) number of items per row
	def calc_item_density(self, item=None):
		# widget area - excluding borders
		h = float(self.height() - 3)
		
		# density is number of items per pixel
		# TODO: return as int?
		if h:
			return len(self.model) / h
		else:
			return 0 # len(self.model)
	
	
	# item index from y-coordinate
	# < y: (int) y-coordinate (i.e. vertical offset from top of widget)
	# > returns: (int) index for accessing item under mouse from self.model
	def index_from_yco(self, y):
		# extents of canvas
		w, h, high = self.calc_item_dimensions()
		
		# from y-co: raw item index is just the inverse calculation used earlier
		# BUT need to clamp to prevent problems with index out of bounds
		index = min( int(y / high),  len(self.model)-1 )
		
		#print "index = ", index, "path = ", self.model[index].data.path
		return index
	
	# y-coordinate for a given index
	# < index: (int) index for accessing item under mouse from self.model
	# > returns: (int) y-coordinate for how far down the widget this is
	def yco_from_index(self, index):
		# extents of canvas
		w, h, high = self.calc_item_dimensions()
		
		# index x height_of_item = y, assuming we are rooted from 0
		yco = int(index * high)
		
		#print "yco = ", yco
		return yco
	
	# ----------------------------------------	
	
	# item index from mouse event
	# < mevt: (QMouseEvent)
	# > returns: (int) index for accessing item under mouse from self.model
	def index_from_mouse__adaptive_browse(self, mevt):
		# extents of canvas
		w, h, high = self.calc_item_dimensions()
		
		# click locations in local coordinates
		x = mevt.x() - 1
		y = mevt.y() - 1
		
		
		# from y-co: raw item index is just the inverse calculation used earlier
		# BUT need to clamp to prevent problems with index out of bounds
		index = min( int(y / high),  len(self.model)-1 )
		
		# now, from x-co: determine the approximate depth-level that x-co implies
		# NOTE: favour lower-depths more to make it easier to browse around the 3-4 level deep area
		#       where most folders reside...
		xfac  = float(x) / w					# [0, 1]
		
		#depth = xfac * self.max_depth				# linear method of calculating depth
		depth = log(xfac + 1) * self.max_depth	# +1 to avoid domain-error, push up low-values a bit to give them priority, scale up to depth levels
		
		depth = max(1,  int(ceil(depth)))      	# root-level access is pretty useless/unexpected, so prevent that...
		
		
		# find a parent of item under index if item directly under cursor is too coarse
		item = self.model[index]
		while (item and item.parItem) and (depth < item.depth):
			item = item.parItem
			
		if self.model[index] is not item:
			index = self.model.index(item)
		#print "index = %d at %d, depth = %d" % (index, self.model[index].depth, depth)
		
		# for now, just single-index accuracy
		#print "index = ", index, "path = ", self.model[index].data.path
		return index
		
	# item index from mouse event
	# < mevt: (QMouseEvent)
	# > returns: (int) index for accessing item under mouse from self.model
	def index_from_mouse__bins(self, mevt):
		# extents of canvas
		w, h, high = self.calc_item_dimensions()
		
		# recalculate high
		high = h / len(self.bins)
		
		# click locations in local coordinates
		#x = mevt.x() - 1
		y = mevt.y() - 1
		
		# from y-co: find index based on which bin we fall into
		# BUT need to clamp to prevent problems with index out of bounds
		bindex = min( int(y / high),  len(self.bins)-1 )
		
		# from the bin, whatever item is it's target, we take that and find its index
		bin = self.bins[bindex]
		index = self.model.index(bin.target)
		
		# to prevent "gray" folders bogging down the cursor, we skip if the gray folder 
		# is not being highlighted with a longer indicator
		if bin.target.tMult == 1.0:
			index = -1
		
		# return
		#print "index = ", index, "bindex = ", bindex, "path = ", self.model[index].data.path()
		return index
	
	
	# item index from mouse event
	# < mevt: (QMouseEvent)
	# > returns: (int) index for accessing item under mouse from self.model
	def index_from_mouse(self, mevt):
		if self.sticky_tars:
			index = self.index_from_mouse__bins(mevt)				# new method
		else:
			index = self.index_from_mouse__adaptive_browse(mevt) 	# old method
		
		return index
		
	# ----------------------------------------
	
	# hook for widget drawing/redrawing
	# TODO: can optimise by only redrawing region as given in evt
	def paintEvent(self, evt):
		p = qgui.QPainter()
		
		p.begin(self)
		
		# configure the painter so that everything will be visible
		# NOTE: we need to set everything manually as Linux does things differently to Windows
		p.setCompositionMode(qgui.QPainter.CompositionMode_SourceOver)
		
		p.setRenderHint(qgui.QPainter.NonCosmeticDefaultPen, True)
		p.setRenderHint(qgui.QPainter.Antialiasing, False)
		p.setRenderHint(qgui.QPainter.HighQualityAntialiasing, False)
		p.setRenderHint(qgui.QPainter.SmoothPixmapTransform, False)
		p.setOpacity(1.0)
		
		# draw summary representation
		self.draw_background(p)
		self.draw_highlight(p)
		self.draw_folders(p)
		
		# draw sticky targets for easier targetting on mouse over (if enabled)
		if self.sticky_tars:
			self.draw_targets(p)
			
		# draw flashing target
		if self.flash_index >= 0:
			self.draw_flashing_hint(p)
			
		# draw activation area for lenses
		if self.lens_spawner:
			self.lens_spawner.hostDrawActivationArea(p)
		
		p.end()
		
	# draw background decorations for widget
	def draw_background(self, p):
		# widget area
		w = self.width()
		h = self.height()
		
		# get color palette
		cs = qgui.QPalette()
		
		# set drawing info, and draw solid fill
		border = qgui.QPen(cs.color(qgui.QPalette.Dark), 1)
		fill = qgui.QBrush(qgui.QColor("white"), qcore.Qt.SolidPattern)
		
		p.setPen(border)
		p.setBrush(fill)
		
		p.drawRect(0, 0, w-1, h-1)
		
	# draw highlight - shows full range that highlighted item affects
	def draw_highlight(self, p):
		cs = qgui.QPalette()
		
		# get dimensions of where to draw the brush
		high = self.calc_item_dimensions()[2]
		w = self.width()
		
		# selected index - selected subtree
		if self.selected_index >= 0:
			# determine number of child items of this
			index = self.selected_index
			numChildren = self.model[index].data.numDescendents()
			
			# update brush color
			p.setPen(qcore.Qt.NoPen)
			
			fill = qgui.QBrush(qgui.QColor(50, 50, 50, 255))
			p.setBrush(fill)
			
			# calculate relevant coordinates of selection rect
			iy =     high * index + 1
			ih = max(high * numChildren, 1) # can't be too small, or else invisible
			
			p.drawRect(1, iy, w-2, ih) 
			
		# highlight item - just the row under the cursor (mouse hover in main list)
		if self.highlight_index >= 0:
			# determine number of child items of this
			index = self.highlight_index
			numChildren = self.model[index].data.numDescendents()
			
			# calculate relevant coordinates of wireframe for highlighted item
			iy =     high * index  +  1
			ih = max(high * numChildren, 1) # can't be too small, or else invisible
			
			# 1) thick shadow
			border = qgui.QPen(cs.color(qgui.QPalette.Shadow), 2)
			p.setPen(border)
			
			hlCol = cs.color(qgui.QPalette.Highlight)
			hlCol.setAlpha(50)
			p.setBrush(hlCol) # very light shading
			
			p.drawRect(2, iy, w-4, ih)
			
			# 2) light highlight
			border = qgui.QPen(cs.color(qgui.QPalette.Light), 1)
			p.setPen(border)
			
			p.setBrush(qcore.Qt.NoBrush) # no shading, to avoid double-up
			
			p.drawRect(1, iy-1, w-3, ih)
			
	# draw flashing highlight for briefly showing item we've navigated to
	def draw_flashing_hint(self, p):
		# get dimensions of where to draw the brush
		high = self.calc_item_dimensions()[2]
		w = self.width()
		
		# determine number of child items of this
		index = self.flash_index
		numChildren = self.model[index].data.numDescendents()
		
		# update brush color
		p.setPen(qcore.Qt.NoPen)
		
		fill = qgui.QBrush(qgui.QColor(255, 255, 0, self.flash_alpha))
		p.setBrush(fill)
		
		# calculate relevant coordinates of selection rect
		iy =     high * index + 1
		ih = max(high * numChildren, 1) # can't be too small, or else invisible
		
		p.drawRect(1, iy, w-2, ih) 
	
	# draw lines indicating folder structure
	def draw_folders(self, p):
		w, h, high = self.calc_item_dimensions()
		
		# pen
		pen = qgui.QPen()
		pen.setWidthF(high)
		pen.setCapStyle(qcore.Qt.RoundCap)
		p.setPen(pen)
		
		fill = qgui.QBrush(qcore.Qt.SolidPattern)
		p.setBrush(fill)
		
		
		# draw items in list
		y = 1
		
		for item in self.model:
			# start is given by width of indent
			# 	+1 is for the border
			xs = item.depth * self.indent + 1
			
			# end is relative to the proportion of the max horizontal length
			# 	- minimum size is set to ensure that all is always visible
			#	- draw important items larger
			if item.important:
				xw = 20
			else:
				xw = max(2, (item.length / self.max_len) * w)
				#xw = 5    # XXX: experimental fixed line width - too strong/blocky effect
			xe = xs + xw
			
			# y-extents
			#	- height of item varies based on salience of item
			# 	- draw important items larger
			if item.important:
				yh = 3
			else:
				yh = high
			
			ys = y - (yh / 2.0)
			#ye = y + yh
			
			# color
			col = qgui.QColor(*item.color)
			
			pen.setColor(col)
			p.setPen(pen)
			fill.setColor(col)
			p.setBrush(fill)
			
			# draw line
			# - for salient targets, draw as rects instead
			if yh > 1:
				p.drawRect(xs, ys,  xw, yh)
			else:
				p.drawLine(xs, y,  xe, y)
			
			# update y - y goes downwards
			y += high
			
	# like draw_folders(), except that we only highlight salient targets based on distance to mouse
	# - currently, we just take the "highlighted" item
	def draw_targets(self, p):
		w, h, high = self.calc_item_dimensions()
		
		# only draw the item at the highlight index
		if self.highlight_index < 0:
			return
			
		# get the item
		item = self.model[self.highlight_index]
		
		# start is given by width of indent
		# 	+1 is for the border
		xs = item.depth * self.indent + 1
		
		# end is relative to the proportion of the max horizontal length
		# 	- take the smaller of the sizes so that we don't overflow over the edge
		#	- tMult is multiplier factor based on newness
		xw = min(w - xs, HITBUCKET_HIGH * item.tMult)
		#xe = xs + xw
		
		# y-extents
		#	- base height of item is the hitbucket size, which is modulated by...
		# 	- tMult, which is a multiplier factor based on newness
		yh = HITBUCKET_HIGH
		
		y  = self.highlight_index * high
		ys = y - (yh / 2.0)
		#ye = y + yh
		
		# color
		col = qgui.QColor(*item.color)
		col.setAlpha(225) # nearly fully visible over everything else
		
		fill = qgui.QBrush(col)
		p.setBrush(fill)
		
		pen = qgui.QPen(qgui.QBrush(col.darker(150)), 1.0, cap=qcore.Qt.RoundCap)
		p.setPen(pen)
		
		# draw as a big juicy rect
		# - rounded corners for a more "click me" affordance
		p.drawRoundedRect(xs, ys,  xw, yh,  2.0, 2.0)
		
	# ----------------------------------------
		
	# generate and set tooltip for highlighted region under cursor (active targets)
	def generate_tooltip(self):
		# only generate tooltip if tooltips are enabled (i.e. when not using lenses at all)
		# XXX: perhaps we can still show these until the lenses are shown?
		if not self.show_tooltips:
			self.setToolTip("") # don't show any tooltips!
			return
		
		# only generate when we're mousing over the widget
		if self.highlight_index < 0:
			#self.setToolTip(FsOverviewBar.DEFAULT_TOOLTIP)
			self.setToolTip("") # don't show anything for now... too distracting
			return
			
		# find item that we're referring to
		item = self.model[self.highlight_index]
		if not item:
			self.setToolTip("") # internal error, so no helptext
			return
			
		# extract some interesting items to display
		folders = item.data.path(fullPath=False)
		
		sep = os.path.sep
		path = sep.join(folders)
		
		if len(folders) > 3:
			# just last two items form the "name"
			
			# TODO: if the last two were very similar, maybe we should use the one above instead?
			#		and if the last these two happened to be part of a single-link chain, then we might want to show key nodes there
			# TODO: review the order and how we present these...
			# TODO: we need relative paths, not the full path when doing this...
			# TODO: shorten very long foldernames (proper ranking algorithm instead could be better for getting predictions)
			
			name = "%s - [<i>%s</i>]" % (folders[-1], folders[-2])
		else:
			# just last part
			name = item.name
			
		# set text
		# - first line is used to ensure that all line-breaks are explicit
		# TODO: do some smart displays which extract out the "salient" features of the path
		tooltip  = "<p style='white-space:pre'>"
		
		tooltip += "<b>%s</b> <br/>" % (name)
		tooltip += "<i>%s</i>" % (path) # XXX: salient feature extraction should come here
		
		tooltip += "</p>"
		
		self.setToolTip(tooltip)
		
	
	# force the tooltip to be shown (on mouse move)
	def force_tooltip(self, event):
		if self.highlight_index >= 0:
			# show current label
			qgui.QToolTip.showText(event.globalPos(), self.toolTip())
		else:
			# hide label for now, until a timer triggers it to show normally
			qgui.QToolTip.hideText()
		
	# --------------------------------------------------
		
	# override generic events to capture the ones we're interested in overriding
	def event(self, event):
		# catch tooltip events so that we respond with less delay
		if event.type() == qcore.QEvent.ToolTip:
			# force tooltip to be displayed normally (help text and other cases get shown via this)			
			qgui.QToolTip.showText(event.globalPos(), self.toolTip())
			return True
		
		return super(FsOverviewBar, self).event(event)
	
	
	# disable sticky when leaving widget (so that previewing in other widgets doesn't look bad)
	def leaveEvent(self, evt):
		self.sticky_tars = False
		self.mouseExit.emit()
		super(FsOverviewBar, self).leaveEvent(evt)
		
	# enable sticky when entering (set default)
	def enterEvent(self, evt):
		self.sticky_tars = True
		super(FsOverviewBar, self).enterEvent(evt)
	
	
	# mouse click event in widget
	def mousePressEvent(self, mevt):
		if mevt.button() == qcore.Qt.LeftButton:
			index = self.index_from_mouse(mevt)
			self.itemClicked.emit(index)
		else:
			super(FsOverviewBar, self).mousePressEvent(mevt)
		
	# mouse-move event in widget
	def mouseMoveEvent(self, mevt):
		# toggle sticky tars
		# - enable by default, and disable only if Ctrl pressed
		self.sticky_tars = not bool(mevt.modifiers() & qcore.Qt.ControlModifier)
		
		# update the highlight index
		old_highlight_index  = self.highlight_index
		self.highlight_index = self.index_from_mouse(mevt)
		
		# set tooltip to mention what folder we've got
		# then force it to be shown immediate
		self.generate_tooltip()
		self.force_tooltip(mevt)
		
		# force repaint of widget at last - faster than updating midway
		self.repaint()
		
		# generate event to say whether we've hit anything or not
		# - only trigger if the highlight index has changed, so that minor bumping of the 
		#   mouse will not cause timer to get reset (i.e. as is the case when using a stylus)
		
		#"""
		if self.highlight_index != old_highlight_index:
			if self.highlight_index >= 0:
				self.targetEntered.emit(mevt.y())
			else:
				# only target lost if moving vertically
				self.targetLost.emit()
				#self.mouseExit.emit()
		#"""
		
		"""
		if self.highlight_index >= 0:
			# a target was gained (maybe a near distance from previous)
			self.targetEntered.emit(mevt.y())
		else:
			# target lost when moving vertically...
			self.targetLost.emit()
		"""
		
		"""
		if self.highlight_index >= 0:
			self.targetEntered.emit(mevt.y())
		"""
		
	# --------------------------------------------------
	
	# context menu
		# show context menu with operations for folder here
	def contextMenuEvent(self, event):
		# create the menu --------------------------------
		# TODO: add all actions in advance, and have them get all their events hooked up in advance too
		menu = qgui.QMenu(self)
		
		# 1) help
		actHelp = menu.addAction("What's this?")
		menu.setDefaultAction(actHelp)
		
		# show menu -----------------------------------------
		
		res = menu.exec_(self.mapToGlobal(event.pos()))
		
		# process events
		if res == actHelp:
			if self.highlight_index >= 0:
				# TODO: include more info about this?
				txt = "This is a tag for a recently modified folder. Click on it to jump straight to that folder"
				#qgui.QMessageBox.information(self, "About SCOFT", txt, 0)
				qgui.QMessageBox.information(self, "About SCOFT", self.DEFAULT_TOOLTIP, 0)
			else:
				qgui.QMessageBox.information(self, "About SCOFT", self.DEFAULT_TOOLTIP, 0)
