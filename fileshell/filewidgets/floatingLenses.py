# Floating "lenses" showing subsets of the tree to allow greater
# when targeting sub-pixel targets...
#
# Author: Joshua Leung
# Date: May 2013

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os
from math import log10

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.theming import stylesheets

#from footprints import FsOverviewBar
#from fishprints import FsOverviewBar

##################################################
# Debug Control

# Perform lens debugging prints
LENS_DEBUG = False

##################################################
# Item-List Role Constants
# NOTE: copied from foldertree.py

# "Index of Visualisation-Item" (int) - User-defined tree item custom data (starts from 32)
IOV = 33

# "Link to original folder" (FolderData) - User-defined tree item custom data (starts from 32)
FolderPtrRole = 34

##################################################
# Floating Lens Base Class
# Based on QFloatingWidget from PyQtTooltipsLib

class FloatingLens(qgui.QWidget):
	# ctor
	# < (parent): (QWidget) widget which owns this floater
	#           = None on Windows, due to control issues
	def __init__(self, parent=None):
		qgui.QWidget.__init__(self, parent, qcore.Qt.ToolTip)
		
		# internal helper structures
		self.hideTimer = qcore.QBasicTimer()
		
		# customise widget to look like a tooltip
		self.setForegroundRole(qgui.QPalette.ToolTipText)
		self.setBackgroundRole(qgui.QPalette.ToolTipBase)
		self.setPalette(qgui.QToolTip.palette()) # XXX: allow substitution from users?
		self.ensurePolished()
		
		self.setWindowOpacity(self.style().styleHint(qgui.QStyle.SH_ToolTipLabel_Opacity, None, self) / 255.0)
		self.setMouseTracking(True)
		
	# Widget Implementation Overrides ---------------
	
	# Draw tooltip background, then contents of tooltip
	# < evt: (QPaintEvent)
	def paintEvent(self, evt):
		# tooltip background
		p = qgui.QStylePainter(self)
		
		opt = qgui.QStyleOptionFrame()
		opt.init(self)
		
		p.drawPrimitive(qgui.QStyle.PE_PanelTipLabel, opt)
		p.end()
		
		# contents...
		qgui.QWidget.paintEvent(self, evt)
	
	
	# Adjust mask on resizing - used for transparency/shadow effects on some platforms
	# < evt: (QResizeEvent)
	def resizeEvent(self, evt):
		frameMask = qgui.QStyleHintReturnMask()
		option = qgui.QStyleOption()
		option.init(self)
		
		if (self.style().styleHint(qgui.QStyle.SH_ToolTip_Mask, option, self, frameMask)):
			self.setMask(frameMask.region)
		
		qgui.QWidget.resizeEvent(self, evt)
	
	# Event Handling Overrides -----------------------
	
	# Timer Event - for hiding the widget after a fixed period of time
	# < evt: (QTimerEvent)
	def timerEvent(self, evt):
		# properly hide widget if timers have expired
		if evt.timerId() == self.hideTimer.timerId():
			# stop timers
			self.resetTimers()
			
			# hide
			self.hideTipImmediately()
		else:
			# otherwise, standard handling applies
			qgui.QWidget.timerEvent(self, evt)
			
	
	# Mouse-move within ourselves - abort any current timers...
	# < evt: (QMouseEvent)
	def mouseMoveEvent(self, evt):
		# cancel timers (for now)
		self.resetTimers()
		
		# pass on events now
		qgui.QWidget.mouseMoveEvent(self, evt)
	
	# Leaving body - start timers for hiding stuff
	def leaveEvent(self, evt):
		# only 'leave event' if we're leaving outside our own bounds
		"""
		mco = qgui.QCursor.pos()
		
		geom = self.geometry()
		tL = geom.topLeft()
		tR = geom.topRight()
		bL = geom.bottomLeft()
		bR = geom.bottomRight()
		
		coordStr = "(%d, %d) - (%d,%d) (%d,%d) // (%d,%d) (%d,%d)" % (mco.x(), mco.y(),
				tL.x(), tL.y(), tR.x(), tR.y(), bL.x(), bL.y(), bR.x(), bL.y())
		
		if self.geometry().contains(mco):
			print "# Leaving Lens - MouseCo still within lens though %s" % (coordStr)
		else:
			print "# Leaving Lens - Mouseco not within lens %s" % (coordStr)
		"""
		
		# schedule eventual hide, if we don't come back in
		self.hideTip()
		
		# pass on events now
		qgui.QWidget.leaveEvent(self, evt)
	
	# Public API ------------------------------------
	
	# Compute the vertical midpoint of own contents
	# ! Individual lenses may override this to balance their own content as needed (i.e. to favour row we're working with)
	# > returns: (float > 0) y-height/offset from top of self that should be centered around the spawn point
	def calcMidpointOffset(self):
		# simply center the widget - 1px offset is slight tweak to ensure that we're lined up correctly
		return self.height() / 2.0 - 1
	
	# Set the (global) placement of the widget on screen
	# < w: (QWidget) the owner widget
	# < yco: (int) how far down the widget space the original target was
	def placeTip(self, w, yco):
		# compute point so that we're in the right place relative to our owner
		# i.e. we're centered vertically alongside the row we're working with
		p  = w.mapToGlobal(w.geometry().topRight())
		
		xofs = -5 # shift a bit over to the left, to overlap the original window to lessen chance of accidental activation
		yofs = yco - self.calcMidpointOffset()
		p += qcore.QPoint(xofs, yofs)
		
		# get screen bounds
		if sys.platform.startswith('darwin'):
			screen = qgui.QApplication.desktop().availableGeometry(self.getTipScreen(p, w))
		else:
			screen = qgui.QApplication.desktop().screenGeometry(self.getTipScreen(p, w))
		
		# clamp within bounds
		if p.x() + self.width() > screen.x() + screen.width():
			p.setX(p.x() - 4 + self.width()) # p.rx()
			
		if p.y() + self.height() > screen.y() + screen.height():
			p.setY(p.y() - 24 + self.height()) # p.ry()
			
		if p.y() < screen.y():
			p.setY(screen.y())
			
		if p.x() + self.width() > screen.x() + screen.width():
			p.setX(screen.x() + screen.width() - self.width())
			
		if p.x() < screen.x():
			p.setX(screen.x())
			
		if p.y() + self.height() > screen.y() + screen.height():
			p.setY(screen.y() + screen.height() - self.height())
		
		# move label to this global point
		self.move(p)
	
	# ...........................
	
	# Reset timers so that they are not ticking over...
	def resetTimers(self):
		self.hideTimer.stop()
	
	# Start fading out the tooltip bubble to hide it
	def hideTip(self):
		if not self.hideTimer.isActive():
			self.hideTimer.start(500, self)
	
	
	# Force tooltip bubble to hide immediately, without fade out delay
	def hideTipImmediately(self):
		self.close()        # to trigger QEvent::Close which stops the animation
	
	# ...........................
		
	# Return which screen (on multi-screen setups) the tooltip appears on
	# < pos: (QPoint) point on screen where tooltip/widget appears
	# < widget: (QWidget) widget that tooltip appears for
	# > returns: (int) screen number/index
	#          >> -1 = if widget does not appear
	def getTipScreen(self, pos, widget):
		if qgui.QApplication.desktop().isVirtualDesktop():
			return qgui.QApplication.desktop().screenNumber(pos)
		else:
			return qgui.QApplication.desktop().screenNumber(widget)


##################################################
# Custom Drawn Lens

# Widget to represent an item within the lens
class FsFlexiLensItem(qgui.QLabel):
	# ctor
	# < lens: (FsFlexiLens) lens that this lives in
	# < host: (FsOverviewViz) SCOFT widget that lens is shown for
	# < fs_item: (FsTreeListItem) the item from SCOFT widget we're representing zoomed-in
	# < index: (int) index for item
	def __init__(self, lens, host, fs_item, index):
		qgui.QLabel.__init__(self, parent=lens)
		
		# store refs
		self.lens = lens
		self.host = host
		self.fs_item = fs_item
		self.index = index
		
		# stub values
		self.background_color = qgui.QColor("white") # base color if all else fails
		self.mouseover = False
		
		# configure theming/display
		self.setup_ui()	
		
	# Setup --------------------------------------------
	
	# setup themeing
	def setup_ui(self):
		# name
		self.setText(self.fs_item.name)
		
		# generic settings
		self.setMouseTracking(True)
		
		# indices for checking purposes
		#curIndex = self.host.highlight_index
		curIndex = self.lens.spawn_index
		
		# grab a whole bunch of shortcuts
		index = self.index
		item = self.fs_item
		
		# theming options
		if index == curIndex:
			# highlight item
			col = qgui.QColor(*item.color)
			col.setAlpha(128) # only half visible
			
			self.setBackgroundColor(col)
			
			# font
			# - bold and slightly smaller
			font = self.font()
			font.setBold(True)
			
			if not item.important:
				# for non-important items, don't promote...
				self.shrink_font(font, -2)
			else:
				# adjust font - needed as Linux default is too large...
				self.shrink_font(font, -1)
				
			self.setFont(font)
			self.setMargin(2)
			
			# hang on to this to ensure we're scrolled correctly
			self.lens.targetCenteredItem = self
			
		# otherwise, if it is a salient target, highlight it too?
		elif item.important:
			col = qgui.QColor(*item.color)
			col.setAlpha(80) # halfway between the two extremes - should stand out, but not be too attention grabbing
			
			self.setBackgroundColor(col)
			
			# smaller font - midsized
			font = self.font()
			self.shrink_font(font, -1)
			self.setFont(font)
			self.setMargin(1)
		
		# simply within range of current pixel
		elif self.lens.start_coindex <= index < self.lens.end_coindex:
			# color same as 
			if curIndex >= 0:
				highlightItem = self.host.model[curIndex]
				col = qgui.QColor(*highlightItem.color)
			else:
				col = qgui.QColor("lightgray") # XXX
			col.setAlpha(25) # really barely there, but should be enough to indicate relationship
			
			self.setBackgroundColor(col)
			
			# smaller font - midsized
			font = self.font()
			#font.setItalic(True)
			font.setBold(True)
			
			self.shrink_font(font, -3)
			
			self.setFont(font)
			self.setMargin(1)
			
		# nearby but non-important
		else:
			# smaller font - smallest
			font = self.font()
			self.shrink_font(font, -5)
			self.setFont(font)
			self.setMargin(0)
			
			# dark-gray text, so that it doesn't jut-out that badly
			self.setStyleSheet("QLabel { color : #444444; }");
	
	# Helper method to shrink fonts
	# XXX: percentage changes?
	def shrink_font(self, font, amount):
		if font.pixelSize() == -1:
			# define in points
			#size = font.pointSize()
			size = 10.0
			font.setPointSize(size + amount)
		else:
			# defined in pixels
			#size = font.pixelSize()
			size = 10
			font.setPixelSize(size + amount)
	
	# Internal Properties ===========================
	
	# Get background color
	# > returns: (QColor)
	def backgroundColor(self):
		if self.mouseover:
			# highlight color
			cs = qgui.QPalette()
			
			hcol = cs.color(qgui.QPalette.Highlight)
			hcol.setAlpha(120)
			
			return hcol
		else:
			# plain background color
			return self.background_color
	
	# Set Background color
	# < col: (QColor)
	def setBackgroundColor(self, col):
		self.background_color = col
	
	# Drawing =======================================
	# NOTE: These are called from the owner
	
	# Draw background color - called from FsFlexiLens.paintEvent()
	def draw_background(self, p):
		# drawing area
		w = self.lens.width() - 2  # full width of lens (excluding borders)
		h = self.height()          # own height still
		
		x = 1                      # just offset from border
		y = self.pos().y()         # NOTE: this is still in lens space...
		
		# set drawing info, and draw solid fill
		col = self.backgroundColor()
		
		fill = qgui.QBrush(col, qcore.Qt.SolidPattern)
		p.setBrush(fill)
		
		if self.mouseover:
			# draw selection bubble...
			# 1) darker border
			col2 = qgui.QColor(col)
			col2.setAlpha(255)
			
			p.setPen(qgui.QPen(col2, 1))
			p.drawRoundedRect(x, y, w, h, 2, 2)
			
			# 2) lighter border
			col3 = col2.lighter(160)
			p.setBrush(qcore.Qt.NoBrush)
			
			p.setPen(qgui.QPen(col3, 1))
			p.drawRoundedRect(x+1, y+1, w-2, h-2, 4, 4)
		else:
			# draw plain background highlight
			p.setPen(qcore.Qt.NoPen)
			p.drawRect(x, y, w, h)


# Custom layout manager for use within custom lens
# NOTE: this is based on the FlowLayout example
class FsLensLayout(qgui.QLayout):
	def __init__(self, parent=None, indent=10):
		super(FsLensLayout, self).__init__(parent)
		
		# we need things to be as tightly packed as possible
		self.setMargin(0)
		self.setSpacing(indent)  # XXX: abuse "spacing" for indent size, since we don't use it
		
		# list of items belonging to this lens
		self.itemList = []
	
	def __del__(self):
		item = self.takeAt(0)
		while item:
			item = self.takeAt(0)
	
	def addItem(self, item):
		assert(isinstance(item, FsFlexiLensItem))
		
		wrapper = qgui.QWidgetItem(item)
		self.itemList.append(wrapper)
	
	def count(self):
		return len(self.itemList)
	
	def itemAt(self, index):
		if index >= 0 and index < len(self.itemList):
			return self.itemList[index]
		return None
	
	def takeAt(self, index):
		if index >= 0 and index < len(self.itemList):
			return self.itemList.pop(index)
		return None
	
	def expandingDirections(self):
		#return qcore.Qt.Orientations(qcore.Qt.Orientation(0))
		return qcore.Qt.Orientations(qcore.Qt.Horizontal | qcore.Qt.Vertical) # XXX?
	
	def hasHeightForWidth(self):
		return False
	
	def heightForWidth(self, width):
		# XXX: not implemented
		print "heightForWidth() Not Implemented"
		return 0
	
	def setGeometry(self, rect):
		super(FsLensLayout, self).setGeometry(rect)
		self.doLayout(rect)
	
	def sizeHint(self):
		minSize = self.minimumSize()
		#print "sizehint = %d %d" % (minSize.width(), minSize.height())
		return self.minimumSize()
	
	def minimumSize(self):
		# compute extents of area...
		# TODO: need to cache this!
		width, height = self.doLayout(None, do_updates=True)
		
		# create size object
		size = qcore.QSize(width, height)
		
		size += qcore.QSize(2, 2) # borders
		size += qcore.QSize(2 * self.margin(), 2 * self.margin())
		
		return size
	
	def doLayout(self, rect, do_updates=False):
		if rect:
			x = rect.x() + 1 # avoid the border
			y = rect.y() + 1 # avoid the border
			
			sy = rect.y() + 1
		else:
			x = 1
			y = 1
			
			sy = 0
		
		indent = self.spacing() # we abuse spacing for size of indent steps, since we don't use that anyway
		maxWidth = 200          # accumulator for maximum width of area - initialised to a minimum width
		
		# first pass - compute position of items, and also compute size of layout bounds...
		for wrapper in self.itemList:
			item = wrapper.widget()
			
			# compute x-offset based on indent size
			xco = x + (indent * item.fs_item.depth)
			
			# set position of label
			itemSize = item.sizeHint()
			if do_updates:
				item.setGeometry(qcore.QRect(qcore.QPoint(xco, y),
											 itemSize))
			
			# compute maximum width
			maxWidth = max(maxWidth, xco + itemSize.width())
			
			# compute new minimum y-co for next item to use
			y += itemSize.height()
		
		# return extents
		return maxWidth, (y - sy)


# Like FsDetailLens, but with custom drawn body for greater control
class FsFlexiLens(FloatingLens):
	LENS_RANGE_SIZE = None        # default number of items on either side of current item to show
	
	# ctor
	# < spawner: (FsVizLensSpawner) spawner that created us
	# < host: (FsOverviewViz) SCOFT widget we are spawned from
	# < spawn_index: (int) index used at the time that lens was spawned
	# < spawn_yco: (int) offset from start of host dataset which we display
	def __init__(self, spawner, host, spawn_index, spawn_yco):
		# parent is either host or screen - screen for Windows, otherwise window gets raised...
		if os.name == 'nt':
			parent = qgui.QApplication.desktop().screen(self.getTipScreen(host, spawn_yco))
		else:
			parent = host
		
		FloatingLens.__init__(self, parent)
		
		# store range settings
		self.spawner = spawner
		self.host = host
		self.spawn_index = spawn_index
		self.spawn_yco = spawn_yco
		
		# create core widget
		self.setup_ui()
		
		# add our eventFilter() method as a filter 
		self.bind_eventFilter()
	
	# ------------------------------------------------
		
	# Build and initialise widget used to display list of items
	def setup_ui(self):
		# 1) compute range of items to show
		# - we include all the items immediately within the pixel, 
		# - we also include those in the pixels either side 
		#   (for flexibility and to accomodate low precision of pointing)
		# XXX: must clamp these after extending to ensure they're still valid!
		
		#curIndex = self.host.highlight_index
		curIndex = self.spawn_index # XXX: we use this one instead, since highlight could have changed in meantime due to mouse movements
		
		item = self.host.model[curIndex]
		
		# use curIndex, and take a number of items either side of this to ensure that it is centered...
		density = self.host.calc_item_density(item)          # how many items per pixel do we have
		
		if FsFlexiLens.LENS_RANGE_SIZE is None:
			# compute optimal size for this dataset on first use
			guess   = 10*log10(len(self.host.model) * 0.01)
			optimal = max(10, guess)  # <--- 10 is "baseline" for minimum number of items to make it worthwhile 
			
			FsFlexiLens.LENS_RANGE_SIZE = int(optimal)
			
		neighbourhood_range = FsFlexiLens.LENS_RANGE_SIZE    # number of items either side to show
		
		self.start_idx = curIndex - neighbourhood_range
		self.end_idx   = curIndex + neighbourhood_range + 1
		
		# - clamp indices to range
		self.start_idx = max(0,            self.start_idx)
		self.end_idx   = min(self.end_idx, len(self.host.model)-1)
		
		# 2) now compute the range of indices which are coincident with highlight_index
		# - Highlighted index may actually be in the middle of a pixel, so we need the real
		#   y-coordinate for it to get the first index that occurs on that pixel
		# - From there, the width of the indices band for that pixel will be the item density
		highlight_yco      = self.host.yco_from_index(curIndex)
		
		self.start_coindex = self.host.index_from_yco(highlight_yco)
		
		if self.start_coindex >= 0:
			self.end_coindex = min(self.start_coindex + density, len(self.host.model)-1)
		else:
			self.end_coindex = -1
		
		# only keep the coindex range if the highlight index is on the edge of it (or within)
		if not (self.start_coindex-1 <= curIndex <= self.end_coindex+1):
			#print "Dropping coindices - range (%s, %s) for %s" % (self.start_coindex, self.end_coindex, curIndex)
			self.start_coindex = -1
			self.end_coindex = -1
		
		# 3) create widgets to show contents
		self.build_viz()
		
	# start building visualisation
	def build_viz(self):
		# custom layout manager for laying out these items
		layout = FsLensLayout(indent=10)
		self.setLayout(layout)
		
		# reference for the highlighted item, so that we can scroll to it once we're ready
		self.targetCenteredItem = None
		
		# list of items added
		self.viz = []
		
		# item which spawned this lens
		#curIndex = self.host.highlight_index
		curIndex = self.spawn_index
		
		# for each item within the range, build a list item and show it
		for index in range(self.start_idx, self.end_idx):
			# get item being represented
			item = self.host.model[index]
			
			# create a widget for this
			listItem = FsFlexiLensItem(self, self.host, item, index)
			self.viz.append(listItem)
			layout.addItem(listItem)
			
	# -------------------------------------------
	
	# Compute the vertical midpoint of own contents - to favour item we're supposed to have centered
	# ! Overides FloatingLens.calcMidpointOffset
	# > returns: (float > 0) y-height/offset from top of self that should be centered around the spawn point
	def calcMidpointOffset(self):
		if self.targetCenteredItem:
			# return the midpoint of this thing's offsets from top of our viz-space
			ymin = self.targetCenteredItem.pos().y()
			ymax = self.targetCenteredItem.height() + ymin
			
			return (ymin + ymax) / 2.0
		else:
			return FloatingLens.calcMidpointOffset(self)
	
	# Override this method to add additional capabilities to get host to redraw
	# ! Override FloatingLens.hideTipImmediately()
	def hideTipImmediately(self):
		# kill host's activation drawing
		# XXX: careful - this may cause more flickering bugs all over again
		self.host.lens_spawner.clearActivationArea()
		
		# perform standard actions now
		FloatingLens.hideTipImmediately(self)
	
	# -------------------------------------------
	
	# Custom drawing
	def paintEvent(self, evt):
		# draw backdrop
		p = qgui.QPainter()
		
		p.begin(self)
		
		self.draw_background(p)
		self.draw_item_backgrounds(p)
		
		p.end()
		
		# now draw normal stuff (i.e. contents)
		qgui.QWidget.paintEvent(self, evt)
		
	# draw background decorations for widget
	def draw_background(self, p):
		# widget area
		w = self.width()
		h = self.height()
		
		# get color palette
		cs = qgui.QPalette()
		
		# set drawing info, and draw solid fill
		border = qgui.QPen(cs.color(qgui.QPalette.Dark), 1)
		fill = qgui.QBrush(qgui.QColor("white"), qcore.Qt.SolidPattern)
		
		p.setPen(border)
		p.setBrush(fill)
		
		p.drawRect(0, 0, w-1, h-1)
	
	# draw backgrounds behind each item in list
	def draw_item_backgrounds(self, p):
		# loop over each item, calling its special drawing functions
		for item in self.viz:
			item.draw_background(p)
	
	# -------------------------------------------
	
	# Check if mouse is within bounds of lens
	# NOTE: cannot use builtin methods, as simply being inside a label can screw things up
	# > returns: (bool)
	def isUnderMouse(self):
		# get relevant coordinates in global space
		mco = qgui.QCursor.pos() # <--- mouse, in global coordinates by definition!
		bounds = self.geometry() # <--- this is global, as it doesn't have a parent
		
		# return whether point is in rect (includes the edges)
		return bounds.contains(mco)
	
	# Mouse move handling
	# < mevt: (QMouseEvent)
	def mouseMoveEvent(self, mevt):
		#print "Do Lens Mousemove - %d %d " % (mevt.x(), mevt.y())
		
		# while within element, enable whichever item is under cursor
		y = mevt.y()
		
		itemUnderMouse = None
		for item in self.viz:
			bounds = item.geometry() # XXX: assume that this is in parent space?
			
			ymin = bounds.y()
			ymax = bounds.height() + ymin
			
			# if within bounds - highlight, otherwise not...
			#print "   item - %d : %d" % (ymin, ymax)
			if ymin <= y < ymax:
				item.mouseover = True
				itemUnderMouse = item
			else:
				item.mouseover = False
		
		# set host's highlight index to highlight the item under mouse
		# so that users can build correspondance between lens and SCOFT
		if itemUnderMouse:
			self.host.highlight_index = itemUnderMouse.index
			self.host.repaint()
		else:
			self.host.highlight_idnex = -1
			self.host.repaint()
		
		# force repaint to update drawing status
		self.repaint()
		
		# unhandled - pass on
		return FloatingLens.mouseMoveEvent(self, mevt)
	
	# Catch list-click event, and redirect it to be used for jumping to a location
	# < mevt: (QMouseEvent)
	def mouseReleaseEvent(self, mevt):
		# find the row on which item lived, and fire off event for that index
		# NOTE: code adapted from above (mouseMoveEvent)
		y = mevt.y()
		index = None
		
		for item in self.viz:
			bounds = item.geometry() # XXX: assume that this is in parent space?
			
			ymin = bounds.y()
			ymax = bounds.height() + ymin
			
			# if within bounds, fire off click event
			if ymin <= y < ymax:
				index = item.index
				break;
		
		# perform navigation, then clean up after ourselves
		if index is not None:
			self.host.itemClicked.emit(index)
			
			# kill spawner's links...
			self.spawner.postLensCleanup() # cleanup all refs to this (and/or disable timers)
			self.hideTipImmediately()      # clean up after ourselves on our own time
	
	# -------------------------------------------
	
	# Replace contents of layout
	# ! Lens MouseWheel Handling Helper
	# NOTE: this is really nasty stuff, but it's the only way that works!
	def replace_layout(self):
		layout = self.layout()
		
		# remove all items from layout
		# - pop all wrappers from its list, and unlink all unused-widgets for deletion
		while layout.count():
			item = layout.takeAt(0)
			if item.widget() not in self.viz:
				item.widget().deleteLater()
		
		# re-add all items
		for item in self.viz:
			layout.addItem(item)
			
		# invalidate the caches and/or force all items to be shown
		for layoutItem in layout.itemList:
			layoutItem.invalidate()
			layoutItem.widget().show()
			
		layout.invalidate()
	
	# Repaint all relevant parts of widget after rebuilding
	def repaint_all(self):
		self.repaint()
		for item in self.viz:
			item.repaint()
	
	# .........
	
	# Fetch and add item(s) above top of lens (discarding items from bottom), and return these items
	# ! Lens Scrolling Helper
	# < steps: (int) number of additional items to add
	# > returns: ([FsFlexiListItem])
	def items_shift_up(self, steps=1):
		items = []
		
		# compute start of new range
		# - clamp number of steps performed to ensure that we only move as far as we have content for
		new_start = max(0, self.start_idx - steps)
		
		steps = self.start_idx - new_start
		if steps == 0:
			# trying to add 0 items fails!
			return items
		
		# grab extra items one by one from above current start index
		for index in range(new_start, self.start_idx):
			# get corresponding item from model
			item = self.host.model[index]
			
			# create list item
			listItem = FsFlexiLensItem(self, self.host, item, index)
			items.append(listItem)
			
		# add these to the start of the lists
		self.viz = items + self.viz[:-steps]
		self.replace_layout()
		
		# update range
		self.start_idx -= steps
		self.end_idx   -= steps
		
		# return list of new items
		return items
		
	# Fetch and add item(s) below bottom of lens (discarding items from top), and return these items
	# ! Lens Scrolling Helper
	# < steps: (int) number of additional items to add
	# > returns: ([FsFlexiListItem])
	def items_shift_down(self, steps=1):
		items = []
		
		# compute end of new range
		# - clamp number of steps performed to ensure that we only move as far as we have content for
		max_items = len(self.host.model) -1
		new_end = min(self.end_idx + steps, max_items)
		
		steps = new_end - self.end_idx
		if steps == 0:
			# trying to add 0 items fails!
			return items
		
		# grab extra items one by one from above current start index
		for index in range(self.end_idx, new_end):
			# get corresponding item from model
			item = self.host.model[index]
			
			# create list item
			listItem = FsFlexiLensItem(self, self.host, item, index)
			items.append(listItem)
			
		# add these to the end of the lists (having chopped off the start items from the lists
		self.viz = self.viz[steps:] + items
		self.replace_layout()
		
		# update range
		self.start_idx += steps
		self.end_idx   += steps
		
		# return list of new items
		return items
	
	# .........
	
	# Expand the number of items shown by steps on either side
	# ! Lens Zooming Helper
	# < steps: (int) number of items to add on either side
	def items_grow(self, steps=1):
		# before range ............................
		start_new = max(self.start_idx - steps, 0)
		before = []
		
		for index in range(start_new, self.start_idx):
			# get corresponding item from model
			item = self.host.model[index]
			
			# create list item
			listItem = FsFlexiLensItem(self, self.host, item, index)
			before.append(listItem)
			
		# after range .............................
		end_new = min(self.end_idx + steps, len(self.host.model) - 1)
		after = []
		
		for index in range(self.end_idx, end_new):
			# get corresponding item from model
			item = self.host.model[index]
			
			# create list item
			listItem = FsFlexiLensItem(self, self.host, item, index)
			after.append(listItem)
		
		# rebuild lists .............................
		self.viz = before + self.viz + after
		self.replace_layout()
		
		# update range
		self.start_idx = start_new
		self.end_idx = end_new
		
		# update lens-range size for later - so that user doesn't have to constantly resize lenses
		FsFlexiLens.LENS_RANGE_SIZE += max(len(before), len(after))
		
	# Reduce the number of items shown by steps on either side
	# ! Lens Zooming Helper
	# < steps: (int) number of items to add on either side
	def items_shrink(self, steps=1):
		# just ensure that after shrinking, we'll still have the same number of items...
		if len(self.viz) - (steps * 2) < 3:
			return;
			
		# simply chop off items from viz
		self.viz = self.viz[steps:-steps]
		self.replace_layout()
		
		# update range
		self.start_idx += steps
		self.end_idx   -= steps
		
		# update lens-range size for later - so that user doesn't have to constantly resize lenses
		FsFlexiLens.LENS_RANGE_SIZE -= steps
	
	# .........
	
	# Mousewheel handling
	# < evt: (QWheelEvent)
	def wheelEvent(self, evt):
		# determine direction of movement, and add relevant number of items
		steps = evt.delta() / 120
		if steps == 0: return # ignore no movement...
		
		if evt.modifiers() & qcore.Qt.ControlModifier:
			# zoom control - increase/decrease number of items shown in lens
			stepsize = abs(steps) # XXX: this can be more than 1, but for now, allow just single-click
			#stepsize = 1
			
			if steps > 0:
				#print "larger - %d" % (steps)
				self.items_grow(stepsize)
			else:
				#print "smaller - %d" % (steps)
				self.items_shrink(stepsize)
				
			# recenter lens - simply by changing the item to keep centered
			self.targetCenteredItem = self.viz[len(self.viz) / 2]
			
			# update size, and reposition again in new place
			self.resize(self.sizeHint())
			self.placeTip(self.host, self.spawn_yco) # XXX: yco?
			
			# repaint with updates
			self.repaint_all()
		else:
			# scroll up/down
			# - stepsize is a percentage of full lens size - makes large list scrolling easier...
			#   current value is based on 2 steps for 10 items
			"""
			if abs(steps) * 10 < len(self.viz):
				stepsize = len(self.viz) / (abs(steps) * 10)
			else:
				stepsize = abs(steps)
			"""
			stepsize = abs(steps) * 2
			#print steps, stepsize
			
			if steps > 0:
				#print "up - %d" % (steps)
				new_items = self.items_shift_up(stepsize)
				direction = -1
			else:
				#print "down - %d" % (steps)
				new_items = self.items_shift_down(stepsize)
				direction = +1
			
			# from items, determine the number of pixels changed...
			steps = len(new_items)
			if steps == 0:
				# not moving anywhere
				# TODO: have bounce-back anim?
				return;
			
			# compute new size/layout, and ensure that everything gets shown using this now...
			# XXX: consider recentering if size changes dramatically? But this is bad as it makes us
			#      lose the centering of the target item which spawned the lens under normal circumstances.
			#      If we hang on to this item until it disappears, then we have other problems too, since
			#      then we'll have a sudden "pop" when the old item goes!
			self.setGeometry(qcore.QRect(self.pos(), self.sizeHint()))
			self.repaint_all()
			
		# repaint host too - to update lens range shown
		self.host.repaint()
		
		# we've now handled the event...
		evt.accept()
		
	# -------------------------------------------
	
	# Register self as event handler (eventFilter() below gets called)
	# on owners so that we respond to changes accordingly
	# TODO: maybe spawner should instead handle some of the more tricky cases, by providing extension opportunities?
	def bind_eventFilter(self):
		# obviously on host, since when it moves/resizes we definitely want to know
		self.host.installEventFilter(self)
		
		# also bind host's window, since that can affect placement of host without it knowing
		self.host.window().installEventFilter(self)
		
	# Unbind lens's event filters from owner
	def closeEvent(self, evt):
		# unbind from host and its window
		self.host.removeEventFilter(self)
		self.host.window().removeEventFilter(self)
		
		# default handling now
		FloatingLens.closeEvent(self, evt)
	
	# Catch-all event handler - catches show/hide events from host + host's window
	# which may cause us to need to make various adjustments
	# < obj: (QObject) object we're attached to
	# < evt: (QEvent) event to check
	# > returns: (bool) True if we're blocking that event
	def eventFilter(self, obj, evt):
		#assert(obj == self.host or self.host.window())
		etype = evt.type()
		
		# we're only really interested in catching window activate/deactive events so far,
		# since those mean that we should probably hide for a while until window comes back
		# (otherwise we'll hang around like a ghost!)
		if etype == qcore.QEvent.WindowDeactivate:
			# when window deactivates, we need to hide...
			# - but not immediately, or else clicking on items fails
			self.hideTip()
		elif etype in (qcore.QEvent.Resize, qcore.QEvent.Move):
			# for now, just kill the lens - not immediately, or else clicking on items fails
			# TODO: could just offset instead?
			self.hideTip()
		elif etype in (qcore.QEvent.KeyPress, qcore.QEvent.KeyRelease):
			key = evt.key()
			
			if key == qcore.Qt.Key_Escape:
				# hide on esckey
				self.hideTipImmediately()
			elif key == qcore.Qt.Key_Shift:
				# shift-key status changed = lens is probably not wanted anymore
				self.hideTipImmediately()
			
			
		# we never swallow anything!
		return False

##################################################
# Lens Spawner - Generate a lens in response to some trigger

# Lens spawner component
class FsVizLensSpawner(qcore.QObject):	
	# ctor
	def __init__(self):
		qcore.QObject.__init__(self)
		
		# host visualisation - we need this info later when figuring out how to spawn
		# ! Attach host visualisation using FsVizLensSpawner.attachtoHost()
		self.host = None                # (FsOverviewBar)
		
		# current lens instance that we have spawned
		self.lens_instance = None		# (FloatingLens)
		
		self.spawn_yco = 0				# (int) y-co collected when we started trying to spawn
		self.spawn_index = -1			# (int) model index at the point when trying to spawn a target
		
		self.spawn_mco = (None, None)   # (int) x/y mouse coordinates in global/screen space when lens was spawned
		
		# activation area - for catching in-transit mouse movements
		self.activation_calc_hack = False# (bool) use hacky calculation for activation area, which isn't correct but works nice
		
		self.activation_area = None     # (QPolygon) global-space polygon for testing if moving within restricted area
		self.activation_vstr = None     # (str) debug string containing the coordinates/extents of area
		self.activation_draw = None     # (QPolygon) host-space polygon, to be drawn to indicate region affected
	
	# Host Registration ----------------------------
	
	# Bind spawner to a host
	# ! Assumes that spawner wasn't attached to any prior hosts
	def attachToHost(self, host):
		# set new host
		self.host = host
		
		# connect signals for events fired when various actions happen with host
		# XXX: since we're now hooking them up more intimately, maybe these won't be needed anymore...
		host.targetEntered.connect(self.prepare)
	
	# Host Interaction -----------------------------
	
	# Called from host mousemove handler to catch transiting mouse movements,
	# and prevent accidental highlight-index changes while mouse may be moving
	# towards the lens
	#
	# < mevt: (QMouseEvent)
	# > returns: (bool) True if event was already caught by the activation area for the menu
	#                   Otherwise, event processing in the host can carry on as usual
	def handleHostMouseMove(self, mevt):
		# if shift isn't held, lens should be killed
		if not mevt.modifiers() & qcore.Qt.ShiftModifier:
			if LENS_DEBUG: print "# SPAWNER: Mouse Move Handling Ignored"
			return False
		
		# we may need to capture this event if a lens is likely to be in progress...
		if self.lens_instance:
			# only capture event if we're still within the capture area...
			if self.withinActivationArea(mevt):
				# HANDLED
				mevt.accept()
				return True
		
		# no lens active - no need for concern
		return False
		
	# .........................
		
	# Check if mouse event falls within "activation area"
	# > returns: (bool)
	def withinActivationArea__Shadow(self, mevt):
		if self.lens_instance is None:
			if LENS_DEBUG: print "> No activation area (no lens)"
			return False
			
		# if shift is held down, allow it to pass - "browse" override
		if mevt.modifiers() & qcore.Qt.ShiftModifier:
			if LENS_DEBUG: print "> Shadow Shift Override In Action - Ignore shadow"
			return False
			
		# get top-left and bottom-left corners in global coordinates ...................
		# NOTE: host coordinates are already in global, since the widget doesn't have parent
		w     = self.lens_instance
		wgeo  = w.geometry()
		
		upper = wgeo.topLeft()    #w.mapToGlobal(wgeo.topLeft())
		lower = wgeo.bottomLeft() #w.mapToGlobal(wgeo.bottomLeft())
		
		# check if mouse is currently within this region ...............................
		# XXX: cache results of polygons?
		mco = qgui.QCursor.pos() # <--- already in global; mevt is local to host...
		sco = self.spawn_mco
		
		# simply return true if the yco-co is within bounds
		range = "(%d, %d) -> U (%d, %d) and L (%d, %d), from S (%d, %d)" % (mco.x(), mco.y(), upper.x(), upper.y(), lower.x(), lower.y(), sco.x(), sco.y())
		if (upper.y() - 10 <= mco.y() <= lower.y() + 10):
			# HACK: still allow some scrubbing nearby...
			# XXX...
			
			if LENS_DEBUG: print "> In Area - %s" % (range)
			return True
		else:
			if LENS_DEBUG: print "> Not in area - %s" % (range)
			return False
			
	# .........................
	
	# Compute activation area using "AAMU-Curved" method
	#    E Tanvir, A Bunt, A Cockburn and P Irani. 
	#	Improving Cascading Menu Selections with Adaptive Activation Areas 
	#    International Journal of Human-Computer Studies. 2011. 69, 769-785.
	#
	# ! Stores results in "activation_area" var, which must be managed alongside the lens
	def calcActivationArea__AAMU_Curved(self):
		# get triangle verts for area in global coordinates .............................
		# lens extents - RHS of area
		# NOTE: lens coordinates are already in global, since the widget doesn't have parent
		wgeo  = self.lens_instance.geometry()
		
		A = wgeo.topLeft()         # top left corner of lens
		B = wgeo.bottomLeft()      # bottom left corner of lens
		
		AR = wgeo.topRight()       # top right corner of lens - to increase activation area to avoid problems during crossover
		BR = wgeo.bottomRight()    # bottom right corner of lens - to increase activation area to avoid problems during crossover
		
		# add extra padding, to make items easier to hit
		A.setX(A.x() - 2)
		B.setX(B.x() - 2)
		
		# starting point of area within host:
		# start should be between 1/5th and 1/2 of the way across
		#  - too far left, and that's when it bugs out when users exit the widget
		#  - too far right, and the cone is too narrow to easy enter
		hgeo = self.host.geometry()
		
		hleft = self.host.mapToGlobal(hgeo.topLeft())
		hright = self.host.mapToGlobal(hgeo.topRight())
		hmid = (hleft.x() + hright.x()) / 2.0
		
		sco = self.spawn_mco
		xco = min(max(10, sco.x()), hmid)
		C = qcore.QPoint(xco, sco.y())
		
		# compute AAMU area poly ........................................................
		# height parameters
		h = wgeo.height()          # full height of lens (h in paper)
		h1 = h2 = h / 2.0          # heights of each "half" of the poly (h1, h2 in paper)
		
		# horizontal distance parameters
		xmin = C.x()               # cursor spawn-x
		xmax = A.x()               # submenu edge
		
		d = abs(xmax - xmin)       # width of activation poly (d in paper)
		if d == 0.0:
			if LENS_DEBUG: print "! Zero distance between spawn and lens; Assume overlap for quick hop across..."
			return True
		
		# quadratic slope parameters (k in paper)
		k1 = float(h1) / d**2
		k2 = float(h2) / d**2
		
		# baseline y-co
		ybase = C.y()   # coordinate that item lies on
		ymargin = 3     # padding around y-coordinate to protect from wobble as we move across
		
		# sample the poly above and below in separate steps...
		upperPts = []
		lowerPts = []
		
		#print "$ AAMU Points..."
		for x in range(0, d+1):
			# f(x) = k x^2
			y_up  = ybase - ymargin - (k1 * x**2)    # upper is before baseline
			y_dn  = ybase + ymargin + (k2 * x**2)    # lower is after baseline
			
			# add points to poly's
			x_val = xmin + x
			
			upperPts.append(qcore.QPoint(x_val, y_up))
			lowerPts.append(qcore.QPoint(x_val, y_dn))
			#print "    %d: %d, %d" % (x_val, y_up, y_dn)
		
		# store activation area data ......................................................
		# NOTE: this goes in clockwise order, start from C, going up to A, over to RHS edge, then back to B, and back around to C
		all_pts = ([C + qcore.QPoint(0, ymargin)] +  
		           upperPts + 
		           [AR] + 
				   [qcore.QPoint(AR.x(), C.y())] +
				   [BR] + 
				   lowerPts[::-1] +
				   [C - qcore.QPoint(0, ymargin)])
		
		self.activation_area = qgui.QPolygon(all_pts)
		self.activation_vstr = "C (%d, %d) : A (%d, %d) & B (%d, %d)" % (C.x(), C.y(), A.x(), A.y(), B.x(), B.y())
		
		# create a copy of poly for drawing only
		# i.e. with all points converted to host's space
		hostMapFn = self.host.mapFromGlobal
		host_pts  = [hostMapFn(pt) for pt in all_pts]
		
		self.activation_draw = qgui.QPolygon(host_pts)
	
	# Clear activation area data (when killing lens)
	def clearActivationArea(self):
		needs_repaint = (self.activation_draw is not None)
		
		# clear vars
		self.activation_area = None
		self.activation_vstr = None
		self.activation_draw = None
		
		# repaint host to force clearance
		if needs_repaint:
			self.host.repaint()
	
	# Draw activation area on host
	# < p: (QPainter)
	def hostDrawActivationArea(self, p):
		# only draw if it exists
		# XXX: keep this in sync with the code in handleHostMouseMove()!
		if self.lens_instance and self.activation_draw:	
			# save state - this step enables extra options
			p.save()
			
			# setup drawing options - a dark grey triangle for now...
			p.setPen(qcore.Qt.NoPen)
			p.setBrush(qgui.QColor(50, 50, 50, 128))
			
			# enable antialiasing, for smoother edges without the jaggies
			p.setRenderHint(qgui.QPainter.Antialiasing, True)
			
			# draw the polygon...
			#p.drawPolygon(self.activation_draw, qcore.Qt.WindingFill)
			p.drawPolygon(self.activation_draw)
			
			# pop state
			p.restore()
	
	# Check if mouse event falls within "activation area"
	# < mevt: (QMouseEvent) position is given in host-local coordinates - unusable...
	# > returns: (bool)
	def withinActivationArea__AAMU(self, mevt):
		if self.lens_instance is None:
			if LENS_DEBUG: print "> No activation area (no lens)"
			return False
			
		# compute area if it doesn't exist already...
		if self.activation_area is None:
			if LENS_DEBUG: print "% Recalculated activation area..."
			self.calcActivationArea__AAMU_Curved()
		
		# check if mouse is currently within this region ...............................
		mco = qgui.QCursor.pos() # <--- already in global; mevt is local to host...
		inArea = self.activation_area.containsPoint(mco, qcore.Qt.WindingFill)
		
		rangeStr = "(%d, %d) --> %s" % (mco.x(), mco.y(), self.activation_vstr) 
		if LENS_DEBUG: print "> Area Check: %s - %s" % (inArea, rangeStr)
		
		return inArea
		
	# Alias for switching between which method we use to select activation area testing
	withinActivationArea = withinActivationArea__AAMU
	#withinActivationArea = withinActivationArea__Shadow
	
	# Triggers -------------------------------------
	# These should be attached to the signals from SCOFT bars which 
	# tell us that a target was triggered and that we should get ready
	# to spawn a response (or conversely, that that target was lost)
	
	# Prepare to spawn a target if nothing else happens in the meantime...
	# NOTE: this is when a target has been freshly acquired (maybe a short time after another one was exited!)
	# < yco: (int) coordinates for what we are spawning
	def prepare(self, yco):
		if LENS_DEBUG: print "# SPAWN: prepare(%d)" % (yco)
		
		# kill anything that is showing
		# XXX: we should just reuse...
		self.kill_lenses() 
		
		# only show lens if shift-modifier is being held down
		if not (qgui.QApplication.keyboardModifiers() & qcore.Qt.ShiftModifier):
			if LENS_DEBUG: print "SPAWN: ignored - unwanted"
			return;
		
		# save off coordinate so that we know where to show
		# and also the index (just in case it changes inbetween on the way to the target)
		self.spawn_yco = yco
		self.spawn_index = self.host.highlight_index
		
		# save off global mouse coordinates (for activation area testing)
		self.spawn_mco = qgui.QCursor.pos()  # <-- already in global...
		
		# spawn new lens now!
		self.spawn()
	
	# Stop all activities and handlers after lens has been used to navigate
	# but leave the cleaning up of the lens to the lens itself!
	def postLensCleanup(self):
		if LENS_DEBUG: print "X SPAWN: PostLensCleanup"
		self.clearActivationArea()
	
	# Spawning ------------------------------------
	
	# Spawn a new lens
	def spawn(self):
		if LENS_DEBUG: print "# SPAWN: spawning lens()"
		# create a new lens depending on the density
		"""
		density = self.host.calc_item_density()
		if density and log10(density) < 1.5:
			# density = [1, 100) => details lens
			normal-size items
		else:
			scale-down items
		"""
		self.lens_instance = FsFlexiLens(self, self.host, self.spawn_index, self.spawn_yco)
		
		# make lens show up in the right place
		self.lens_instance.resize(self.lens_instance.sizeHint()) # XXX: this works bad near the border
		self.lens_instance.placeTip(self.host, self.spawn_yco)
		self.lens_instance.show()
		self.lens_instance.raise_()
		
		# repaint host to show activation area...
		self.calcActivationArea__AAMU_Curved()
		self.host.repaint()
		
		if LENS_DEBUG: print "! SPAWN: lens shown..."
	
	# Helper to create a testing lens instance - just an plausible-looking empty lens with dummy contents
	def make_test_lens(self):
		self.lens_instance = FloatingLens() # XXX temp
		
		layout = qgui.QVBoxLayout()
		self.lens_instance.setLayout(layout)
		
		layout.addSpacing(100)
		layout.addWidget(qgui.QLabel("Test"))
		layout.addSpacing(100)
		
	# Kill lens (and spawners it may have created...)
	def kill_lenses(self):
		if LENS_DEBUG: print "X SPAWN: kill_lenses()"
		
		# clear activation areas (for catching events)
		self.clearActivationArea()
		
		# kill lens itself
		if self.lens_instance:
			self.lens_instance.close()
			self.lens_instance = None
	
##################################################
