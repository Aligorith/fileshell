# Fisheye Version of FsOverviewBar 
# 
# An interactive full filesystem folder visualisation widget
# designed for easier selection of targets within the 
# information space.
#
# Author: Joshua Leung
#         (Based on algorithms by Stephen Fitchett)
# Date: July 2013

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import bisect
from math import ceil, sqrt, log

from fileshell.model import fsdb
from fileshell.model.fsutils import *
from fileshell.time.timeRange import *

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui


# for fun... not critical component for overall widget,
# but is used for a hacky experiment
import random
from fileshell.components import tagLabels 

#############################################
# Debug Constants

# Debugging prints for floating lens handling...
LENS_DEBUG = False

#############################################
# Fisheye Summary Widget
# NOTE: many of these parts are adapted from Footprints,
#       but we'll just duplicate them here to make it easier
#       to manage...

# ============================================

# Item in the treelist
class FsTreeListItem(object):
	__slots__ = ('name', 'depth', 'data',
				 'length', 'color', 
				 'weight', 'weightSum', 'ybounds',
				 'tMult', 'important',
				 'parItem', 'childItems')
	
	# ctor
	def __init__(self, name, depth, data, parItem):
		# id info
		self.name = name
		self.depth = depth
		
		# data being represented
		# (FolderData)
		self.data = data
		
		
		# "parent" list item
		# (FsTreeListItem)
		self.parItem = parItem
		
		# "child" list items
		# ([FsTreeListItem]) - each child adds itself to its parent
		self.childItems = []
		
		# add self to parent list item's child list
		if self.parItem:
			self.parItem.childItems.append(self) 
		
		
		# fisheye distortion weights
		self.weight = 1.0				# log/damped weight of own influence - initialise with a dummy first
		self.weightSum = 0.0			# sum of weights of children
		
		self.ybounds = None             # (ymin:float, ymax:float)
		
		# visualisation data
		self.length = len(self.name) 	# length of line - starts from length of name
		self.color = [0, 0, 0] 		 	# all use black as default (colors are 0-255 range for PyQt)
		
		self.tMult = 1.0				# multiplying factor - used to emphasize important items (by time)
		self.important = False			# is item an important target (i.e. appears in consecutive bins)
		
	# Importance Handling (Weights, Colours, etc.) -----------------------------------
	
	# Update weight(s) of node
	# ! Assume that when a node is visited, none of its subtree has been seen yet
	# ! Clears the sum total of subtree weights first, to prevent accumulation errors
	# < filters: (FilterModel) filters used to determine what is in range (and not)
	# < (distort_space): (bool) weights for targets varies...
	# < (propagate): (bool) propagate new weight up tree
	def update_ranking(self, filters, distort_space=True, propagate=True):
		# reset sum of subtree weights - assumes that all of those are yet to come
		self.weightSum = 0.0
		
		# determine weight + importance...
		if self.data.ignore:
			# item has been tagged as a "bad" node - ignore and try our best to hide...
			self.weight = 0.0
			
			self.tMult = 0.0
			self.important = False
		elif self.data.bookmarked:
			# user has explicitly specified item as being interesting
			self.weight = 1.0 # this makes it one of the bigger items
			
			# although item is generally important, for search to work, 
			# importance must still follow filters...
			self.important = self.calc_importance__filters(filters)
		else:
			# compute importance + weight of node normally
			if distort_space:
				self.important, self.weight = self.calc_ranking__standard(filters)
			else:
				self.important, self.weight = self.calc_ranking__constant(filters)
		
		# propagate weight up tree, by adding our weight right up the tree...
		if propagate:
			parent = self.parItem
			while parent:
				parent.weightSum += self.weight
				parent = parent.parItem
	
	# ------------------------------------------
	
	# compute importance of node
	# - This is the part that we can vary to determine different importance effects
	#
	# < filters: (FilterModel)
	# > return[0]: is this node important or not
	# > return[1]: "weight" value which can be stored
	
	# Helper - account for filters when determining if something may potentially be important
	def calc_importance__filters(self, filters):
		"""
		t = self.data.mtime()
		
		# time filter
		if filters.use_time_filter:
			inRange = (filters.tmin <= t <= filters.tmax)
		else:
			inRange = True
			
		# text filter
		if inRange and filters.search_text:
			# item must be in range AND match the text filter
			filter_txt = filters.search_text # cache for quicker access...
			
			for item in ([self] + self.data.files):
				# match found...
				# NOTE: this is just the "brute-force" exact match version - but it's slightly faster...
				if filter_txt in item.name:
					important = True
					break;
			else:
				# no matches found
				important = False
		else:
			# importance only depends on whether item is in range or not
			important = inRange
		"""
		# simply read this off from cache - assuming it has been updated first
		important = filters.folderOkOverview[self.data]
		
			
		# return importance according to filters
		return important
	
	# Ranking Mechanism: Constant (Baseline)
	# NOTE: this is the same as the linear condition
	def calc_ranking__constant(self, filters):
		# importance is still just in-range...
		t = self.data.mtime()
		inRange = (filters.tmin <= t <= filters.tmax)
		
		important = inRange and self.calc_importance__filters(filters)
		
		# (isImportant, weight)
		return (important, 1.0)
		
	# New standard ranking mechanism
	def calc_ranking__standard(self, filters):
		# current time
		t = self.data.mtime()                           # time for item - last modification time
		
		tmodel = filters.fs_model.time_model            # full time-range
		now = tmodel.tmax                               # "current" time (i.e. last timestamp)
		
		# weight
		recency = (now - t)                             # (0 = new) => (M = old)
		scaleFactor = 100000                            # controls how quickly it drops off
		weight = scaleFactor / (recency + scaleFactor)  # [0.0, 1.0]
		
		# recency drops off for less relevant, so only most relevant get highlighted this way
		# XXX: only select top N of these to highlight
		important = (weight > 0.1) and (self.calc_importance__filters(filters))
		
		# (isImportant, weight)
		return (important, weight)

# ============================================

# Default tooltip for footprints
FishprintsHelpText = """\
<p style='white-space:pre'>
<b>SCOFT Bar</b>
This view shows a zoomed out view of your folder hierarchy
as it would appear if the hierarchy was fully expanded.

Folders are ordered vertically in the normal way (i.e. in
alphabetical order), and are offset horizontally according to
their depth in the hierarchy.

Folders containing files you have recently accessed are 
highlighted in orange and are shown larger than those which
haven't been touched for a while.

To use this:
 * <b>Click on a tag</b> to jump straight to the tagged folder
 
 * <b>Click anywhere</b> else to jump to the outermost folder
   in this region.
   
   - <i><b>Click again</b> to descend one level in the hierarchy, OR
   - <b>Use the Scroll Wheel</b> to control browsing precision
   - <b>Move the mouse up and down</b> to browse nearby folders located
     at the same depth in the hierarchy
 
 * <b>Hold down the <i>Ctrl</i> key</b> for fine-grained control,
   allowing you to jump straight to whatever folder is currently
   under the cursor
 
 * <b>Hold down the <i>Alt</i> key</b> and slowly move your mouse
   <i>Left</i>/<i>Right</i> to be able to select items with greater 
   precision
   
   <i>Tip: This is useful for exploring the folder structure</i>
 
 * <b>Hold down the <i>Shift</i> key</b> to see a zoomed in view of the
   folders in the area under the cursor. Clicking on the names 
   shown in this view takes you straight to the named folder.
   
   <i>Hint: The lens will only appear/be visible while <i>Shift</i> 
   key is being held. Releasing <i>Shift</i> will cause the lens
   to disappear immediately</i>
</p>
"""

# Stick Target Constants
HITBUCKET_HIGH = 10 	# minimum vertical height of a sticky target which has focus
HITBUCKET_SIZE = 3  	# number of pixels on either side of current pixel to check for more salient items (when on a non-salient one)

# ---------------------------------------------

# Fisheye version of FsOverviewBar
class FishOverviewBar(qgui.QWidget):
	# Tooltip to display when no targets are selected
	DEFAULT_TOOLTIP = FishprintsHelpText
	
	# Something was clicked on - use to update related view accordingly
	# < index: (int) index of item clicked on
	itemClicked = qcore.pyqtSignal(int)
	
	# Target entered/lost
	# < yco: (int) relative vertical coordinate of the item entered (use to get the range)
	targetEntered = qcore.pyqtSignal(int)
	targetLost    = qcore.pyqtSignal()
	
	# Mouse leaving widget - trigger for timing out the floaters
	mouseExit     = qcore.pyqtSignal()
	
	# ------------------------------------------
	
	# ctor
	# < fs_model: (FsModel) file system model
	# < filters: (FilterModel) filtering info mediator
	# < lens_spawner: (FsVizLensSpawner) floating lens
	# < (indent): (int) number of pixels to indent each level
	# < (distort_space): (bool) enable non-linear allocation of target sizes?
	# < (no_time_colors): (bool) don't show any recency hints using color
	def __init__(self, fs_model, filters, lens_spawner, indent=3, distort_space=True, no_time_hints=False):
		qgui.QWidget.__init__(self)
		
		# widget configuration
		self.setMinimumSize(50, 1)
		self.setMouseTracking(True) # for mousemove highlighting to work
		
		# underlying data models
		self.fs_model = fs_model
		self.filters = filters
		
		# own settings
		self.indent = indent		        # pixels each level is indented by
		
		self.highlight_index = -1	        # index (full-tree model) for row/subtree that is highlighted (under cursor)
		self.selected_index  = -1	        # index (full-tree model) for subtree that is selected
		
		self.flash_index     = -1           # index (full-tree model) for row/subtree that should be flashed (nothing shown means no flash)
		self.flash_alpha     = 0            # (float) opacity of flash to draw - 0 = invisible
		
		self.sticky_tars = False	        # (bool) toggle between Standard Browse (pick most salient = default = True), or Adaptive Browse (xco controls sensitivity = false)
		self.distort_space = distort_space  # (bool) if True, non-linear (salient targets larger), otherwise linear (equal sizes)
		self.no_time_hints = no_time_hints  # (bool) don't show any time colouring of any items
		
		self.use_vicinity_first = True      # (bool) when targetting in areas with no salient targets, perform a variant of Adaptive Browse, but with a fixed level which advances on mouse clicks
		
		self.vicinity_level = 0             # (int) depth in tree that vicinity level will grab targets from
		self.reset_vicinity_level()
		
		# floating lens vs tooltips
		# NOTE: having a lens and showing tooltips is mutually exclusive, 
		# since they fight for space, and end up overlapping
		self.show_tooltips = True                     # (bool) tooltips are shown by default, and only muted when user wants a lens instead
		self.lens_spawner = lens_spawner              # reference to lens spawner (if provided)
		
		if lens_spawner:
			lens_spawner.attachToHost(self)
		
		# context menu - build this first, so that others can extend it
		self.build_context_menu()
		
	# ------------------------------------------
	
	# Reset vicinity level control to "default" level
	def reset_vicinity_level(self):
		# if there is only a single root, lowest level should ignore the root
		if len(self.fs_model.tree) == 1:
			self.vicinity_level = 1
		else:
			self.vicinity_level = 0
			
		# save off "default" vicinity level
		self.vicinity_baseline = self.vicinity_level
	
	# Toggle between showing Linear or Non-Linear SCOFT display space
	# < distort_space: (bool) if True, sets SCOFT space to be displayed in a Non-Linear way
	#                         with salient targets given more space (for easier targetting)
	def toggleSpaceDistortion(self, distort_space):
		# set new value
		self.distort_space = distort_space
		
		# force filters recalc, to refresh the weights/bounds
		# NOTE: show wait-cursor as this can take a while...
		qgui.QApplication.setOverrideCursor(qgui.QCursor(qcore.Qt.WaitCursor))
		self.update_filters()
		qgui.QApplication.restoreOverrideCursor()
	
	
	# build display model
	# XXX: we need to distinguish between full model and display model
	def build_display(self):
		# 1) flatten tree out to form a flat list of all the items
		# NOTE: we always clear out the display model when we do this
		print "Building display model..."
		self.clear_model()
		self.build_model()
		
		print "Timestamping display model..."
		self.update_filters()
	
	# ------------------------------------------
	
	# Clear internal model data - so that model building starts from a clean slate
	# NOTE: override this to knock out model rebuilding when dealing with proxies...
	def clear_model(self):
		self.model = []			# ([FsTreeListItem]) each item that can potentially show up in the tree
		self.roots = []			# ([FsTreeListItem]) each item in self.model which doesn't have a parent
		
		self.max_depth = 0		# max depth of recursion
		self.max_len = 0		# max length (including indents) of items
		
		self.bounds_dirty = True # (bool) y-extents of items need to be recalculated - set after rebuild/ranking change/size change 
	
	# Build internal model (by traversing file system)
	def build_model(self):
		# flatten the trees rooted at the folders given
		for folder in self.fs_model.tree:
			folder.traverseFolders(self.flatten_folders, self.model)
	
	
	# callback to flatten the folder hierarchy to a list (in self.model)
	# TODO: add support for not including "hidden" items?
	def flatten_folders(self, folder, parent, depth, data):
		# find parent item in the list
		# NOTE: inefficient, but shouldn't be too bad in most cases (assuming we don't have a "big blob" problem)
		if parent:
			for parItem in reversed(data):
				if parItem.data == parent:
					break
			else:
				parItem = None
		else:
			parItem = None
		
		# store folder in model
		item = FsTreeListItem(folder.name, depth, folder, parItem)
		data.append(item)
		
		# store model in roots (if necessary)
		if parItem is None:
			self.roots.append(item)
		
		# adjust extents
		self.max_depth = max(self.max_depth, depth)
		self.max_len   = max(self.max_len,   depth*self.indent + item.length)			
	
	# ------------------------------------------
		
	# Heavyweight, full model rebuild
	def update_filters(self):
		# update colors of items
		with TimedOperation("Updating Fishprints Colors..."):
			self.update_colors(self.model)
		
		# update rankings of items
		with TimedOperation("Updating Fishprints Rankings..."):
			self.update_rankings()
		
		# force repaint now
		self.repaint()
		
	# Lightweight model update method
	# - To be used when just a single item's ranking changes
	# < (index): (int - display model index) index of item which has changed
	def refresh_item(self, index = -1):
		# update just a single item?
		if 0 <= index < len(self.model):
			# update just a single item
			item  = self.model[index]
			
			# update colors of this item
			with TimedOperation("Refreshing Fishprints Item Color..."):
				self.update_colors([item])
			
			# recalculate ranking for this item
			# NOTE: don't propagate value up tree, as it may be far too high
			old_weight = item.weight
			item.update_ranking(self.filters, self.distort_space, propagate=False)
			
			# ensure that new weight doesn't go oversize...
			# XXX: this computation comes from update_rankings()
			tot_weight = self.calc_total_weight() # xxx - approximation only!
			
			MAX_HEIGHT = 0.02                 # 2% of total height = tallest that a target can be
			w_max = MAX_HEIGHT * tot_weight   # maximum weight that a target can have - for cutting down weight of really tall items
			
			if item.weight > w_max:
				item.weight = w_max
			
			# HACK: recalculate subtree weight, since that got cleared in the update_ranking() call...
			item.weightSum = sum((child.weight + child.weightSum) for child in item.childItems)
			
			# propagate changed weight up tree, by reducing parent sums by amount of reduction
			delta  = item.weight - old_weight
			#print "refresh weights: n = %s, o = %s, d = %s, t = %s" % (item.weight, old_weight, delta, tot_weight)
			
			parent = item.parItem
			while parent:
				parent.weightSum += delta
				parent = parent.parItem
		else:
			# update whole model, but without expensive rebuilds
			# - update colors of items
			with TimedOperation("Refreshing Fishprints Colors..."):
				self.update_colors(self.model)
			
		# don't rebuild rankings for all (too slow!)
		# just tag bounds as now being invalid
		self.bounds_dirty = True
		
		# do repaint
		self.repaint()
		
	# ....................
		
	# update color filters
	# < items: ([FsTreeListItem]) list of items to update color values for
	def update_colors(self, items):
		if self.no_time_hints:
			self.update_colors__no_recency_hints(items)
		elif self.filters.use_time_filter:
			# XXX: will need some other method to activate this in future
			self.update_colors__time_range_filter(items)
		else:
			self.update_colors__full_range_recency(items)
		
	# Set all items to have the same color
	def update_colors__no_recency_hints(self, items):
		# "Irrelevant" item color
		# NOTE: on Linux, for some reason the blending goes wrong and makes things invisible, so we need darker shades...
		#if os.name == 'posix':
		#	irrelevantCol = [120, 120, 120, 180]
		#else:
		#	irrelevantCol = [180, 180, 180, 128]
		irrelevantCol = [120, 120, 120, 180]
		
		# "Bookmark" color
		bookmarkCol = [210, 20, 0, 255] # reddish...
		#bookmarkCol = [114, 159, 207, 255] # light blue
		
		# set colors for all items to be exactly the same - nothing is favoured
		for item in items:
			# special handling for bookmarks
			if item.data.bookmarked:
				# bookmarks don't depend on time or other factors...
				item.color = bookmarkCol[:]
				item.tMult = 25.0
				
				self.filters.folderColorHints[item.data] = 1.0
			else:
				item.color = irrelevantCol[:]
				item.tMult = 1.0
				
				self.filters.folderColorHints[item.data] = 0.0 # XXX?
		
	# update colors based on recency -> interpolation factor
	def update_colors__full_range_recency(self, items):
		# recency factor
		tmodel = self.fs_model.time_model
		
		wsize = tmodel.tmax - tmodel.tmin     # total time range (in seconds)
		tmin = tmodel.tmin
		tmax = tmodel.tmax
		
		
		# "Irrelevant" item color
		# NOTE: on Linux, for some reason the blending goes wrong and makes things invisible, so we need darker shades...
		#if os.name == 'posix':
		#	irrelevantCol = [120, 120, 120, 180]
		#else:
		#	irrelevantCol = [180, 180, 180, 128]
		irrelevantCol = [120, 120, 120, 180]
		
		# "Bookmark" color
		bookmarkCol = [210, 20, 0, 255] # reddish...
		#bookmarkCol = [114, 159, 207, 255] # light blue
		
		# colors for max (really recent) vs min (really old) colours
		# XXX: make define & config setting for this?
		color_scheme = 3; clip = False; cThresh = 0
		#color_scheme = 5;  clip = False; cThresh = 0 
		
		#color_scheme = 0; clip = True;  cThresh = 0.05
		#color_scheme = 6; clip = True;  cThresh = 0.05
		#color_scheme = 3; clip = True;  cThresh = 0.05
		
		if color_scheme == 0:
			# max = red, old = blue
			minCol = [52, 101, 164] # dark blue
			maxCol = [250, 0, 0]    # red
		elif color_scheme == 1:
			# max = orange, old = blue
			minCol = [114, 159, 207] # light blue
			maxCol = [206, 92, 0]    # dark orange
		elif color_scheme == 2:
			# max = orange, old = irrelevant
			minCol = irrelevantCol
			maxCol = [206, 92, 0]    # dark orange
		elif color_scheme == 3:
			# max = bright orange, old = irrelevant
			minCol = irrelevantCol
			maxCol = [245, 121, 0]   # bright orange
		elif color_scheme == 4:
			# max = light blue, old = irrelevant
			minCol = irrelevantCol
			maxCol = [52, 101, 164] # light blue
		elif color_scheme == 5:
			# max = red, old = irrelevant
			minCol = irrelevantCol
			maxCol = [250, 0, 0]    # red
		elif color_scheme == 6:
			# max = bright orange, old = white
			minCol = [230, 230, 230] # nearly white
			maxCol = [245, 121, 0]   # bright orange
			
		# iterate over items in list, adjusting their colors
		# NOTE: we could easily parallise this part...
		for item in items:
			# special handling for bookmarks
			if item.data.bookmarked:
				# bookmarks don't depend on time or other factors...
				item.color = bookmarkCol[:]
				item.tMult = 25.0
				
				self.filters.folderColorHints[item.data] = 1.0
				
				continue;  # <---------- bookmarks done!
			#else:
			#	pass;       # <---------- everyone else carry on!
			
			# time for alpha factor
			t = item.data.mtime()
			aval = 255 * ((t - tmin) / wsize)
			
			# compute interpolation factor between extents
			weight_method = 3
			if weight_method == 0:
				# weight used for item sizing - seems to fall off too slowly for color purposes... 
				w = item.weight
			elif weight_method == 1:
				# special version of the weights formula, with different falloff rate catered to colour perception
				recency = tmax - t
				
				#scaleFactor = 10000
				scaleFactor = 25000
				#scaleFactor = 50000
				#scaleFactor = 100000
				
				w = scaleFactor / (recency + scaleFactor)
			elif weight_method == 2:
				# linear
				w = (t - tmin) / wsize
			elif weight_method == 3:
				# another version of weights formula, with an exponent control
				recency = tmax - t
				
				scaleFactor = 25000
				
				# falloff exponent for recency - controls how fast this drops off
				# 0.5 = method 2, 1.0 = method 1, 0.8 = relatively flat range
				#falloff = 0.9   # ok, but upper is still a bit flat
				falloff = 0.93 # seems to work quite well 
				
				w = scaleFactor / (recency**falloff + scaleFactor)
			#print w
			
			# interpolate between colors
			#assert(0.0 <= w <= 1.0)
			
			if (clip) and (w < cThresh):
				# use static color
				col = irrelevantCol[:]
			else:
				w = (w - cThresh) / (1.0 - cThresh)   # broaden out range to emphasize colours more
				
				aW = 1.0 - w   # weight for "a" (i.e. min) = inverse of weight
				bW = w         # weight for "b"            = weight, as weight = strength of relevance
				
				col = [(aW*a + bW*b) for a,b, in zip(minCol, maxCol)] + [aval+128]
				
			item.color = [min(255, max(0, x)) for x in col] # clamp within range
			self.filters.folderColorHints[item.data] = min(255, max(0, w))
			
			# tMult - length boost
			if w > 0.8:
				item.tMult = 20.0
			elif w > 0.6:
				item.tMult = 15.0
			elif w > 0.4: # >= "month"
				item.tMult = 10.0
			elif w > 0.2: # hardly any, but just enough to be ignored when mousing over
				item.tMult = 2.0
			else:
				item.tMult = 1.0
	
	# update colors using time range colours filter
	def update_colors__time_range_filter(self, items):
		# size of time window
		tmodel = self.filters
		
		tmin = tmodel.tmin
		tmax = tmodel.tmax
		
		wsize = tmax - tmin
		
		# calculate gradient timestamps
		tranges = calcTimeGradientStops(tmin, tmax)
		tcolors = getTimeGradientStopPairs(tranges)
		
		# pre-compute colour for "irrelevant" items - not adaptive anymore, but just fixed
		# NOTE: on Linux, for some reason the blending goes wrong and makes things invisible, so we need darker shades...
		if os.name == 'posix':
			irrelevantCol = [120, 120, 120, 180]
		else:
			irrelevantCol = [180, 180, 180, 128]
		
		# iterate over items in list, adjusting their colors
		# NOTE: we could easily parallise this part...
		for item in items:
			# compute colour given the folder data
			folder = item.data
			
			# get timestamp
			t = folder.mtime()
			if t is None: t = 0.0
			
			# alpha - oldest should be faintest...
			# 	max(0, val) is to clamp against invalid times...
			val = ((t - tmin) / wsize)
			#val = sqrt(max(0, val))  # <-- ok, but not that clear on ends
			
			aval = 255 * val # time-faded alpha value
			
			# find relevant time range that we fall into
			# XXX: do we have the right side of the bin?
			i = bisect.bisect_right(tranges, t) 
			#print t, tmin, tmax, i
			
			# boost relevance factor (tMult) for most recent items
			# FIXME: hardcoded multipliers
			if (i <= 0) or (i > 8):
				item.tMult = 1.0 # no boost - out of range
			else:
				if i >= 7: # >= "3 hrs"
					item.tMult = 20.0
				elif i > 5: # >= "week"
					item.tMult = 15.0
				elif i > 3: # >= "month"
					item.tMult = 10.0
				else: # hardly any, but just enough to be ignored when mousing over
					item.tMult = 2.0
				
			# lookup relevant color to use
			if (t < tmin) or (t > tmax) or (i <= 0) or (i >= len(tranges)):
				# too old OR too new - just make them all greyish
				#item.color = [cval, cval, cval, val*50]
				item.color = irrelevantCol[:]
			else:
				# fetch the color, and add our own alpha value
				col = tcolors[i][1] # second item is color, first is timestamp
				item.color = [col[0], col[1], col[2], aval+128]
				
			# ensure valid colors - perform clamping
			item.color = [min(255, max(0, x)) for x in item.color]
			self.filters.folderColorHints[item.data] = item.color
		
	# ....................
		
	# update item weights - used to determine amount of distortion applied
	def update_rankings(self):
		# clear set of important folders, since we're about to reset that
		self.filters.importantFolders.clear()
		
		# First Pass: compute rankings for each item
		# - the ranking of each item doesn't really depend on those of its neighbours...
		first_time = True
		min_weight = None
		max_weight = None
		
		print "Calculating Weights - Pass 1..."
		for i, item in enumerate(self.model):
			#if (i % 1000) == 0: print "   ... Progress Item %d of %d" % (i, len(self.model))
			
			# update ranking of item
			item.update_ranking(self.filters, self.distort_space)
			
			# take note of the weight ranges (for debug)
			if not first_time:
				min_weight = min(min_weight, item.weight)
				max_weight = max(max_weight, item.weight)
			else:
				min_weight = item.weight
				max_weight = item.weight
				first_time = False
		
		tot_weight = self.calc_total_weight()
		
		# Debug - print weight extents
		#print ""
		print "Updated Rankings:"
		print "    Min Weight = %s" % (min_weight)
		print "    Max Weight = %s" % (max_weight)
		print "    Tot Weight = %s" % (tot_weight)
		#print ""
		
		# Second Pass: Even out sizes (to prevent overly large items)
		# and only try to limit the density of small targets
		print "Adjusting Weights - Pass 2..."
		
		MAX_HEIGHT = 0.02                 # 2% of total height = tallest that a target can be
		w_max = MAX_HEIGHT * tot_weight   # maximum weight that a target can have - for cutting down weight of really tall items
		
		reduce_target_density = True
		w_small = 0.005 * tot_weight      # "small" targets - below this threshold, targets may be skipped to make them easier to handle
		last_target = None                # previous item was a target within the "dangerous" size-range
		
		for item in self.model:
			# clamp weights to max
			if item.weight > w_max:
				# delta is how much we've shaved off...
				delta = item.weight - w_max
				
				# perform clamping to max weight
				item.weight = w_max
				
				# propagate changed weight up tree, by reducing parent sums by amount of reduction
				parent = item.parItem
				while parent:
					parent.weightSum -= delta
					parent = parent.parItem
					
			# reduce target importance/density...
			# - is this item interesting?
			# XXX: this reduces target density, at risk of making some targets patently unselectable...
			if self.no_time_hints:
				# nothing is important, coloured, or highlighted in any way
				item.important = False
			elif reduce_target_density:
				if item.important:
					# if previous target was also interesting, then we may want to drop this one
					if last_target:
						# was previous target too small to handle? 
						if item.weight <= w_small:
							# skip
							item.important = False
							# XXX: shrink and reassign weight to neighbour
							
							# reset checker...
							last_target = None
						else:
							# this becomes previous target...
							last_target = item
					else:
						# this becomes previous target
						last_target = item
						
					# if item is still important, only now do we note it down in cache
					if item.important:
						self.filters.importantFolders.add(item.data)
				else:
					# reset again - we don't need to worry for a while again
					last_target = None
			
		# ...	
		
		# tag bounds as now being invalid
		self.bounds_dirty = True
		
	
	# ----------------------------------------
	
	# compute basic item dimensions
	# > returns: (float, float, float) canvas width, canvas height, item height
	def calc_item_dimensions(self):
		# widget area - excluding borders
		w = float(self.width() - 3)
		h = float(self.height() - 3)
		
		# dimension of items
		high = h / len(self.model)
		
		# return these three aspects
		return w, h, high
	
	# ----------------------------------------
	
	# Compute total weight for provided item (or root of all trees, if no item provided)
	# < (item): (FsTreeListItem)
	# > returns: (float) total weight of subtrees/trees based on root-level
	def calc_total_weight(self, item=None):
		if item:
			# total item weight
			return (item.weight + item.weightSum)
		else:
			# total tree weight
			return sum((item.weight + item.weightSum) for item in self.roots)
	
	
	# Partition up vertical space for drawing folder hierarchy by proportionally
	# allocating space based on the weight assigned to items. 
	# ! To be used as part of a recursive drawing technique...
	#
	# < folder: (FsTreeListItem) folder that we're drawing (Can be None for toplevel)
	# < children: ([FsTreeListItem]) list of subtrees that share the weight in this region...
	#
	# < weight: (float) total weight occupied by this region
	#
	# < ymin: (float) minimum y-coordinate of region that we can draw in
	# < ymax: (float) maximum y-coordinate of region that we can draw in
	#
	# > return[0]: (float, float) folder ymin, folder ymax
	# > return[1]: ([(float, float)]) list of ymin/ymax extents for each subtree...
	# > return[2]: (float, float) total subtree ymin + ymax
	def partition_folder_weights(self, folder, children, weight, ymin, ymax):
		# divide up space if there's space to divide...
		if weight: # DIVIDE UP SPACE ...............................................
			# folder
			if folder:
				# folder's share...
				fmin = ymin
				fmax = (folder.weight / weight) * (ymax - ymin) + fmin
				#print "\tf = %s | %s / %s = %s (%s, %s)" % (folder.name, folder.weight, weight, folder.weight / weight, fmin, fmax)
				
				# exclude folder's share - what's left is the subtree's
				ymin = fmax
				weight -= folder.weight
			else:
				fmin = fmax = ymin
			
			# subtrees
			subtree_extents = []				 
			
			yrange = ymax - ymin
			y = ymin
			
			for child in children:
				# each subtree's share - remember, this is the child AND its subtree sharing each sub-region...
				cWeightTot = child.weight + child.weightSum
				
				if cWeightTot == 0.0:
					smin = smax = y
				else:
					smin = y
					smax = (cWeightTot / weight) * yrange + y
				
				# ... it should be fine to just divide up remaining like this...
				subtree_extents.append((smin, smax))
				y = smax
		else: # ASSIGN ALL TO SAME PLACE ...........................................
			# linearly divide - basic division size
			H = ymax - ymin
			
			if folder:
				h = H / (len(children) + 1)
			elif len(children):
				h = H / len(children)
			else:
				h = 0
			#print h, H, ymin, ymax
				
			# folder assign
			if folder:
				fmin = ymin
				fmax = ymin + h
				ymin = fmax
			
			# subtree assign
			subtree_extents = [( ymin + h * i,  ymin + h * (i + 1) ) 
								for i in xrange(len(children))]
		
		# return paritioned space for further actions...
		return (fmin, fmax), subtree_extents, (ymin, ymax)
	
	
	# Compute vertical extents of item and its subtree (if needed, for highlight drawing)
	# < item: (FsTreeListItem)
	# < (includeSubtree): (bool)
	# > returns: (float, float) ymin, ymax for item
	def calc_item_extents(self, item, includeSubtree=True):
		# overall dimensions
		w, h, high = self.calc_item_dimensions()
		
		# 1) find root for this item by going up tree
		# (and storing items encountered in a stack, to be popped to quickly index what we need)
		parentStack = [item] # item itself should be first, as it will be the last one we act on...
		
		par = item
		while par:
			par = par.parItem
			parentStack.append(par) # NOTE: last one on here will be null, but that's quite handy...
		
		# 2) now that we've got this toplevel folder, traverse back down to compute its bounds...
		frange = subtrees = sRangeTot = None
		
		while parentStack:
			# grab item from the stack, and compute bounds of this item
			folder = parentStack.pop()
			
			if folder is None:
				# top-level node (on first time through)
				srange = (1, h)
				weight = self.calc_total_weight()
				children = self.roots
			else:
				# just another node along the way...
				srange = subtrees[children.index(folder)]
				
				# BUT, if we've now reach our target again, the srange is the answer we need!
				if folder == item:
					# just the folder, or include the subtree?
					if includeSubtree:
						return srange # <------------ ANSWER IS RETURNED HERE (Folder + Subtree)
					else:
						# do one more step locally to just grab the real frange (for this one)
						frange2 = self.partition_folder_weights(folder, folder.childItems, 
						                                        self.calc_total_weight(folder),
						                                        srange[0], srange[1])[0]
						
						return frange2 # <------------ ANSWER IS RETURNED HERE (Folder only)
				else:
					# too bad, carry on...
					weight = self.calc_total_weight(folder)
					children = folder.childItems
				
			# still here, so calculate extents of subtree rooted under folder
			frange, subtrees, sRangeTot = self.partition_folder_weights(folder, children, weight, srange[0], srange[1])
			
		# error
		raise ValueError, "Somehow we sailed straight past our target!"
	
	# y-coordinate for a given index
	# < index: (int) index for accessing item under mouse from self.model
	# < (use_midpoint): (bool) return midpoint of item's range, or vertical y-value where it actually occurs
	# > returns: (int) y-coordinate for how far down the widget this is
	def yco_from_index(self, index, use_midpoint=False):
		# get item
		item = self.model[index]
		
		# get extents of item itself
		if self.bounds_dirty or (item.ybounds is None):
			extents = self.calc_item_extents(item, includeSubtree=False)
		else:
			extents = item.ybounds
		
		# yco is midpoint of folder range
		if use_midpoint:
			return (extents[0] + extents[1]) / 2.0
		else:
			return extents[0]
		
	# item index from y-coordinate
	# ! Assumes that item bounds have been cached...
	# < y: (int) y-coordinate (i.e. vertical offset from top of widget)
	# > returns: (int) index for accessing item under mouse from self.model
	def index_from_yco(self, y):
		# get dimensions and mouse-coordinates
		w, h, high = self.calc_item_dimensions()
		
		# compute "index" score - the weight value to search for
		totWeight = self.calc_total_weight()
		wIndex = (y / h) * totWeight
		
		# perform tree traversal to find item/node that weight refers to...
		#print "index_from_yco(y = %d, w = %f)" % (y, wIndex)
		subtrees = self.roots	# ([FsTreeListItem]) list of subtree/child nodes to search
		baseWeight = 0.0		# (float) lowest weight value that first subtree in list can have - each nested subtree can only get larger
		d = 0
		
		tarItem = None			# (FsTreeListItem) last item which fit these criteria...
		found = False
		
		while (subtrees) and (found is False):
			# keep track of base weight for this subtree, in case we need to refer back later...
			startWeight = baseWeight
			
			# go over each child, and see which one contains the weight-range required
			for child in subtrees:
				if baseWeight <= wIndex < (baseWeight + child.weight + child.weightSum):
					# child is in this subtree - this child could be the node we want...
					tarItem = child
					break;
				else:
					# child not yet found, so baseWeight now must include the subtree we just skipped
					baseWeight += (child.weight + child.weightSum)
			else:
				# if we're still here after going over whole tree, we're right at the end?
				#print "Warning: Reached end of child search without match (y = %d, s = %d, d = %d, parent = %s)" % (y, wIndex, d, tarItem.data.path())
				tarItem = child
			
			# if target itself contains the weight, it is very likely we've found what we were looking for
			if baseWeight <= wIndex <= (baseWeight + tarItem.weight):
				# XXX: this is slightly cheating, as we're expanding the range a bit too much
				before, after = self.find_neighbouring_range_items(tarItem, HITBUCKET_SIZE, HITBUCKET_SIZE, sorted_order=False)
				
				# first item we want is the first in "before" whose int-clamped minbound is still within bounds
				for it in before:
					if int(it.ybounds[0]) == y:
						# found item was not first...
						tarItem = it
						found = True
						break;
				else:
					# tarItem is the first in its list
					#found = True
					pass
			
			# we may/may not have found our target at this level;
			# nevertheless, try to carry on to next depth
			# (i.e. we only know that subtree roote at this item contains the score)
			baseWeight += tarItem.weight    # include item itself - target wasn't found yet, but we know that this isn't it, so y-co must be greater!
			subtrees = tarItem.childItems
			d += 1
		
		if tarItem:
			index = self.model.index(tarItem)
		else:
			index = -1
		#print "y = ", y, "index = ", index, "d = ", d, "path = ", self.model[index].data.path()
		return index
		
	# Compute density of items - either at given point or in general
	# compute density of items (i.e. items per pixel row)
	# ! Keep this in sync with calc_item_dimensions()
	# > returns: (float) number of items per row
	def calc_item_density(self, item=None):
		# widget area - excluding borders
		h = float(self.height() - 3)
		
		# density is number of items per pixel
		if item:
			# density around item only
			# XXX?
			return 1.0 / (item.ybounds[1] - item.ybounds[0])
		else:
			# general density - assuming linear distribution of items
			# NOTE: this will only be an approximation at best
			# TODO: return as int?
			if h:
				return len(self.model) / h
			else:
				return 0 # len(self.model)
	
	# ----------------------------------------
	
	# Find indices of neighbouring items which fall within a fixed pixel range on either side of item
	# TODO...
	
	# Find neighbouring items which occur within a fixed number of pixels on either side of item
	# ! Assumes that item bounds are all set and correct
	# < item: (FsTreeListItem) item 
	# < px_above: (Num) number of pixels ABOVE item.yco to include
	# < px_below: (Num) number of pixels BELOW item.yco to include
	# < (sorted_order): (bool) is the "before" list sorted in normal order of appearance?
	# > returns: ([FsTreeListItem], [FsTreeListItem]) lists of items before and after (but not including item itself)
	def find_neighbouring_range_items(self, item, px_above, px_below, sorted_order=True):
		assert(self.bounds_dirty is False)
		
		# determine minimum and maximum pixel values...
		# NOTE: for now, assume that these extents *include* the pixel's bounds too, so wide targets only select themselves
		w, h, high = self.calc_item_dimensions()
		
		yco  = (item.ybounds[0] + item.ybounds[1]) / 2.0
		index = self.model.index(item)
		
		ymin = max(0, yco - px_above)
		ymax = min(h, yco + px_below)
		
		# traverse backwards from item to find other items which still fit within range...
		before = []
		for idx in xrange(index-1, 0, -1):
			# as long as upper bound of item sits after ymin, at least part of it must overlap!
			i = self.model[idx]
			if ymin <= i.ybounds[1]:
				before.append(i)
			else:
				break;
		
		if sorted_order:
			before.reverse()
		
		# traverse forwards from item to find other items which still fit within range...
		after = []
		for idx in xrange(index+1, len(self.model)):
			# as long as lower bound of item sits before ymax, at least part of it must overlap!
			i = self.model[idx]
			if ymax >= i.ybounds[0]:
				after.append(i)
			else:
				break;
				
		# return the two lists
		return before, after
	
	
	# build stack of parent items for given item
	# < item: (FsTreeListItem)
	# < (include_item): (bool) whether item is included in the stack or not
	# < (ascending_order): (bool) stack items are ordered in terms of depth
	def get_parent_stack(self, item, include_item=False, ascending_order=True):
		stack = []
		
		# include item itself as last item?
		if include_item:
			stack.append(item)
		
		# traverse up the parent hierarchy
		par = item.parItem
		while par:
			stack.append(par)
			par = par.parItem
		
		# sort in terms of depth?
		if ascending_order:
			stack.reverse()
		
		return stack
		
	# ----------------------------------------
	
	# == Tree Traversal Mechanics for Weight Finding ==
	#
	# Name | baseWeight | Weight | SubWeight | TWeight | Depth
	# -----+------------+--------+-----------+---------+-------
	# A    |     0      |   1    |     8     |    9    |   0
	# -B   |     1      |   1    |     3     |    4    |   1
	# --C  |     2      |   2    |     0     |    2    |   2
	# --D  |     4      |   1    |     0     |    1    |   2
	# -E   |     1      |   2    |     5     |    7    |   1
	# --F  |     3      |   2    |     3     |    5    |   2
	# ---G |     5      |   3    |     0     |    3    |   3
	#
	# TWeight = Weight + SubWeight     (= Total Weight occupied by a node and its subtree)
	#
	# Given a weight score, to find a whether a score fits within a node + its subtree:
	#	 node.baseWeight <= score < (node.baseWeight + node.TWeight)
	
	
	# item index from mouse event - adaptive/free browse technique
	# < mevt: (QMouseEvent)
	# > returns: (int) index for accessing item under mouse from self.model
	def index_from_mouse__adaptive_browse(self, mevt):
		# extents of canvas
		w, h, high = self.calc_item_dimensions()
		
		# click locations in local coordinates
		x = mevt.x() - 1
		y = mevt.y() - 1
		
		
		# compute "index" score - the weight value to search for
		totWeight = self.calc_total_weight()
		wIndex = (float(y) / h) * totWeight
		
		# now, from x-co: determine the approximate depth-level that x-co implies
		# NOTE: favour lower-depths more to make it easier to browse around the 3-4 level deep area
		#       where most folders reside...
		xfac  = float(x) / w					# [0, 1]
		
		#depth = xfac * self.max_depth				# linear method of calculating depth
		depth = log(xfac + 1) * self.max_depth	# +1 to avoid domain-error, push up low-values a bit to give them priority, scale up to depth levels
		
		depth = max(1,  int(ceil(depth)))      	# root-level access is pretty useless/unexpected, so prevent that...
		
		
		# traverse tree to find node which contains the weight we require,
		# but stop as soon as we reach an appropriate node which contains
		# this on the depth we require...
		subtrees = self.roots	# ([FsTreeListItem]) list of subtree/child nodes to search
		baseWeight = 0.0		# (float) lowest weight value that first subtree in list can have - each nested subtree can only get larger
		d = 0
		
		tarItem = None			# (FsTreeListItem) last item which fit these criteria...
		
		while (d < depth) and (subtrees):
			# go over each child, and see which one contains the weight-range required
			for child in subtrees:
				if baseWeight <= wIndex < (baseWeight + child.weight + child.weightSum):
					# child is in this subtree - this child could be the node we want...
					tarItem = child
					break;
				else:
					# child not yet found, so baseWeight now must include the subtree we just skipped
					baseWeight += (child.weight + child.weightSum)
			else:
				# if we're still here after going over whole tree, we're right at the end?
				#print "Warning: Reached end of child search without match (y = %d, s = %d, d = %d, parent = %s)" % (y, wIndex, d, tarItem.data.path())
				tarItem = child
			
			# XXX: what if the target lives right on the given item and not inside its subtree?
			
			# we may/may not have found our target at this level;
			# nevertheless, try to carry on to next depth
			# (i.e. we only know that subtree roote at this item contains the score)
			subtrees = tarItem.childItems
			d += 1
		
		
		# find index of last child found
		if tarItem in self.model:
			index = self.model.index(tarItem)
			#print "Index %d at %d -> %d" % (index, tarItem.depth, depth)
		else:
			index = -1
			print "Index <Invalid> for Not-Found at %d (%f items total, %d depth reached)" % (depth, len(self.model), d)
		
		# return index
		return index
		
	
	# Default fisheye browsing technique
	def index_from_mouse__fisheye(self, mevt):
		# get dimensions and mouse-coordinates
		w, h, high = self.calc_item_dimensions()
		
		mx = mevt.x() - 1
		my = mevt.y() - 1
		
		# compute "index" score - the weight value to search for
		totWeight = self.calc_total_weight()
		wIndex = (float(my) / h) * totWeight
		
		# perform tree traversal to find item/node that weight refers to...
		#print "index_from_mouse__fisheye(y = %d, w = %f)" % (y, wIndex)
		subtrees = self.roots	# ([FsTreeListItem]) list of subtree/child nodes to search
		baseWeight = 0.0		# (float) lowest weight value that first subtree in list can have - each nested subtree can only get larger
		d = 0                   # (int) depth in tree
		
		tarItem = None			# (FsTreeListItem) last item which fit these criteria...
		stack = []				# ([FsTreeListItem]) stack of items at various depths along ths pixel slice
		found = False
		
		while (subtrees) and (found is False):
			# keep track of base weight for this subtree, in case we need to refer back later...
			startWeight = baseWeight
			
			# go over each child, and see which one contains the weight-range required
			for child in subtrees:
				if baseWeight <= wIndex < (baseWeight + child.weight + child.weightSum):
					# child is in this subtree - this child could be the node we want...
					tarItem = child
					break;
				else:
					# child not yet found, so baseWeight now must include the subtree we just skipped
					baseWeight += (child.weight + child.weightSum)
			else:
				# if we're still here after going over whole tree, we're right at the end?
				#print "Warning: Reached end of child search without match (y = %d, s = %d, d = %d, parent = %s)" % (y, wIndex, d, tarItem.data.path())
				tarItem = child
			
			# if target itself contains the weight, it is very likely we've found what we were looking for
			if baseWeight <= wIndex <= (baseWeight + tarItem.weight):
				# BUT, ensure that this is salient - if not, we may have caught a
				# lower-level target which prevents access to a true salient target...
				if tarItem.important:
					#print "salient item found at d=%d" % (d)
					found = True
					break;
				else:
					# Search up and down for most salient item in vicinity... Otherwise, we may just catch a dud...
					before, after = self.find_neighbouring_range_items(tarItem, HITBUCKET_SIZE, HITBUCKET_SIZE, sorted_order=False)
					neighbours = before + [tarItem] + after
					
					salientKeyFunc = lambda x: (x.important, x.weight, x.data.mtime())
					salientTar = max(neighbours, key=salientKeyFunc)
					
					if salientTar != tarItem:
						#print "salient neighbour found at d=%d" % (d)
						tarItem = salientTar
						found = True
						break;
					else:
						#print "non salient item under cursor at d=%d... keep recursing" % (d)
						pass
			
			# grab items up to the maximum depth level that we are allowed to examine
			# - we may not need any of these, or we may need to traverse up from the end
			#   depending on various factors, as below...
			# - don't add anything deeper than a vicinity level that we can use, since
			#   we must start from the bottom of this stack and work back up
			if d <= self.vicinity_level:
				stack.append(tarItem)
			
			# we may/may not have found our target at this level;
			# nevertheless, try to carry on to next depth
			# (i.e. we only know that subtree roote at this item contains the score)
			baseWeight += tarItem.weight    # include item itself - target wasn't found yet, but we know that this isn't it, so y-co must be greater!
			subtrees = tarItem.childItems
			d += 1
		
		# make "vicinity first" selections easier?
		if self.use_vicinity_first:
			requires_common_ancestor = True
			
			# if we don't have a salient item, use the current "selection depth"
			# to determine how deep in the chain the target we fetch needs to be
			if (tarItem) and (tarItem.important):
				# we have a salient item already which does the trick...
				pass;
			elif requires_common_ancestor:
				# Starting from deepest item we're allowed to use, track upwards
				# until we find an item with a common ancestor to current selection
				#  - This way, siblings and children of the item can be browsed with 
				#    the desired precision, but everything else quickly degrades to
				#    rough levels outside that
				
				# Current item/location should be one level up from deepest item
				# on stack in most cases. Special handling needed here to fetch
				# as single-root case is not described in usual way
				if self.selected_index == -1:
					curItem = self.roots[0]
				else:
					curItem = self.model[self.selected_index]
					
				# grab the depth stack for the current location
				curItemStack = self.get_parent_stack(curItem, include_item=True, ascending_order=True)
				
				# ascend depth levels until we no longer have a common ancestor
				# TODO: we have no way of allowing scrollwhell with no clicking to do this in another region
				for d in range(len(stack)):
					# do we have freedom to descend several levels deeper than current location 
					# (within the current location, due to scrollwheel)?
					if d >= len(curItemStack):
						# we're still here, so we must be in a sub-subregion after using scrollwheel
						# -> all other ancestors were fine, so we should just take the deepest one
						#    we've got...
						tarItem = stack[-1]
						break;
					else:
						# still within bounds
						cAnc = curItemStack[d] # current item stack's ancestor at this depth
						tAnc = stack[d]        # tar item stack's ancestor at this depth
						
						# if these don't match, use ancestor one level down
						if cAnc != tAnc:
							# first non-common ancestor found
							if d == 0:         
								# base level - have to accept...
								# (NOTE: this case only fires when there are multiple roots0
								tarItem = tAnc
								break;
							elif d == self.vicinity_baseline:
								# base-level - have to accept...
								# (NOTE: this case fires when there is only a single root - toplevel root doesn't count)
								tarItem = stack[d]
								break;
							else:
								# go back down one level to common ancestor
								tarItem = stack[d - 1]
								break;
						else:
							# common ancestor: for now, this is our best bet 
							# - it will be used if this turns out to be the last item on both stacks
							tarItem = tAnc
							# continue;
				else:
					#print "Common Ancestor check fell through - d = %d, c = %d, t = %d" % (d, len(curItemStack), len(stack))
					#assert(tarItem in stack)
					pass
			else:
				# no common ancestor required - just use deepest item we're entitled to use
				tarItem = stack[-1]
				
		# find index of last child found
		if tarItem in self.model:
			index = self.model.index(tarItem)
			#print "Index %d at %d -> %d" % (index, tarItem.depth, depth)
		else:
			index = -1
			print "Index <Invalid> for Not-Found at %d (%f items total, %d depth reached)" % (depth, len(self.model), d)
		
		# return index
		return index
	
	# ................
	
	# item index from mouse event
	# < mevt: (QMouseEvent)
	# > returns: (int) index for accessing item under mouse from self.model
	def index_from_mouse(self, mevt):
		if self.sticky_tars:
			#index = self.index_from_mouse__bins(mevt)				# new method
			index = self.index_from_mouse__fisheye(mevt)			# standard fisheye technique
		else:
			index = self.index_from_mouse__adaptive_browse(mevt) 	# adaptive browse for fisheye
		
		return index
		
	# ----------------------------------------
	
	# hook for widget drawing/redrawing
	# TODO: can optimise by only redrawing region as given in evt
	def paintEvent(self, evt):
		p = qgui.QPainter()
		
		p.begin(self)
		
		# configure the painter so that everything will be visible
		# NOTE: we need to set everything manually as Linux does things differently to Windows
		p.setCompositionMode(qgui.QPainter.CompositionMode_SourceOver)
		
		p.setRenderHint(qgui.QPainter.NonCosmeticDefaultPen, True)
		p.setRenderHint(qgui.QPainter.Antialiasing, False)
		p.setRenderHint(qgui.QPainter.HighQualityAntialiasing, False)
		p.setRenderHint(qgui.QPainter.SmoothPixmapTransform, False)
		p.setOpacity(1.0)
		
		# backdrop of whole widget
		self.draw_background(p)
		
		# draw lens-range indicator
		# - area covered by items in lens
		if self.lens_spawner and self.lens_spawner.lens_instance:
			self.draw_lens_range(p)
		
		# draw summary browsing/current location indicators
		self.draw_selection(p)
		self.draw_highlight(p)
		
		# draw summary representation
		self.draw_folders(p)
		
		# when drawing linear, targets aren't really visible, so draw them again larger here...
		if not self.distort_space:
			self.draw_linear_targets(p)
		
		# draw flashing target - under bubble so that it's still clear what was activated
		if self.flash_index >= 0:
			self.draw_flashing_hint(p)
			
		# draw sticky targets for easier targetting on mouse over (if enabled)
		# (i.e. "bubble" for currently activated target)
		if self.sticky_tars:
			self.draw_targets(p)
			
		# draw activation area for lenses
		if self.lens_spawner:
			self.lens_spawner.hostDrawActivationArea(p)
		
		p.end()
		
	# draw background decorations for widget
	def draw_background(self, p):
		# widget area
		w = self.width()
		h = self.height()
		
		# get color palette
		cs = qgui.QPalette()
		
		# set drawing info, and draw solid fill
		border = qgui.QPen(cs.color(qgui.QPalette.Dark), 1)
		fill = qgui.QBrush(qgui.QColor("white"), qcore.Qt.SolidPattern)
		
		p.setPen(border)
		p.setBrush(fill)
		
		p.drawRect(0, 0, w-1, h-1)
		
	# draw range covered by lens
	def draw_lens_range(self, p):
		# widget area
		w = self.width() - 1
		
		# only draw if mouse is still within self... otherwise, the highlight index gets updated...
		lens = self.lens_spawner.lens_instance
		#if lens.isUnderMouse():
		#	# mouse is under lens - should only show the range that is within the lens
		#	return;
		
		# highlight range
		start_item = self.model[lens.start_idx]
		end_item = self.model[lens.end_idx]
		
		if None in (start_item.ybounds, end_item.ybounds):
			# don't draw - no bounds
			return;
			
		ymin = start_item.ybounds[0]
		ymax = end_item.ybounds[1]
		
		h = max(3, ymax - ymin)
		
		# color - same as used for lens highlight
		cs = qgui.QPalette()
		
		col = cs.color(qgui.QPalette.Highlight)
		col.setAlpha(90)
		
		# draw rect spanning this range
		p.setPen(qcore.Qt.NoPen)
		p.setBrush(col)
		
		p.drawRect(1, ymin, w, h)
		
	# draw extents of selected item (i.e. where we currently are)
	def draw_selection(self, p):
		# abort if no selected item
		if self.selected_index < 0:
			return;
		
		# get dimensions of where to draw the brush
		w = self.width()
		
		# helper function for performing the drawing
		# < item: (FsTreeListItem) item to draw subtree of
		# < alpha: (int - 0 to 255) intensity of subtree visibility
		def draw_selection_subtree(item, alpha):
			# get extents of this item (and subtree)
			extents = self.calc_item_extents(item)
			
			iy = extents[0]
			ih = extents[1] - extents[0]
			
			# can't be too small...
			if ih <= 3:
				ih = 3         # 3-5px minimum to ensure visible without being misleading
				iy -= ih / 2.0 # ensure centered
			
			# draw a filled rect indicating this region
			p.setPen(qcore.Qt.NoPen)
			
			fill = qgui.QColor(0, 0, 0, alpha)
			p.setBrush(fill)
			
			p.drawRect(1, iy, w-2, ih) 
			
			# draw lines to indicate top and bottom of this 
			p.setPen(qgui.QColor(0, 0, 0, 60)) # faint line
			p.setBrush(qcore.Qt.NoBrush)
			
			ys = ih
			p.drawLine(1, iy,  w-2, iy)    # top
			
			ye = iy + ih - 1
			p.drawLine(1, ye,  w-2, ye)    # bottom
		
		# selected index -> selected subtree (to be drawn last)
		sel = self.model[self.selected_index]
		
		# when vicinity-first is enabled, we need to visualise the selection steps
		if self.use_vicinity_first:
			# unroll our parent subtrees, and draw their regions
			par = sel.parItem
			while par:
				# only draw if this not a solo root folder 
				# - otherwise, entire widget gets shaded, which is confusing
				if (par.parItem) or (len(self.roots) > 1):
					# quite faint, but will build up as layers accumulate
					draw_selection_subtree(par, 35)
				
				# go to parent
				par = par.parItem
			
			# finally, draw the currently selected subtree
			# NOTE: if this is at root level, it doesn't need to be too strong
			if sel.depth:
				draw_selection_subtree(sel, 35)  # non-root current location
			else:
				draw_selection_subtree(sel, 35)  # root current location    (50 alpha looks good too)
		else:
			# draw only the selected subtree - no hierarchy info needs to be considered
			draw_selection_subtree(sel, 90)
		
	# draw highlight - shows full range that highlighted item affects
	def draw_highlight(self, p):
		cs = qgui.QPalette()
		
		# get dimensions of where to draw the brush
		w = self.width()
		
		# highlight item - just the row under the cursor (mouse hover in main list)
		if self.highlight_index >= 0:
			# get extents of this item (and subtree)
			index = self.highlight_index
			extents = self.calc_item_extents(self.model[index])
			
			iy =     extents[0]
			ih = max(extents[1] - extents[0], 3) # can't be too small, or else invisible
			
			# set shading
			p.setPen(qcore.Qt.NoPen)
			
			hlCol = cs.color(qgui.QPalette.Highlight)
			hlCol.setAlpha(150)
			p.setBrush(hlCol)
			
			# draw
			p.drawRect(1, iy-1, w-2, ih)
				
	# draw flashing highlight for briefly showing item we've navigated to
	def draw_flashing_hint(self, p):
		cs = qgui.QPalette()
		
		# get dimensions of where to draw the brush
		w = self.width()
		
		# get extents of this item (and subtree)
		index = self.flash_index
		extents = self.calc_item_extents(self.model[index])
		
		iy = extents[0]
		ih = extents[1] - extents[0]
		
		# can't be too small...
		if ih <= 1:
			ih = 10 # 10px minimum to ensure visible (can't have some leeway to be visible)
			iy -= 5 # ensure centered
		
		# update brush color
		p.setPen(qcore.Qt.NoPen)
		
		fill = cs.color(qgui.QPalette.Highlight) # system highlight/sel color - darker than our select one...
		fill.setAlpha(self.flash_alpha)
		p.setBrush(fill)
		
		# draw!
		p.drawRect(1, iy, w-2, ih)
	
	# ............
	
	# draw lines indicating folder structure
	def draw_folders(self, p):
		w, h, high = self.calc_item_dimensions()
		
		# pen
		pen = qgui.QPen()
		pen.setWidthF(1.0)
		pen.setCapStyle(qcore.Qt.RoundCap)
		p.setPen(pen)
		
		fill = qgui.QBrush(qcore.Qt.SolidPattern)
		p.setBrush(fill)
		
		# draw the items
		if self.bounds_dirty:
			# traverse tree to draw items...
			totWeight = self.calc_total_weight()
			self.recursive_folder_draw(p, pen, fill, w, None, self.roots, totWeight, 1, h, 0)
			
			# clear dirty flag - bounds have now been updated/stored
			self.bounds_dirty = False
		else:
			# use cached values, but still do this recursively so that stacking remains the same
			self.recursive_folder_draw_cached(p, pen, fill, w, None, self.roots, 0)
	
	
	# Recursive drawing function - computed on the fly...
	def recursive_folder_draw(self, p, pen, fill, w, folder, children, weight, ymin, ymax, depth):
		# 1) partition up drawing space
		frange, subtrees, sRangeTot = self.partition_folder_weights(folder, children, weight, ymin, ymax)
		#print "%d: (%s) ||\n   %s ||\n   %s" % (depth, frange, subtrees, sRangeTot)
		
		# 2) draw folder (and store new bounds for later)
		if folder:
			self.draw_folder_item(p, pen, fill, w, folder, frange)
			folder.ybounds = frange
			
		# 3) subtree recurse...
		for child, sweights in zip(children, subtrees):
			cWeightTot = child.weight + child.weightSum
			
			# if no-weight, only draw if below a certain depth...
			if cWeightTot == 0.0:
				# no-weight, but still need to draw or else nothing shows up...
				if True: #depth < 2:
					self.recursive_folder_draw(p, pen, fill, w, 
												child, child.childItems,
												cWeightTot,
												sweights[0], sweights[1],
												depth + 1)
			else:
				# there's stuff to draw here...
				self.recursive_folder_draw(p, pen, fill, w, 
											child, child.childItems,
											cWeightTot,
											sweights[0], sweights[1],
											depth + 1)
	
	# Recursive drawing function - uses precomputed values for faster updates
	def recursive_folder_draw_cached(self, p, pen, fill, w, folder, children, depth):
		# 1) draw folder
		if folder:
			self.draw_folder_item(p, pen, fill, w, folder, folder.ybounds)
			
		# 2) draw children
		for child in children:
			self.recursive_folder_draw_cached(p, pen, fill, w, child, child.childItems, depth + 1)
	
	
	# draw a folder as a line
	# < p: (QPainter)
	# < pen: (QPen)
	# < fill: (QBrush | QColor)
	# < w: (int) width of widget
	# < item: (FsTreeListItem)
	# < yext: (float, float) ymin, ymax for line
	def draw_folder_item(self, p, pen, fill, w, item, yext):
		# start is given by width of indent
		# 	+1 is for the border
		xs = item.depth * self.indent + 1
		
		# end is relative to the proportion of the max horizontal length
		# 	- minimum size is set to ensure that all is always visible
		#	- draw important items larger
		if item.important:
			xw = 20
		else:
			xw = max(2, (item.length / self.max_len) * w)
			#xw = 5    # XXX: experimental fixed line width - too strong/blocky effect
		xe = xs + xw
		
		# y-extents
		#	- height of item varies based on salience of item
		# 	- draw important items larger
		TARSIZE_NATURAL, TARSIZE_FIXED, TARSIZE_ADAPTIVE = range(3)
		target_size = TARSIZE_NATURAL
		
		if item.important:
			if target_size == TARSIZE_NATURAL:
				yh = max(3,  yext[1] - yext[0])
			elif target_size == TARSIZE_FIXED:
				yh = 3
			elif target_size == TARSIZE_ADAPTIVE:
				yh = 3 * pow(item.tMult / 20.0, 2)
			
			y  = (yext[0] + yext[1]) / 2.0
			
			ys = y - (yh / 2.0)
			ye = y + yh
		else:
			if target_size == TARSIZE_NATURAL:
				yh = yext[1] - yext[0]
			elif target_size == TARSIZE_FIXED:
				yh = 1
			elif target_size == TARSIZE_ADAPTIVE:
				yh = 1 * pow(item.tMult / 20.0, 0.0001)
			
			y  = (yext[0] + yext[1]) / 2.0
			
			ys = yext[0]
			ye = yext[1]
		
		# color
		col = qgui.QColor(*item.color)
		
		# set color...
		if target_size == TARSIZE_NATURAL:
			if os.name == 'posix':
				pen.setColor(col.darker(200))
			else:
				pen.setColor(col.darker(150))
		else:
			pen.setColor(col)
			
		p.setPen(pen)
		fill.setColor(col)
		p.setBrush(fill)
		
		# draw line
		# - for salient targets, draw as rects instead
		if yh > 1:
			p.drawRect(xs, ys,  xw, yh)
		else:
			p.drawLine(xs, y,  xe, y)
	
	# ............
	
	# like draw_folders(), except that we only draw all the salient targets for "linear" condition
	# as they don't show up too well in the regular view...
	def draw_linear_targets(self, p):
		w, h, high = self.calc_item_dimensions()
		
		# temporarily change composition mode so that we don't blank out the base color
		p.save()
		p.setCompositionMode(qgui.QPainter.CompositionMode_Darken)
		
		# configure pen and fill
		p.setPen(qcore.Qt.NoPen)
		
		fill = qgui.QBrush(qcore.Qt.SolidPattern)
		p.setBrush(fill)
		
		# draw the items
		if not self.bounds_dirty:
			# use cached values from previous pass, but still do this recursively so that stacking remains the same
			self.recursive_linear_targets_draw_cached(p, fill, w, None, self.roots, 0)
			
		# clear temp compo-mode change
		p.restore()
			
	# Recursive drawing function - uses precomputed values for faster updates
	def recursive_linear_targets_draw_cached(self, p, fill, w, folder, children, depth):
		# 1) draw folder - but only if is worthy of being drawn/highlighted
		if folder and folder.important:
			# color
			col = qgui.QColor(*folder.color)
			#col.setAlpha(128) # not full opacity, so that shape is still visible  # XXX: check on this
			
			fill.setColor(col)
			p.setBrush(fill)
			
			# compute y-extents
			# - it should be about 3px tall, or else it will never be seen
			yext = folder.ybounds
			y  = (yext[0] + yext[1]) / 2.0
			
			yh = 3
			
			ys = y - (yh / 2.0)
			ye = y + yh
			
			# draw line - full way across widget
			p.drawRect(1, ys,  w, yh)
			
		# 2) draw children
		for child in children:
			self.recursive_linear_targets_draw_cached(p, fill, w, child, child.childItems, depth + 1)
	
	# ............
	
	# like draw_folders(), except that we only highlight most salient target based on distance to mouse
	# - currently, we just take the "highlighted" item
	def draw_targets(self, p):
		w, h, high = self.calc_item_dimensions()
		
		# only draw the item at the highlight index
		if self.highlight_index < 0:
			return
			
		# get the item
		item = self.model[self.highlight_index]
		
		# don't draw sticky target if it isn't important - lens covers this case anyway...
		if not item.important:
			return
		
		# start is given by width of indent
		# 	+1 is for the border
		xs = item.depth * self.indent + 1
		
		# end is relative to the proportion of the max horizontal length
		# 	- take the smaller of the sizes so that we don't overflow over the edge
		#	- tMult is multiplier factor based on newness
		xw = min(w - xs, HITBUCKET_HIGH * item.tMult)
		#xe = xs + xw
		
		# y-extents
		# - base height is the hitbucket height -> small items appear to grow larger
		# - for anything larger than that, we want the target's size (for those given higher rankings)
		item_extents = self.calc_item_extents(item, includeSubtree=False)
		yh = max(HITBUCKET_HIGH, item_extents[1] - item_extents[0])
		
		y  = self.yco_from_index(self.highlight_index, use_midpoint=True)
		ys = y - (yh / 2.0)
		#ye = y + yh
		
		# color
		col = qgui.QColor(*item.color)
		col.setAlpha(225) # nearly fully visible over everything else
		
		fill = qgui.QBrush(col)
		p.setBrush(fill)
		
		pen = qgui.QPen(qgui.QBrush(col.darker(150)), 1.0, cap=qcore.Qt.RoundCap)
		p.setPen(pen)
		
		# draw as a big juicy rect
		# - rounded corners for a more "click me" affordance
		p.drawRoundedRect(xs, ys,  xw, yh,  2.0, 2.0)
		
	# ----------------------------------------
		
	# generate and set tooltip for highlighted region under cursor (active targets)
	def generate_tooltip(self):
		# only generate tooltip if tooltips are enabled (i.e. when not using lenses at all)
		# XXX: perhaps we can still show these until the lenses are shown?
		if not self.show_tooltips:
			self.setToolTip("") # don't show any tooltips!
			return
		
		# only generate when we're mousing over the widget
		if self.highlight_index < 0:
			#self.setToolTip(FishOverviewBar.DEFAULT_TOOLTIP)
			self.setToolTip("") # don't show anything for now... too distracting
			return
			
		# find item that we're referring to
		item = self.model[self.highlight_index]
		if not item:
			self.setToolTip("") # internal error, so no helptext
			return
			
		# extract some interesting items to display
		folders = item.data.path(fullPath=False)
		
		sep = os.path.sep
		path = sep.join(folders)
		
		if len(folders) > 3:
			# just last two items form the "name"
			
			# TODO: if the last two were very similar, maybe we should use the one above instead?
			#		and if the last these two happened to be part of a single-link chain, then we might want to show key nodes there
			# TODO: review the order and how we present these...
			# TODO: we need relative paths, not the full path when doing this...
			# TODO: shorten very long foldernames (proper ranking algorithm instead could be better for getting predictions)
			
			name = "%s - [<i>%s</i>]" % (folders[-1], folders[-2])
		else:
			# just last part
			name = item.name
			
		# set text
		# - first line is used to ensure that all line-breaks are explicit
		# TODO: do some smart displays which extract out the "salient" features of the path
		tooltip  = "<p style='white-space:pre'>"
		
		tooltip += "<b>%s</b> <br/>" % (name)
		tooltip += "<i>%s</i>" % (path) # XXX: salient feature extraction should come here
		
		tooltip += "</p>"
		
		self.setToolTip(tooltip)
		
	
	# force the tooltip to be shown (on mouse move)
	def force_tooltip(self, event):
		if self.highlight_index >= 0:
			# show current label
			qgui.QToolTip.showText(event.globalPos(), self.toolTip())
		else:
			# hide label for now, until a timer triggers it to show normally
			qgui.QToolTip.hideText()
		
	# --------------------------------------------------
		
	# override generic events to capture the ones we're interested in overriding
	def event(self, event):
		# catch tooltip events so that we respond with less delay
		if event.type() == qcore.QEvent.ToolTip:
			# force tooltip to be displayed normally (help text and other cases get shown via this)			
			qgui.QToolTip.showText(event.globalPos(), self.toolTip())
			return True
		
		return super(FishOverviewBar, self).event(event)
		
	# override resize event to recalculate bounds of items
	def resizeEvent(self, evt):	
		super(FishOverviewBar, self).resizeEvent(evt)
		self.bounds_dirty = True
	
	
	# mouse left widget
	def leaveEvent(self, evt):
		self.sticky_tars = False     # disable sticky when leaving widget (so that previewing in other widgets doesn't look bad)
		self.clearFocus()            # when mouse leaves, focus should transfer away (so shift doesn't continue to lock)
		
		self.mouseExit.emit()
		super(FishOverviewBar, self).leaveEvent(evt)
		
	# mouse entered widget
	def enterEvent(self, evt):
		self.sticky_tars = True      # enable sticky when entering (set default)
		self.setFocus()              # force widget to get focus (on mouseover) so that we can catch keyboard events
		
		super(FishOverviewBar, self).enterEvent(evt)
	
	# --------------------------------------------------
	
	# mouse click event in widget
	def mousePressEvent(self, mevt):
		if mevt.button() == qcore.Qt.LeftButton:
			# handle item select
			index = self.index_from_mouse(mevt)
			self.itemClicked.emit(index)
			
			# if vicinity-first handling is in action, this should also advance the depth
			# and cause a browse-level refresh, allowing multiple clicks to descend down
			# the hierarchy without moving
			if self.use_vicinity_first:
				if index >= 0:
					# valid item - go down one level
					item = self.model[index]
					self.vicinity_level = item.depth + 1
				else:
					# invalid item - something happened, so go back to baseline
					self.vicinity_level = self.vicinity_baseline #+ 1
				
				# start previewing with updated level
				self.mouseMoveEvent(mevt)
		else:
			super(FishOverviewBar, self).mousePressEvent(mevt)
		
	# mouse-move event in widget
	def mouseMoveEvent(self, mevt):
		if LENS_DEBUG: print "$ FISH: mmove (%d, %d)" % (mevt.x(), mevt.y())
		
		# toggle lens enabled...
		# - lens is shown when SHIFT is held
		# - tooltips cannot be shown at same time as lens
		self.show_tooltips = not (bool(mevt.modifiers() & qcore.Qt.ShiftModifier))
		
		# check if mouse-move has been hijacked by floating lens activation area
		# NOTE: this prevents problems with indices changing on the way to a target
		if self.lens_spawner and self.lens_spawner.handleHostMouseMove(mevt):
			if LENS_DEBUG: print "FISH: aborted! Caught by lens activation area"
			return;
		
		# toggle sticky tars (aka normal browse mode)
		# - enable by default, and disable only if ALT pressed
		self.sticky_tars = not bool(mevt.modifiers() & qcore.Qt.AltModifier)
		
		# toggle vicinity-first control
		# - enable by default, and disable only if CTRL pressed (i.e. this acts like a precision toggle)
		self.use_vicinity_first = not bool(mevt.modifiers() & qcore.Qt.ControlModifier)
		
		
		# update the highlight index
		old_highlight_index  = self.highlight_index
		self.highlight_index = self.index_from_mouse(mevt)
		
		# set tooltip to mention what folder we've got
		# then force it to be shown immediate
		self.generate_tooltip()
		self.force_tooltip(mevt)
		
		# generate event to say whether we've hit anything or not
		if self.highlight_index >= 0:
			# a target was gained (maybe a near distance from previous)
			if LENS_DEBUG: print "FISH: new target - idx = %d" % (self.highlight_index)
			self.targetEntered.emit(mevt.y())
		else:
			# target lost when moving vertically...
			if LENS_DEBUG: print "FISH: target lost"
			self.targetLost.emit()
			
		# force repaint of widget at last
		self.repaint()
	
	# --------------------------------------------------
	
	# Helper - construct dummy mouse-move event from keypress...
	def fireDummyMouseMoveEvent(self, evt):
		# grab current state values - so that they get into closure and get cached up so they remain valid when run)
		pos  = self.mapFromGlobal(qgui.QCursor.pos())
		modifiers = evt.modifiers()
		
		# dummy function to schedule up...
		def scheduled_dummy_mevt():
			# construct fake mouse-move event					
			# XXX: button and buttons
			mevt = qgui.QMouseEvent(qcore.QEvent.MouseMove, pos, 
									qcore.Qt.NoButton, qcore.Qt.NoButton,
									modifiers)
			
			# send fake mouse-move event
			self.mouseMoveEvent(mevt)
		
		# schedule a dummy event to get pushed in a few ms time
		# - this is necessary, as otherwise we block the shift event from getting through,
		#   which in turns doesn't get flushed in the main event pipeline...
		qcore.QTimer.singleShot(5, scheduled_dummy_mevt)
	
	
	# Key Press Handling
	def keyPressEvent(self, evt):
		key = evt.key()
		
		if key in (qcore.Qt.Key_Shift, qcore.Qt.Key_Alt):
			# toggle different browsing behaviours
			self.fireDummyMouseMoveEvent(evt)
			evt.accept()
		elif key == qcore.Qt.Key_Control:
			# toggle different browsing behaviour + reset vicinity first control too
			self.reset_vicinity_level()
			
			self.fireDummyMouseMoveEvent(evt)
			evt.accept()
		else:
			# normal hanlding
			super(FishOverviewBar, self).keyPressEvent(evt)
			
	# Key Release Handling
	def keyReleaseEvent(self, evt):
		key = evt.key()
		
		if key == qcore.Qt.Key_Alt:
			# toggle different browsing behaviour - back to sticky targets...
			self.fireDummyMouseMoveEvent(evt)
			evt.accept()
		elif key == qcore.Qt.Key_Control:
			# toggle different browsing behaviour + reset vicinity first control
			self.reset_vicinity_level()
			
			self.fireDummyMouseMoveEvent(evt)
			evt.accept()
		elif key == qcore.Qt.Key_Shift:
			# no need to go through all the normal trouble...
			if self.lens_spawner:
				# kill current lens and/or its activation area
				self.lens_spawner.kill_lenses()
				
				# repaint self to not show the problems anymore
				self.repaint()
			
			# handled!
			evt.accept()
		else:
			# normal handling
			super(FishOverviewBar, self).keyReleaseEvent(evt)
		
	# Mousewheel Event
	# < evt: (QWheelEvent)
	def wheelEvent(self, evt):
		# determine direction of movement
		steps = evt.delta() / 120
		if steps == 0: return # ignore no movement...
		
		# adjust precision level accordingly
		SCROLL_DIR_NORMAL = 0  # zoom in = up
		SCROLL_DIR_NATURAL = 1 # reduce area size = down
		
		scroll_direction = SCROLL_DIR_NORMAL
		if scroll_direction == SCROLL_DIR_NORMAL:
			# - UP = lower in hierarchy
			# - DOWN = higher in hierarchy
			self.vicinity_level += steps
		else:
			# - UP = higher in hierarchy
			#   DOWN = lower in hierarchy
			self.vicinity_level -= steps
		
		# we must clamp level to remain within acceptable bounds
		# - maximum is set to be just half max-depth, since before max depth
		#   we're already down to items of a pixel high
		max_level = self.max_depth / 2
		#max_level = self.max_depth
		
		if self.vicinity_level < self.vicinity_baseline:
			self.vicinity_level = self.vicinity_baseline
			qgui.QApplication.beep()
		elif self.vicinity_level > max_level:
			self.vicinity_level = max_level
			qgui.QApplication.beep()
			
		# adjust selection area
		self.fireDummyMouseMoveEvent(evt)
		
		# handled!
		evt.accept()
		
	# --------------------------------------------------
	
	# Convenience method exposed as external API for changing highlight
	# < index: (int) display-model index
	def setHighlightIndex(self, index):
		self.highlight_index = int(index)
		self.repaint()
	
	# --------------------------------------------------
	
	# create menu event
	def build_context_menu(self):
		self.context_menu = menu = qgui.QMenu(self)
		
		# 1) help
		actHelp = menu.addAction("What's this?")
		actHelp.triggered.connect(self.showScoftHelp)
		menu.setDefaultAction(actHelp)
		
		actIgnore = menu.addAction("Hide Region...")
		actIgnore.setToolTip("Do not display this folder or its subfolders in SCOFT.\nUse this to hide large and useless folders")
		actIgnore.triggered.connect(self.ignoreTree)
		
		menu.addSeparator()
		
		# 3) tag a random item
		actTagRandomItem = menu.addAction("Tag Random Folder...")
		actTagRandomItem.setToolTip("Show me a random folder to try to visit")
		actTagRandomItem.triggered.connect(self.tagRandomItem)
		
		menu.addSeparator()
		
		# 2) linear/non-linear display space toggle
		actDistortionToggle = menu.addAction("Enlarge Salient Targets...")
		actDistortionToggle.setToolTip("If enabled, distort spatial distribution of items to make salient items easier to target")
		
		actDistortionToggle.setCheckable(True)
		actDistortionToggle.setChecked(self.distort_space)
		
		actDistortionToggle.toggled.connect(self.toggleSpaceDistortion)
	
	# context menu
	def contextMenuEvent(self, event):
		pt = self.mapToGlobal(event.pos())
		res = self.context_menu.exec_(pt)
		
		# handle other events which couldn't be handled
		#if res == ...:
		#	pass;
	
	
	
	# Show help message for SCOFT
	def showScoftHelp(self):
		# target name...
		if self.highlight_index >= 0:
			item = self.model[self.highlight_index]
			
			itemName = "%s" % (item.name)
			#parName  = "(in <i>%s</i>)" % (item.parItem.name) if item.parItem else ""
			parName  = ""
			path     = item.data.path(asStr=True)
			
			curItem = "<hr /><p><b>Current Tag:</b> &nbsp; &nbsp; %s %s <br /><i>%s</i></p>" % (itemName, parName, path)
		else:
			curItem = ""
		
		# construct full message...
		msg = self.DEFAULT_TOOLTIP + curItem
		qgui.QMessageBox.information(self, "About SCOFT", msg, 0)
	
	# Hide a folder and its subtree - useful for removing large and unwanted folders
	def ignoreTree(self):
		# ignore folder and its subtree...
		if self.highlight_index >= 0:
			item = self.model[self.highlight_index]
			
			folder = item.data
			folder.ignoreTree()
			
			# force recalc of filters - since this is similar to changing the filters
			self.filters.filtersChanged.emit()
	
	# test code to tag a random item, and allow users to play a little game of "find the item"
	def tagRandomItem(self):
		# construct a new spawner...
		try:
			# clear existing one - let's us test if there's one already
			self.tagSpawner.clear()
		except:
			# create a new one
			self.tagSpawner = tagLabels.TagSpawner([self])
		
		# randomly pick an item
		item = random.choice(self.model)
		
		# determine its size
		ybounds = self.calc_item_extents(item, False)
		y  = (ybounds[0] + ybounds[1]) / 2.0
		
		# figure out what cue to display
		parentPath = item.data.path(asStr=True, fullPath=False)
		lbl = "<b>%s</b><br /><i>%s</i>" % (item.name, self.tagSpawner.prepareLongText(parentPath, '<br />'))
		
		# add show tag
		self.tagSpawner.addTextToLeft(lbl, y, tagLabels.TAG_INFO)
		
		# show tag(s)
		self.tagSpawner.finalise()
		self.tagSpawner.showAll()
		
		
		
		# bind close-tag event to successfully getting there
		try:
			folderTreeWidget = self.parent()
			navmodel = folderTreeWidget.navmodel
		except:
			print "Oops... no folder-tree widget as hoped for with hack"
			navmodel = None
		
		def postNavCheck():
			folder = navmodel.location
			
			if folder == item.data:
				# got there!
				# TODO: add a little "time taken" hint too...
				self.tagSpawner.clear()
				tag = self.tagSpawner.addTextToRight("<b>Yay! Gotcha!</b> &nbsp; Try another one?", y, tagLabels.TAG_GOOD)
				self.tagSpawner.finalise()
				self.tagSpawner.showSequentially()
				
				# if click occurs on this, close it
				tag.clicked.connect(self.tagRandomItem) # pick another target...
			else:
				# not yet there...
				if len(self.tagSpawner) == 1:
					tag = self.tagSpawner.addTextToRight("<b>Nuhah!</b> &nbsp; Not this one...", y, tagLabels.TAG_ERROR)
					tag.showTag()
				else:
					tag = self.tagSpawner.tags[-1]
					tag.showTag() # reshow it
		
		if navmodel:
			navmodel.locationChanged.connect(postNavCheck)


