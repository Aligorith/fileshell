#!/usr/bin/python
#
# Bidirectional time-range selector
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import bisect
import math

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from timeRange import *
from fileshell.model.fsutils import *

##########################################
# Time Range Slider

# Handle for bidirectional time slider
# TODO: restore custom drawing?
# TODO: use abstractbutton instead?
class BidirHandle(qgui.QPushButton):	
	# Dimensions of tag
	WIDTH = 15
	HEIGHT = 20
	HWIDTH = WIDTH / 2.0
	PAD = 1
	
	# Custom signals
	valueChanged = qcore.pyqtSignal([float], [])
	
	# ctor
	def __init__(self, slider, ctrl):
		qgui.QPushButton.__init__(self, slider)
		
		# widget settings
		self.resize(BidirHandle.WIDTH, BidirHandle.HEIGHT)
		
		self.setMouseTracking(True)
		self.setCursor(qcore.Qt.SplitHCursor)
		
		# behavioural settings - what we're being done for
		self.slider = slider
		self.ctrl = ctrl
		
		# previous value
		self.mx = None
		
		# internal events - trigger update on movement
		self.valueChanged.connect(self.updatePosition)
		
	# Value =======================================================
		
	# utility property to get value that handle represents
	def value():
		def fget(self):
			# get value from slider
			return getattr(self.slider, self.ctrl)
			
		def fset(self, val):
			# clamp
			# - to never go past the other one
			# XXX: they shouldn't be allowed to overlap!
			if self.ctrl == 'start':
				val = min(val, self.slider.end)
			if self.ctrl == 'end':
				val = max(val, self.slider.start)
			# - to never go out of bounds
			val = min(1.0, max(0.0, val))
			
			# set value to slider now
			setattr(self.slider, self.ctrl, val)
			
		return locals()
	value = property(**value())
	
	# recalculate position relative to parent
	def updatePosition(self):
		w = self.slider.width()
		h = self.slider.height()
		
		# calculate coordinates
		# - handle position is always given as "centered"
		x = (self.value * w) - BidirHandle.HWIDTH
		y = h - BidirHandle.HEIGHT - BidirHandle.PAD
		
		self.move(x, y)
	
	# Event Handling ==============================================
	
	# hack to force tooltips to be shown
	def force_tooltip(self, evt):
		# force tooltip from slider to be shown
		#qgui.QToolTip.showText(evt.globalPos(), self.slider.toolTip())
		qgui.QToolTip.showText(evt.globalPos(), self.toolTip())
	
	# override generic events to capture the ones we're interested in overriding
	def event(self, event):
		# catch tooltip events so that we respond with less delay
		if event.type() == qcore.QEvent.ToolTip:
			self.force_tooltip(event)
			return True
		
		return super(BidirHandle, self).event(event)
	
	# Start of mouse movement
	def mousePressEvent(self, mevt):
		# default handling
		super(BidirHandle, self).mousePressEvent(mevt)
		
		# initiate moving
		self.mx = mevt.x()
	
	# Mouse-move handling
	def mouseMoveEvent(self, mevt):
		# if moving...
		if self.isDown() and self.mx:
			# apply delta movement
			x = mevt.x()
			dx = x - self.mx
			
			# inverse from pixels to relative offset
			w = self.slider.width()
			
			delta = float(dx) / w
			self.value += delta
			
			# send events
			# XXX: is the wait-cursor really needed? This is supposed to be short!
			qgui.QApplication.setOverrideCursor(qcore.Qt.WaitCursor)
			self.valueChanged.emit(self.value)
			qgui.QApplication.restoreOverrideCursor()
			
			# force tooltip to be shown
			# WARNING: this MUST be done after valueChanged(), as only then will
			#          the tooltip contents actually be correct!
			self.force_tooltip(mevt)
			
	# End of mouse move event
	def mouseReleaseEvent(self, mevt):
		# default handling
		super(BidirHandle, self).mouseReleaseEvent(mevt)
		
		# stop moving
		self.mx = None
		
		# send event that value has changed (so that final flush will work)
		qgui.QApplication.setOverrideCursor(qcore.Qt.WaitCursor)
		self.valueChanged.emit(self.value)
		qgui.QApplication.restoreOverrideCursor()

# =======================================

# Marker positions in the slider
# XXX: need to be able to fetch values for easy comparisons
class TimeMarker(object):
	# ctor
	# < pos: (float) position as parametric value of overall width
	# < label: (str) label to display in position for this item
	# < tdelta: (TimeDelta) that this corresponds to
	def __init__(self, pos, label, tdelta):
		self.pos = pos
		self.label = label
		
		self.tdelta = tdelta

# =======================================

# Bidirectional Time Slider
class TimeRangeSlider(qgui.QWidget):
	# Standard Labels
	LABELS = ["Older", "Year", "6 Months", "Month", "Week", 
			  "Day", "3 Hours", "Hour", "Now"]
	# Mapping from Standard Labels to TimeRange labels
	TRL_MAP = {
		"Older"		: "Older",
		"Year" 		: "LastYear",
		"6 Months"	: "LastHalfYear",
		"Month"		: "LastMonth",
		"Week"		: "LastWeek",
		"Day"		: "LastDay",
		"3 Hours"	: "LastFewHours",
		"Hour"		: "LastHour",
		"Now"		: "Now"
	}
	
	# custom signal emitted when the time range changes
	timeRangeChanged = qcore.pyqtSignal()
	
	# ctor
	# < time_model: (TimeModel)
	# < (movable_gradient): (bool) gradient is only for the region defined by the handles
	def __init__(self, time_model, movable_gradient=True):
		qgui.QWidget.__init__(self)
		
		# data model
		self.time_model = time_model
		
		# drawing data
		self.calc_markers()
		
		# setup handles
		self.start = 0.0
		self.end = 1.0
		
		self.tmin = 0.0		# min/max timestamps that handles correspond to
		self.tmax = 1.0
		
		self.movable_gradient = movable_gradient
		
		self.setup_handles()
		self.setup_hotkeys()
		
		self.calc_gradient_stops()
		
		# adjust dimensions and other stuff like that, now that all components exist
		self.setMinimumSize(360, 40)
		
		# hook up updating code
		self.timeRangeChanged.connect(self.update_display)
		self.resetRange() # to get everything into the right states...
		
	# Setup ==================================================
		
	# setup handles
	def setup_handles(self):
		# create widgets
		self.sCtrl = BidirHandle(self, 'start')
		self.eCtrl = BidirHandle(self, 'end')
		
		# bind updates
		self.sCtrl.valueChanged.connect(self.timeRangeChanged)
		self.eCtrl.valueChanged.connect(self.timeRangeChanged)
		
		# additional mouse handling
		self.setMouseTracking(True)
		self.mx = None
		
	# setup convenience hotkeys
	def setup_hotkeys(self):
		# Home - reset range
		homeShortcut = qgui.QShortcut(qgui.QKeySequence("Home"), self)
		homeShortcut.activated.connect(self.resetRange)
		
	# Time Handling ==========================================
	
	# calculate positions of (scale) markers
	def calc_markers(self):		
		# create markers
		# - always present
		self.fixed_markers = [
			TimeMarker(0.5,     "Year",     TimeDeltas['Year']),
			TimeMarker(0.625,   "6 Months", TimeDeltas['6 Months']),
			TimeMarker(0.75,    "Month",    TimeDeltas['Month']),
			TimeMarker(0.8125,  "Week",		TimeDeltas['Week']),
			TimeMarker(0.875,   "Day",      TimeDeltas['Day']),
			TimeMarker(0.93,    "3Hr",      TimeDeltas['3 Hours']),
			TimeMarker(0.96,    "Hr ",      TimeDeltas['Last Hour']),
			TimeMarker(1.0,     "Now",      TimeDeltas['Now'])
		]
		
		# - year markers
		year_markers = []
		
		if self.time_model:
			# extract year range from the markers
			tmin = dtFromTimestamp(self.time_model.tmin)
			tmax = dtFromTimestamp(self.time_model.tmax)
			
			ymin = tmin.year
			ymax = tmax.year
			
			# for each year (apart from the last)
			# - when we've got more than 5 years of stuff, only show every 5 years
			years = ymax - ymin
			if years > 5:
				# +1 is to ensure that the last item doesn't overlap with anything
				years = math.ceil((years + 0.5) / 5.0)
				step = 5
			else:
				step = 1
				
			# - 0.5 here is max length of region that markers are supposed to fit into
			xsp = 0.5 / years
			
			x = 0.0
			for year in range(ymin, ymax, step):
				year_markers.append(TimeMarker(x, str(year), year))
				x += xsp
		
		# create the list now
		self.markers = year_markers + self.fixed_markers
		
		# create a quick mapping from marker positions to markers
		self.marker_pos_map = [m.pos for m in self.markers]
	
	
	# Convert a time on the slider to a real time
	def map_time(self, t):
		# if no time model is present, just do 1-1 mapping
		if not self.time_model:
			return t
			
		# special cases - endpoints
		if t <= 0.0:
			return self.time_model.tmin
		elif t >= 1.0:
			return self.time_model.tmax
		
		# get timestamp of "now"
		now = dtFromTimestamp(self.time_model.tmax)
		
		# check which time range it fell into
		i = bisect.bisect_right(self.marker_pos_map, t)
		if 0 <= i < len(self.markers):
			# get markers
			start = self.markers[i-1]
			end = self.markers[i]
			
			# grab the timestamps for these
			# XXX: could possibly optimise this more, to reduce need to convert so many times
			if type(start.tdelta) is int:
				stime = timestampFromDt(now.replace(day=1, month=1, year = start.tdelta))
			else:
				stime = timestampFromDt(now - start.tdelta)
			
			if type(end.tdelta) is int:
				etime = timestampFromDt(now.replace(day=1, month=1, year = end.tdelta))
			else:
				etime = timestampFromDt(now - end.tdelta)
				
			# interpolate between these based on the position
			return stime + (t - start.pos) / (end.pos - start.pos) * (etime - stime)
		
		# invalid time
		return -1
	
	
	# calculate positions of gradient stops
	def calc_gradient_stops(self):
		# map these a usable range
		if self.movable_gradient:
			# use the start/end points of the range slider
			s = self.start
			e = self.end
		else:
			# use hardcoded coordinates for 'recent' range
			# year = 0.5
			s = 0.5
			e = 1.0
			
		# use standard gradient <-> stops calculation
		self.gradientStops = calcTimeGradientStops(s, e)
		self.gradientStops = [min(1, max(0, x)) for x in self.gradientStops] # clamp to prevent warnings
	
	# Drawing ===============================================
	
	# hook for widget resizing - hint for handles to get redrawn
	def resizeEvent(self, evt):
		qgui.QWidget.resizeEvent(self, evt)
		
		# adjust positions of the handles
		self.sCtrl.updatePosition()
		self.eCtrl.updatePosition()
	
	# -------------------------------------------------------
	
	# hook for widget drawing/redrawing
	def paintEvent(self, evt):
		# perform drawing
		p = qgui.QPainter()
		p.begin(self)
		
		# background indicator stuff
		self.draw_background(p)
		self.draw_markers(p)
		
		# controls
		self.draw_controls(p)
		
		p.end()
	
	# draw the slider background
	def draw_background(self, p):
		# font for labels
		font = qgui.QFont('Helvetica', 9, qgui.QFont.Light)
		p.setFont(font)
		
		# widget area
		size = self.size()
		w = self.width()
		h = self.height()
		
		# color palette - native colors
		cs = qgui.QPalette()
		
		# border color
		pen = qgui.QPen(cs.color(qgui.QPalette.Dark), 1)
		p.setPen(pen)
		
		# white background
		p.setBrush(qgui.QColor("white"))
		p.drawRect(0, 0, w, h)
		
		# gradient - new to old (right to left) ----------------------------------
		gr = qgui.QLinearGradient(0.0, 0.0, w-1, 0)
		alpha = 200
		
		# initial "white stop"
		gr.setColorAt(0.0, qgui.QColor("white"))
		
		# gradient color stops
		for pos, col in getTimeGradientStopPairs(self.gradientStops):
			gr.setColorAt(pos, qgui.QColor(col[0], col[1], col[2], alpha))
		
		# fade to white at end
		if self.gradientStops[-1] < 1.0:
			gr.setColorAt(min(1.0, self.end + 0.01), qgui.QColor("white"))
		
		# border outline and background fill (gradient) ---------------------------
		# - draw gradient over white background so that alpha fades properly
		p.setBrush(gr)
		p.drawRect(0, 0, w, h)
	
	
	# scale markers
	def draw_markers(self, p):
		# font for labels
		font = qgui.QFont('Helvetica', 9, qgui.QFont.Light)
		p.setFont(font)
		metrics = p.fontMetrics()
		
		# widget area
		w = self.width()
		h = self.height()
		
		# set colors
		p.setPen(qgui.QColor("black"))
		p.setBrush(qcore.Qt.NoBrush)
		
		# draw markers themselves
		for marker in self.markers:
			pos = marker.pos
			lbl = marker.label
			
			fw = metrics.width(lbl)
			fh = metrics.height()
			
			# x-coordinate of marker
			x = pos * (w - 1)
			
			if pos == 1.0:
				# right aligned
				lx = x - fw
			elif pos == 0.0:
				# left aligned
				lx = x + 1
			else:
				# centered
				lx = x - (fw / 2.0)
			
			# top indicators - relative -----------------------------------
			y = fh + 5
			
			p.drawLine(x, 1, x, 6)
			p.drawText(lx, y, lbl)
			
			# bottom indicators - relative -------------------------------
			"""
			y = h - 5
			
			# FIXME: labels used should be actual timestamps...
			p.drawLine(x, h, x, h-5)
			p.drawText(lx, y, lbl) 
			"""
		
	# draw the slider controls
	def draw_controls(self, p):
		# calculate extents
		w = self.width()
		h = self.height()
		
		# - midpoints of the control endpoints
		xs = self.start * w
		xe = self.end * w
		
		# draw solid (translucent) bar between these
		pen = qgui.QPen(qgui.QColor(30, 30, 30, 128), 1)
		fill = qgui.QColor(40, 40, 40, 128)
		
		p.setBrush(fill)
		p.setPen(pen)
		
		p.drawRect(xs, h-18, xe-xs, 14)
		
		# draw indicator lines - dark dotted lines
		pen = qgui.QPen(qgui.QColor(30, 30, 30), 1, qcore.Qt.DashLine)
		p.setPen(pen)
		
		p.drawLine(xs, 1, xs, h-1)
		p.drawLine(xe, 1, xe, h-1)
		
	# Event Handling =====================================
	
	# Time range changed - repaint widget
	def update_display(self):
		# update the values
		self.tmin = self.map_time(self.start)
		self.tmax = self.map_time(self.end)
		
		# update position of gradient stops
		self.calc_gradient_stops()
		
		# update tooltip, for whenever it gets shown
		self.update_tooltip()
		
		# update handles
		self.sCtrl.updatePosition()
		self.eCtrl.updatePosition()
		
		# now draw...
		self.repaint()
	
	# Generate tooltip text
	def update_tooltip(self):
		# helper to get string from timestamp
		def str_from_timestamp(pos, t):
			# time stamp
			ts = time.localtime(t)
			
			# perform string formatting based on time range
			if pos <= 0.5: # < year
				# date format - emphasize just year/month
				return time.strftime("<b>%Y</b> %b", ts)
			elif pos <= 0.75: # < month
				# date format - emphasize year/month, but still show dates
				return time.strftime("<b>%Y %b</b> %d", ts)
			elif pos <= 0.875: # < day
				# date format (+ day of week)
				return time.strftime("%Y <b>%b %d</b> <i>(%a)</i>", ts)
			else:
				# time format
				return time.strftime("<b>%I:%M %p</b>  <i>(%a)</i>", ts)
		
		# get min/max dates
		min_str = str_from_timestamp(self.start, self.tmin)
		max_str = str_from_timestamp(self.end, self.tmax)
		
		# construct tooltip
		tooltip  = "<p style='white-space:pre'>"
		tooltip += "%s   -->   %s" % (min_str, max_str)
		tooltip += "</p>"
		
		# set tooltip
		self.setToolTip(tooltip)
		
		self.sCtrl.setToolTip(tooltip)
		self.eCtrl.setToolTip(tooltip)
		
	# force the tooltip to be shown
	def force_tooltip(self, event):
		if self.sCtrl.x() <= event.x() <= self.eCtrl.x():
			qgui.QToolTip.showText(event.globalPos(), self.toolTip())
		else:
			qgui.QToolTip.hideText()
	
	# override generic events to capture the ones we're interested in overriding
	def event(self, event):
		# catch tooltip events so that we respond with less delay
		if event.type() == qcore.QEvent.ToolTip:
			# try to force tooltip to be shown
			self.force_tooltip(event)
			
			return True
		
		return super(TimeRangeSlider, self).event(event)
	
	# ----------------------------------------------------
	
	# Reset range
	def resetRange(self):
		# just most recent - week = 0.8125
		self.start = 0.8125
		self.end = 1.0
		
		# range has now changed...
		self.timeRangeChanged.emit()
	
	# ----------------------------------------------------
	
	# shift the range by the given amount
	def range_shift(self, delta):
		# apply delta
		self.start += delta
		self.end   += delta
		
		# clamp - to never go out of bounds
		self.start = min(1.0, max(0.0, self.start))
		self.end   = min(1.0, max(0.0, self.end))
		
		# send events
		# WARNING: sometimes this can be slow!
		qgui.QApplication.setOverrideCursor(qcore.Qt.WaitCursor)
		self.timeRangeChanged.emit()
		qgui.QApplication.restoreOverrideCursor()
		
		
	# shift the center of the range to the specified spot
	def range_center_shift(self, pos):
		# calculate midpoint of the range
		mid = (self.end - self.start) / 2.0 + self.start
		
		# calculate offset needed to move center to new position
		delta = pos - mid
		
		# apply
		self.range_shift(delta)
	
	
	# Is time slider being edited?
	def isMoving(self):
		return (self.mx is not None) or (self.sCtrl.isDown() or self.eCtrl.isDown())
	
	
	# Update cursor
	def update_cursor(self, mevt):
		if self.isMoving():
			# moving
			self.setCursor(qcore.Qt.ClosedHandCursor)
		else:
			# show hand if within grip/moving range region, otherwise default
			if isinstance(mevt, qgui.QMouseEvent):
				# check if we are within the grip range
				if self.sCtrl.x() <= mevt.x() <= self.eCtrl.x():
					self.setCursor(qcore.Qt.OpenHandCursor)
				else:
					self.setCursor(qcore.Qt.ArrowCursor)
			else:
				# not a mouse event (i.e. leave event) -> cannot tell where we are, but we're outside!
				self.setCursor(qcore.Qt.ArrowCursor)
	
	
	# Start of mouse movement
	def mousePressEvent(self, mevt):
		# determine which handle to activate
		mx = mevt.x()
		
		if mx <= self.sCtrl.x():
			# move middle of control to the left first
			self.range_center_shift(float(mx) / self.width())
			
			# now allow dragging from this point as a base
			self.mx = mx
		elif mx >= self.eCtrl.x():
			# move middle of control to the right first
			self.range_center_shift(float(mx) / self.width())
			
			# now allow dragging from this point as a base
			self.mx = mx
		elif (self.start == 0.0) and (self.end == 1.0):
			# full range start - move start to wherever we clicked
			self.start = float(mx) / self.width()
			
			# clamp - to never go out of bounds
			self.start = min(1.0, max(0.0, self.start))
			
			# send events
			self.timeRangeChanged.emit()
			
			# TODO: activate handle
		else:
			# initiate moving
			self.mx = mx
			
		# update cursor
		self.update_cursor(mevt)
	
	# Mouse-move handling
	def mouseMoveEvent(self, mevt):
		# set cursor
		self.update_cursor(mevt)
		
		# if moving...
		if self.isMoving():
			# apply delta movement
			x = mevt.x()
			dx = x - self.mx
			
			# inverse from pixels to relative offset
			delta = float(dx) / self.width()
			
			# apply delta
			self.range_shift(delta)
			self.force_tooltip(mevt)
			
			# store reference point for next step
			self.mx = x
		
	# End of mouse movement
	def mouseReleaseEvent(self, mevt):
		# clear the settings
		self.mx = None
		
		# update cursor
		self.update_cursor(mevt)
		
		# send events
		# NOTE: need to change cursor to waitcursor as this is slow...
		qgui.QApplication.setOverrideCursor(qcore.Qt.WaitCursor)
		self.timeRangeChanged.emit()
		qgui.QApplication.restoreOverrideCursor()
		
	# Mouse exited bounds of widget...
	def leaveEvent(self, evt):
		# Exit "moving" mode, as it is confusing when mouse-event focus is lost when
		# user strays outside the window, but then comes back in and finds that 
		# the widget still responds (even if they let go of mouse inbetween)
		#
		# - as per mouseReleaseEvent()
		self.mx = None
		self.update_cursor(evt)

##########################################

if __name__ == '__main__':
	import sys
	
	app = qgui.QApplication(sys.argv)
	
	win = qgui.QMainWindow()
	win.setWindowTitle("Time Range Slider Test")
	
	trs = TimeRangeSlider() 
	
	vbox = qgui.QVBoxLayout()
	vbox.addWidget(trs)
	
	pane = qgui.QWidget()
	pane.setLayout(vbox)
	win.setCentralWidget(pane)
	
	win.show()
	app.exec_()
