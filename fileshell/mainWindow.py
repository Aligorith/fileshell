# Main window for file browser
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.theming import qrc_resources_rc
from fileshell.theming import stylesheets
from fileshell.theming import windowTheme

from fileshell import browser
from fileshell.components import statsDialog

from fileshell.model.fsutils import *


# Main Window
class FileShell(qgui.QTabWidget):
	# Setup =======================================
	
	# ctor
	# < fs_model: (fsdb.FsModel) file system model to use
	# < path: (str) path to starting directory to display on startup
	# < features: ({str:bool}) dict of "feature name" <-> enabled-status flags
	def __init__(self, fs_model, path, features):
		qgui.QTabWidget.__init__(self)
		
		# window settings
		self.setWindowIcon( qgui.QIcon.fromTheme("folder", 
							qgui.QIcon(":/images/folder.png")) )
		self.setWindowTitle("File Browser")
		self.resize(900, 570)
		
		windowTheme.enableGlassWindowTheme(self, True)
		
		# tab bar settings
		# TODO: allow MMB to be used to close tabs!
		self.setTabsClosable(True)
		self.tabCloseRequested.connect(self.closeTab)
		
		self.setMovable(True)
		
		# custom theming for this tab (and this tab alone)
		# NOTE: we need to name tabBar too, otherwise theming fails
		self.setObjectName("winTabHost")
		self.tabBar().setObjectName("winTabHost")
		
		self.setStyleSheet(stylesheets.MainTabs)
		
		# state - for allowing window dragging by clicking on tabbar
		self.window_moving = False
		
		# init data model
		self.fs_model = fs_model
		self.features = features
		
		# setup additional buttons and bind events
		self.setup_ui()
		self.bind_events()
		
		# show window
		# TODO: show with a progress bar which is visible until tab is added
		self.show()
		
		# add initial tab
		self.addNewTab(path)
		
	# Setup additional buttons 
	def setup_ui(self):
		# app menu --------------------------------
		self.appButton = qgui.QPushButton("File")
		self.appButton.setToolTip("Click here for File Browser options and settings")
		
		self.appButton.setObjectName("AppButton")
		#self.appButton.setFixedSize(qcore.QSize(68, 20))
		self.setCornerWidget(self.appButton, qcore.Qt.TopLeftCorner)
		
		self.setup_appMenu()
		self.appButton.setMenu(self.appMenu)
		
		# new tab button --------------------------
		self.addTabButton = qgui.QToolButton()
		self.addTabButton.setToolTip("Open a new tab")
		
		self.addTabButton.setObjectName("AddTab")
		self.addTabButton.setFixedSize(qcore.QSize(26, 16))
		self.setCornerWidget(self.addTabButton, qcore.Qt.TopRightCorner)
		
		self.addTabButton.clicked.connect(self.on_AddNewTab) # NOTE: cannot directly connect, as this emits bool
		self.addTabButton.setShortcut(qgui.QKeySequence.AddTab)
		
	# Menu for "app button"
	def setup_appMenu(self):
		menu = self.appMenu = qgui.QMenu()
		
		# New window/tab functionalities ------------
		actNewTab = menu.addAction("New Tab")
		actNewTab.triggered.connect(self.on_AddNewTab)
		
		actNewWin = menu.addAction("New Window")
		
		#actPrivateToggle = menu.addAction("Start Private Browsing")
		#actPrivateToggle.setToolTip("Start a new session where FileShell won't save information about where you've been")
		
		# Database Management -----------------------
		menu.addSeparator()
		
		actStats = menu.addAction("Statistics...")
		actStats.setToolTip("Show keys statistics about file system database")
		actStats.triggered.connect(self.on_StatsShow)
		
		actUpdate = menu.addAction("Update Database...")
		actUpdate.setToolTip("Update file system database to in response to major changes made outside of FileShell")
		
		#actManage = menu.addAction("Manage Locations...")
		#actManage.setToolTip("Add/Remove directories from SCOFT bar for easy access")
		
		actReset = menu.addAction("Reset Browser")
		actReset.setToolTip("Reset browser interface to initial state")
		actReset.triggered.connect(self.resetUI)
		
		# General Settings ---------------------------
		menu.addSeparator()
		
		#actTimeFilterToggle = menu.addAction("Time-Based Filtering")
		#actTimeFilterToggle.setToolTip("Fade out items that weren't changed within the specified time period to make it easier to find things")
		#actTimeFilterToggle.setCheckable(True)
		#actTimeFilterToggle.setChecked(True) # XXX: always on for nows
		
		actSettings = menu.addAction("Preferences...")
		
		# About --------------------------------------
		menu.addSeparator()
		
		actAboutQt = menu.addAction("About Qt")
		actAboutQt.triggered.connect(self.on_AboutQt)
		
		actAbout = menu.addAction("About...")
		actAbout.triggered.connect(self.on_AboutApp)
		
		
		actExit = menu.addAction("Exit")
		actExit.triggered.connect(self.on_ExitApp)
		
	# Bind event handling things
	def bind_events(self):
		# changing tabs should change title of app
		self.currentChanged.connect(self.refreshActiveTab)
		
		# TODO: mmb to close tabs...
		# http://stackoverflow.com/questions/9442165/pyqt-mouse-events-for-qtabwidget
		
	# Window Frame Moving =======================================================
	
	def mousePressEvent(self, mevt):
		if mevt.button() == qcore.Qt.LeftButton:
			self.window_moving = True
			self.window_mprev = [mevt.globalX(), mevt.globalY()]
		else:
			self.window_moving = False
			self.window_mprev = None
	
	def mouseMoveEvent(self, mevt):
		if self.window_moving:
			newPos = [mevt.globalX(), mevt.globalY()]
			delta  = [(nPos - pPos)  for nPos,pPos in zip(newPos, self.window_mprev)]
			
			self.move(self.x() + delta[0], self.y() + delta[1])
			self.window_mprev = newPos
			
	def mouseReleaseEvent(self, mevt):
		self.window_moving = False
		self.window_mprev = None
	
	# Event Handling ============================================================
	
	# Callback to add new tab
	def on_AddNewTab(self):
		qgui.QApplication.setOverrideCursor(qgui.QCursor(qcore.Qt.WaitCursor))
		
		# TODO: use current tab as base?
		self.addNewTab(None)
		
		qgui.QApplication.restoreOverrideCursor()
	
	# Add a new tab page
	# < path: (str | FolderData) path to open as a new tab
	def addNewTab(self, path):
		with TimedOperation("Adding new tab..."):
			bpane = browser.BrowserInterface(self.fs_model, path, self.features)
			
			if path:
				name = get_destination_name(path)
			else:
				name = "New Tab"
			
			# add tab
			self.addTab(bpane, name)
			
			# finish off setting up browser
			bpane.initUI()
		
		
	# Close tab - used when a tab close button is pressed
	def closeTab(self, index):	
		widget = self.widget(index)
		
		if widget:
			# TODO: to prevent errors, allow closing active tab only?
			if self.count() == 1:
				# if only tab open, kill application
				self.close()
			else:
				# kill the tab in question
				self.removeTab(index)
				del widget
	
	# Update title and/or perform any additional checks needed when the active tab changes
	def refreshActiveTab(self, index):
		# sanity check
		if (index < 0) or (index > self.count()):
			print "Invalid tab index: %d" % (index)
			return
		
		# set window title to that of the active tab
		self.setWindowTitle("%s - File Browser" % (self.tabText(index)))
		
	# Reset UI to initial state
	def resetUI(self):
		qgui.QApplication.setOverrideCursor(qgui.QCursor(qcore.Qt.WaitCursor))
	
		bpane = self.currentWidget()
		if bpane:
			bpane.resetUI()
			
		qgui.QApplication.restoreOverrideCursor()
		
	# Close application
	def on_ExitApp(self):
		print "Closing..."
		# TODO: need to perform cleanup operations!
		self.deleteLater()
		
	# Show "About Qt" Dialog
	def on_AboutQt(self):
		qgui.QMessageBox.aboutQt(self)
		
	# Show "About" dialog
	def on_AboutApp(self):
		aboutText  = "<p><b>SCOFT File Browser</b><br>"
		aboutText += "Version: %s (%s)<br>" % ("2.5", "August 2013")
		aboutText += "Author: Joshua Leung (aligorith@gmail.com)</p>"
		aboutText += "<p>An experimental file browser which aims to make it easier and faster "
		aboutText += "to find and access files, using spatially consistent visualisations, "
		aboutText += "revisitation behaviour, and in-place temporal search. It was originally developed "
		aboutText += "as part of my Honours project in Computer Science at the University of Canterbury, "
		aboutText += "and has been subsequently enhanced as part of ongoing research work.</p>"
		aboutText += "<p>Icons Taken From: <a href='http://tango.freedesktop.org'>Tango</a> and "
		aboutText += "<a href='http://www.oxygen-icons.org'>Oxygen</a> icon sets</p>"
		
		qgui.QMessageBox.about(self, "About File Browser", aboutText)
		
	# Show "Statistics" dialog
	def on_StatsShow(self):
		# WARNING: sometimes this can be slow!
		qgui.QApplication.setOverrideCursor(qcore.Qt.WaitCursor)
		
		dlg = statsDialog.StatsDialog(self.fs_model, self)
		dlg.show()
		
		qgui.QApplication.restoreOverrideCursor()


