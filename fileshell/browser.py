# File browser panel - Body of each tab
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.theming import stylesheets

from fileshell.components.navbar import Navbar
from fileshell.filewidgets.foldertree import FsFolderPane
from fileshell.filewidgets.fileview import StandardFileList
from fileshell.model.filters import FilterModel
from fileshell.model.navmodel import NavModel
from fileshell.time.timeRangeSlider import TimeRangeSlider

#########################################

# File Browser interface
class BrowserInterface(qgui.QWidget):
	# ctor
	# < fs_model: (FsModel)
	# < (startPath): (str | FolderData)
	# < (features): ({str:bool}) dict of "feature name" <-> "enabled status" mappings 
	# < (private): (bool) log each folder access made - FOR NOW/TESTING, THIS IS TURNED ON TO PREVENT BIAS
	def __init__(self, fs_model, startPath=None, features=None, private=True):
		qgui.QWidget.__init__(self)
		
		# init models
		self.fs_model = fs_model				# file system model (full dataset)
		
		self.navmodel = NavModel(fs_model, startPath, private) # navigation stuff
		self.filters = FilterModel(fs_model)	# model for the filters used
		
		# init settings
		self.features = features
		
		# init ui components
		self.setup_ui()
		
		# connect special cross-component event handlers
		try:
			self.itemView.folderHighlightChanged.connect(self.folderPane.viz.setHighlightIndex)
		except:
			print "FileView -> SCOFT item highlights hookup failed..."
			pass
		
		try:
			self.itemView.postItemOpen.connect(self.folderPane.on_flashTargetTriggered)
		except:
			print "FileView -> FolderPane.SCOFT tag flash trigger failed..."
			pass
		
		# connect models to UI components
		self.navmodel.locationChanged.connect(self.on_LocationChanged)
		
	# Setup UI ============================================================
	
	# setup ui components
	def setup_ui(self):
		# layout container
		vbox = qgui.QVBoxLayout()
		
		vbox.setSpacing(0)
		vbox.setMargin(0)
		
		self.setLayout(vbox)
		self.setContentsMargins(1, 0, 1, 0)
		
		# navigation bar ..........................
		self.setup_navbar()
		vbox.addWidget(self.navbar, stretch=0)
		
		# TODO: action bar below navigation bar may need to be
		# such that both are in a splitter, splitting against
		# everything else...
		
		# views ....................................
		
		# hsplitter contains the two folder/file views to allow resizing
		self.splitter = qgui.QSplitter()
		self.splitter.setHandleWidth(2)
		vbox.addWidget(self.splitter, stretch=1)
		
		# tree views
		self.folderPane = FsFolderPane(self.fs_model, self.navmodel, self.filters, self.features)
		
		self.splitter.addWidget(self.folderPane)
		self.splitter.setStretchFactor(0, 0)
		
		# icon grid
		self.setup_itemView()
		self.splitter.addWidget(self.itemsHost)
		self.splitter.setStretchFactor(1, 20)
		
		# time filter ...............................
		
		self.setup_timeFilter()
		vbox.addLayout(self.timeHost, stretch=0)
		
	# navigation bar
	def setup_navbar(self):
		# setup widget
		self.navbar = Navbar()
		
		self.navbar.attachFilterModel(self.filters)
		self.navbar.attachNavigationModel(self.navmodel)
	
	# main content view
	def setup_itemView(self):
		# layout
		vbox = qgui.QVBoxLayout()
		vbox.setSpacing(0)
		vbox.setMargin(2)
		
		self.itemsHost = qgui.QWidget()
		self.itemsHost.setContentsMargins(1,0,1,0)
		self.itemsHost.setLayout(vbox)
		
		# item view
		self.itemView = StandardFileList(self.fs_model, self.navmodel, self.filters, self.features)
		vbox.addWidget(self.itemView, stretch=1)
		
		# statusbar
		#self.statusbar = qgui.QStatusBar()
		#vbox.addWidget(self.statusbar, stretch=0)
		
		# TODO: area for a cross-directory clipboard...
		
	# time filter panel
	def setup_timeFilter(self):
		# padding around time filter
		self.timeHost = qgui.QHBoxLayout()
		self.timeHost.setContentsMargins(1, 3, 1, 2)
		
		# filtering widget
		self.timeFilter = TimeRangeSlider(self.fs_model.time_model)
		self.timeHost.addWidget(self.timeFilter)
		
		# hook up updates to the filters
		self.timeFilter.timeRangeChanged.connect(self.on_TimeFilterChanged)
		
		# flush initial values
		# XXX: this is a bit hacky, but just copy values across, since the consumers aren't properly setup yet
		#self.filters.update_timeRange(self.timeFilter.tmin, self.timeFilter.tmax)
		
		self.filters.tmin = self.timeFilter.tmin
		self.filters.tmax = self.timeFilter.tmax
		
		# hide time filter if not used
		if self.filters.use_time_filter == False:
			self.timeFilter.hide()
	
	# Methods ==========================================
	
	# Finish initialising browser
	def initUI(self):
		# build shared cache models in filters
		self.filters.updateCaches()
		
		# build models in various views
		self.folderPane.initUI()
		
		# flush the navigation model so that everything is valid
		self.navmodel.locationChanged.emit()
		
	# Reset UI to initial state
	def resetUI(self):
		# reset filemodel
		self.fs_model.reset()
		
		# set navmodel to initial location
		self.navmodel.reset()
		
		# reset folder tree expand status
		self.folderPane.resetUI()
		
		# reset filters
		self.filters.reset()
		if self.filters.use_time_filter:
			try:
				self.timeFilter.resetRange()
			except AttributeError:
				pass
	
	# Event Callbacks ==================================
	
	# update time filter
	def on_TimeFilterChanged(self):
		# flush the filter changes to the time ranges
		# NOTE: for efficiency, only do this when it isn't moving
		if not self.timeFilter.isMoving():
			self.filters.update_timeRange(self.timeFilter.tmin, self.timeFilter.tmax)
			
	# update parent on location change
	def on_LocationChanged(self):
		# grab parent tab-host
		tabhost = self.window()
		
		# refresh current tab's name
		name = self.navmodel.location.name
		
		index = tabhost.indexOf(self)
		tabhost.setTabText(index, name)
		tabhost.refreshActiveTab(index)

#########################################
